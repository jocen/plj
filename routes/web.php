<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PageController@home');
Route::get('tentang-kami', 'PageController@tentang_kami');
Route::get('kegiatan', 'PageController@kegiatan');
Route::get('informasi', 'PageController@informasi');

Route::group(['prefix' => 'plj-admin'], function () {
    Route::get('/', function () {
        return redirect('plj-admin/login');
    });
    Route::get('login', 'Admin\AuthController@checkLogin');
    Route::post('auth/login', 'Admin\AuthController@login');
    Route::get('auth/logout', 'Admin\AuthController@logout');

    Route::group(['middleware' => ['checksession']], function () {
        Route::get('dashboard', 'Admin\DashboardController@view');
        Route::get('dashboard/report_product', 'Admin\DashboardController@report_product');
        Route::post('dashboard/export_report_product', 'Admin\DashboardController@export_report_product');

        Route::get('banner', 'Admin\BannerController@view')
            ->name('banner_view');
        Route::get('banner/create', 'Admin\BannerController@create');
        Route::get('banner/edit/{id}', 'Admin\BannerController@edit');
        Route::post('banner/insert', 'Admin\BannerController@insert');
        Route::post('banner/update', 'Admin\BannerController@update');
        Route::get('banner/delete/{id}', 'Admin\BannerController@delete');
        Route::get('banner/status/{id}/{status}', 'Admin\BannerController@status');
        Route::post('banner/update_sort', 'Admin\BannerController@update_sort');

        Route::get('tentang_kami', 'Admin\TentangKamiController@view')
            ->name('tentang_kami_view');
        Route::get('tentang_kami/create', 'Admin\TentangKamiController@create');
        Route::get('tentang_kami/edit/{id}', 'Admin\TentangKamiController@edit');
        Route::post('tentang_kami/insert', 'Admin\TentangKamiController@insert');
        Route::post('tentang_kami/update', 'Admin\TentangKamiController@update');
        Route::get('tentang_kami/delete/{id}', 'Admin\TentangKamiController@delete');
        Route::get('tentang_kami/status/{id}/{status}', 'Admin\TentangKamiController@status');
        Route::post('tentang_kami/update_sort', 'Admin\TentangKamiController@update_sort');

        Route::get('cara_kerja', 'Admin\CaraKerjaController@view')
            ->name('cara_kerja_view');
        Route::get('cara_kerja/create', 'Admin\CaraKerjaController@create');
        Route::get('cara_kerja/edit/{id}', 'Admin\CaraKerjaController@edit');
        Route::post('cara_kerja/insert', 'Admin\CaraKerjaController@insert');
        Route::post('cara_kerja/update', 'Admin\CaraKerjaController@update');
        Route::get('cara_kerja/delete/{id}', 'Admin\CaraKerjaController@delete');
        Route::get('cara_kerja/status/{id}/{status}', 'Admin\CaraKerjaController@status');
        Route::post('cara_kerja/update_sort', 'Admin\CaraKerjaController@update_sort');

        Route::get('program_kelas', 'Admin\ProgramKelasController@view')
            ->name('program_kelas_view');
        Route::get('program_kelas/create', 'Admin\ProgramKelasController@create');
        Route::get('program_kelas/edit/{id}', 'Admin\ProgramKelasController@edit');
        Route::post('program_kelas/insert', 'Admin\ProgramKelasController@insert');
        Route::post('program_kelas/update', 'Admin\ProgramKelasController@update');
        Route::get('program_kelas/delete/{id}', 'Admin\ProgramKelasController@delete');
        Route::get('program_kelas/status/{id}/{status}', 'Admin\ProgramKelasController@status');
        Route::post('program_kelas/update_sort', 'Admin\ProgramKelasController@update_sort');
        Route::get('program_kelas_title', 'Admin\ProgramKelasController@title')
            ->name('program_kelas_title_view');
        Route::post('program_kelas_title/update', 'Admin\ProgramKelasController@update_title');

        Route::get('galeri', 'Admin\GaleriController@view')
            ->name('galeri_view');
        Route::get('galeri/create', 'Admin\GaleriController@create');
        Route::get('galeri/edit/{id}', 'Admin\GaleriController@edit');
        Route::post('galeri/insert', 'Admin\GaleriController@insert');
        Route::post('galeri/update', 'Admin\GaleriController@update');
        Route::get('galeri/delete/{id}', 'Admin\GaleriController@delete');
        Route::get('galeri/status/{id}/{status}', 'Admin\GaleriController@status');
        Route::post('galeri/update_sort', 'Admin\GaleriController@update_sort');
        Route::get('galeri_title', 'Admin\GaleriController@title')
            ->name('galeri_title_view');
        Route::post('galeri_title/update', 'Admin\GaleriController@update_title');

        Route::get('sejarah', 'Admin\SejarahController@view')
            ->name('sejarah_view');
        Route::get('sejarah/create', 'Admin\SejarahController@create');
        Route::get('sejarah/edit/{id}', 'Admin\SejarahController@edit');
        Route::post('sejarah/insert', 'Admin\SejarahController@insert');
        Route::post('sejarah/update', 'Admin\SejarahController@update');
        Route::get('sejarah/delete/{id}', 'Admin\SejarahController@delete');
        Route::get('sejarah/status/{id}/{status}', 'Admin\SejarahController@status');
        Route::post('sejarah/update_sort', 'Admin\SejarahController@update_sort');

        Route::get('dasar_pemikiran', 'Admin\DasarPemikiranController@view')
            ->name('dasar_pemikiran_view');
        Route::get('dasar_pemikiran/create', 'Admin\DasarPemikiranController@create');
        Route::get('dasar_pemikiran/edit/{id}', 'Admin\DasarPemikiranController@edit');
        Route::post('dasar_pemikiran/insert', 'Admin\DasarPemikiranController@insert');
        Route::post('dasar_pemikiran/update', 'Admin\DasarPemikiranController@update');
        Route::get('dasar_pemikiran/delete/{id}', 'Admin\DasarPemikiranController@delete');
        Route::get('dasar_pemikiran/status/{id}/{status}', 'Admin\DasarPemikiranController@status');
        Route::post('dasar_pemikiran/update_sort', 'Admin\DasarPemikiranController@update_sort');
        Route::post('dasar_pemikiran/update_image', 'Admin\DasarPemikiranController@updateImage');

        Route::get('layanan', 'Admin\LayananController@view')
            ->name('layanan_view');
        Route::get('layanan/create', 'Admin\LayananController@create');
        Route::get('layanan/edit/{id}', 'Admin\LayananController@edit');
        Route::post('layanan/insert', 'Admin\LayananController@insert');
        Route::post('layanan/update', 'Admin\LayananController@update');
        Route::get('layanan/delete/{id}', 'Admin\LayananController@delete');
        Route::get('layanan/status/{id}/{status}', 'Admin\LayananController@status');
        Route::post('layanan/update_sort', 'Admin\LayananController@update_sort');

        Route::get('keunggulan_kami', 'Admin\MengapaController@view')
            ->name('mengapa_belajar_view');
        Route::get('keunggulan_kami/create', 'Admin\MengapaController@create');
        Route::get('keunggulan_kami/edit/{id}', 'Admin\MengapaController@edit');
        Route::post('keunggulan_kami/insert', 'Admin\MengapaController@insert');
        Route::post('keunggulan_kami/update', 'Admin\MengapaController@update');
        Route::get('keunggulan_kami/delete/{id}', 'Admin\MengapaController@delete');
        Route::get('keunggulan_kami/status/{id}/{status}', 'Admin\MengapaController@status');
        Route::post('keunggulan_kami/update_sort', 'Admin\MengapaController@update_sort');

        Route::get('klien', 'Admin\KolaborasiController@view')
            ->name('kolaborasi_view');
        Route::get('klien/create', 'Admin\KolaborasiController@create');
        Route::get('klien/edit/{id}', 'Admin\KolaborasiController@edit');
        Route::post('klien/insert', 'Admin\KolaborasiController@insert');
        Route::post('klien/update', 'Admin\KolaborasiController@update');
        Route::get('klien/delete/{id}', 'Admin\KolaborasiController@delete');
        Route::get('klien/status/{id}/{status}', 'Admin\KolaborasiController@status');
        Route::post('klien/update_sort', 'Admin\KolaborasiController@update_sort');
        Route::get('klien_title', 'Admin\KolaborasiController@title')
            ->name('kolaborasi_title_view');
        Route::post('klien_title/update', 'Admin\KolaborasiController@update_title');

        Route::get('kerjasama', 'Admin\KerjasamaController@view')
            ->name('kerjasama_view');
        Route::get('kerjasama/create', 'Admin\KerjasamaController@create');
        Route::get('kerjasama/edit/{id}', 'Admin\KerjasamaController@edit');
        Route::post('kerjasama/insert', 'Admin\KerjasamaController@insert');
        Route::post('kerjasama/update', 'Admin\KerjasamaController@update');
        Route::get('kerjasama/delete/{id}', 'Admin\KerjasamaController@delete');
        Route::get('kerjasama/status/{id}/{status}', 'Admin\KerjasamaController@status');
        Route::post('kerjasama/update_sort', 'Admin\KerjasamaController@update_sort');
        Route::get('kerjasama_title', 'Admin\KerjasamaController@title')
            ->name('kerjasama_title_view');
        Route::post('kerjasama_title/update', 'Admin\KerjasamaController@update_title');

        Route::get('pertanyaan', 'Admin\PertanyaanController@view')
            ->name('pertanyaan_view');
        Route::get('pertanyaan/create', 'Admin\PertanyaanController@create');
        Route::get('pertanyaan/edit/{id}', 'Admin\PertanyaanController@edit');
        Route::post('pertanyaan/insert', 'Admin\PertanyaanController@insert');
        Route::post('pertanyaan/update', 'Admin\PertanyaanController@update');
        Route::get('pertanyaan/delete/{id}', 'Admin\PertanyaanController@delete');
        Route::get('pertanyaan/status/{id}/{status}', 'Admin\PertanyaanController@status');
        Route::post('pertanyaan/update_sort', 'Admin\PertanyaanController@update_sort');
        Route::post('pertanyaan/update_image', 'Admin\PertanyaanController@updateImage');


        Route::get('kontak_kami', 'Admin\DashboardController@kontak')
            ->name('kontak_kami_view');
        Route::post('kontak_kami/update', 'Admin\DashboardController@update_kontak');

        Route::get('bank', 'Admin\BankController@view')
            ->name('bank_view');
        Route::get('bank/create', 'Admin\BankController@create');
        Route::get('bank/edit/{id}', 'Admin\BankController@edit');
        Route::post('bank/insert', 'Admin\BankController@insert');
        Route::post('bank/update', 'Admin\BankController@update');
        Route::get('bank/delete/{id}', 'Admin\BankController@delete');
        Route::get('bank/status/{id}/{status}', 'Admin\BankController@status');
        Route::post('bank/update_sort', 'Admin\BankController@update_sort');

        Route::get('social_media', 'Admin\SocialMediaController@view')
            ->name('social_media_view');
        Route::get('social_media/create', 'Admin\SocialMediaController@create');
        Route::get('social_media/edit/{id}', 'Admin\SocialMediaController@edit');
        Route::post('social_media/insert', 'Admin\SocialMediaController@insert');
        Route::post('social_media/update', 'Admin\SocialMediaController@update');
        Route::get('social_media/delete/{id}', 'Admin\SocialMediaController@delete');
        Route::get('social_media/status/{id}/{status}', 'Admin\SocialMediaController@status');
        Route::post('social_media/update_sort', 'Admin\SocialMediaController@update_sort');


        Route::get('about', 'Admin\DashboardController@about')
            ->name('about_view');
        Route::post('about/update', 'Admin\DashboardController@about_update');

        Route::get('company_data', 'Admin\DashboardController@company_data')
            ->name('company_data_view');
        Route::post('company_data/update', 'Admin\DashboardController@company_data_update');

        Route::get('hubungi_kami', 'Admin\DashboardController@hubungi_kami')
            ->name('hubungi_kami_view');
        Route::post('hubungi_kami/update', 'Admin\DashboardController@hubungi_kami_update');

        Route::get('latar_belakang/{id}', 'Admin\LatarBelakangController@edit')
            ->name('latar_belakang_view');
        Route::post('latar_belakang/update', 'Admin\LatarBelakangController@update');

        Route::get('testimonial', 'Admin\TestimonialController@view')
            ->name('testimonial_view');
        Route::get('testimonial/create', 'Admin\TestimonialController@create');
        Route::get('testimonial/edit/{id}', 'Admin\TestimonialController@edit');
        Route::post('testimonial/insert', 'Admin\TestimonialController@insert');
        Route::post('testimonial/update', 'Admin\TestimonialController@update');
        Route::get('testimonial/delete/{id}', 'Admin\TestimonialController@delete');
        Route::get('testimonial/status/{id}/{status}', 'Admin\TestimonialController@status'); 

        Route::get('change-password', 'Admin\AuthController@changePassword')->name('change_password_view');
        Route::post('auth/update', 'Admin\AuthController@updatePassword');

        Route::get('metadata', 'Admin\MetadataController@view')
            ->name('metadata_view');
        Route::get('metadata/create', 'Admin\MetadataController@create');
        Route::get('metadata/edit/{id}', 'Admin\MetadataController@edit');
        Route::post('metadata/update', 'Admin\MetadataController@update');
        Route::post('metadata/insert', 'Admin\MetadataController@insert');

        Route::post('logo/update', 'Admin\DashboardController@logo_update');

        Route::get('menu/create', 'Admin\MenuController@create');
        Route::get('menu/edit/{id}', 'Admin\MenuController@edit');
        Route::post('menu/insert', 'Admin\MenuController@insert');
        Route::post('menu/update', 'Admin\MenuController@update');
        Route::get('menu/delete/{id}', 'Admin\MenuController@delete');
        Route::get('menu/status/{id}/{status}', 'Admin\MenuController@status');
        Route::post('menu/update_sort', 'Admin\MenuController@update_sort');

        Route::get('admin_color', 'Admin\DashboardController@admin_color')
            ->name('admin_color_view');
        Route::post('admin_color/update', 'Admin\DashboardController@update_admin_color');

        Route::get('keunggulan', 'Admin\DashboardController@mengapa')
            ->name('mengapa_view');
        Route::post('keunggulan/update', 'Admin\DashboardController@update_mengapa');

        Route::get('apakah', 'Admin\DashboardController@apakah')
            ->name('apakah_view');
        Route::post('apakah/update', 'Admin\DashboardController@update_apakah');

        Route::get('timeline', 'Admin\TimelineController@view')
            ->name('timeline_view');
        Route::get('timeline/create', 'Admin\TimelineController@create');
        Route::get('timeline/edit/{id}', 'Admin\TimelineController@edit');
        Route::post('timeline/insert', 'Admin\TimelineController@insert');
        Route::post('timeline/update', 'Admin\TimelineController@update');
        Route::get('timeline/delete/{id}', 'Admin\TimelineController@delete');
        Route::get('timeline/status/{id}/{status}', 'Admin\TimelineController@status');
        Route::post('timeline/update_sort', 'Admin\TimelineController@update_sort');
        Route::get('timeline_title', 'Admin\TimelineController@title')
            ->name('timeline_title_view');
        Route::post('timeline_title/update', 'Admin\TimelineController@update_title');

        Route::get('video', 'Admin\DashboardController@video')
            ->name('video_view');
        Route::post('video/update', 'Admin\DashboardController@update_video');

        Route::get('struktur_kepengurusan', 'Admin\StrukturKepengurusanController@view')
            ->name('struktur_kepengurusan_view');
        Route::get('struktur_kepengurusan/create', 'Admin\StrukturKepengurusanController@create');
        Route::get('struktur_kepengurusan/edit/{id}', 'Admin\StrukturKepengurusanController@edit');
        Route::post('struktur_kepengurusan/insert', 'Admin\StrukturKepengurusanController@insert');
        Route::post('struktur_kepengurusan/update', 'Admin\StrukturKepengurusanController@update');
        Route::get('struktur_kepengurusan/delete/{id}', 'Admin\StrukturKepengurusanController@delete');
        Route::get('struktur_kepengurusan/status/{id}/{status}', 'Admin\StrukturKepengurusanController@status');
        Route::post('struktur_kepengurusan/update_sort', 'Admin\StrukturKepengurusanController@update_sort');
        Route::get('struktur_kepengurusan_title', 'Admin\StrukturKepengurusanController@title')
            ->name('struktur_kepengurusan_title_view');
        Route::post('struktur_kepengurusan_title/update', 'Admin\StrukturKepengurusanController@update_title');

        Route::get('artikel_kategori', 'Admin\ArtikelKategoriController@view')
            ->name('artikel_kategori_view');
        Route::get('artikel_kategori/create', 'Admin\ArtikelKategoriController@create');
        Route::get('artikel_kategori/edit/{id}', 'Admin\ArtikelKategoriController@edit');
        Route::post('artikel_kategori/insert', 'Admin\ArtikelKategoriController@insert');
        Route::post('artikel_kategori/update', 'Admin\ArtikelKategoriController@update');
        Route::get('artikel_kategori/delete/{id}', 'Admin\ArtikelKategoriController@delete');
        Route::get('artikel_kategori/status/{id}/{status}', 'Admin\ArtikelKategoriController@status');
        Route::post('artikel_kategori/update_sort', 'Admin\ArtikelKategoriController@update_sort');

        Route::get('artikel', 'Admin\ArtikelController@view')
            ->name('artikel_view');
        Route::get('artikel/create', 'Admin\ArtikelController@create');
        Route::get('artikel/edit/{id}', 'Admin\ArtikelController@edit');
        Route::post('artikel/insert', 'Admin\ArtikelController@insert');
        Route::post('artikel/update', 'Admin\ArtikelController@update');
        Route::get('artikel/delete/{id}', 'Admin\ArtikelController@delete');
        Route::get('artikel/status/{id}/{status}', 'Admin\ArtikelController@status');
        Route::post('artikel/update_sort', 'Admin\ArtikelController@update_sort');
        Route::get('artikel/featured/{id}/{status}', 'Admin\ArtikelController@featured');

        Route::get('informasi_category', 'Admin\InformasiCategoryController@view')
            ->name('informasi_category_view');
        Route::get('informasi_category/create', 'Admin\InformasiCategoryController@create');
        Route::get('informasi_category/edit/{id}', 'Admin\InformasiCategoryController@edit');
        Route::post('informasi_category/insert', 'Admin\InformasiCategoryController@insert');
        Route::post('informasi_category/update', 'Admin\InformasiCategoryController@update');
        Route::get('informasi_category/delete/{id}', 'Admin\InformasiCategoryController@delete');
        Route::get('informasi_category/status/{id}/{status}', 'Admin\InformasiCategoryController@status');
        Route::post('informasi_category/update_sort', 'Admin\InformasiCategoryController@update_sort');

        Route::get('informasi', 'Admin\InformasiController@view')
            ->name('informasi_view');
        Route::get('informasi/create', 'Admin\InformasiController@create');
        Route::get('informasi/edit/{id}', 'Admin\InformasiController@edit');
        Route::post('informasi/insert', 'Admin\InformasiController@insert');
        Route::post('informasi/update', 'Admin\InformasiController@update');
        Route::get('informasi/delete/{id}', 'Admin\InformasiController@delete');
        Route::get('informasi/status/{id}/{status}', 'Admin\InformasiController@status');
        Route::post('informasi/update_sort', 'Admin\InformasiController@update_sort');
        Route::get('informasi_title', 'Admin\InformasiController@title')
            ->name('informasi_title_view');
        Route::post('informasi_title/update', 'Admin\InformasiController@update_title');

        Route::get('butuhkan', 'Admin\CabangController@view')
            ->name('cabang_view');
        Route::get('butuhkan/create', 'Admin\CabangController@create');
        Route::get('butuhkan/edit/{id}', 'Admin\CabangController@edit');
        Route::post('butuhkan/insert', 'Admin\CabangController@insert');
        Route::post('butuhkan/update', 'Admin\CabangController@update');
        Route::get('butuhkan/delete/{id}', 'Admin\CabangController@delete');
        Route::get('butuhkan/status/{id}/{status}', 'Admin\CabangController@status');
        Route::post('butuhkan/update_sort', 'Admin\CabangController@update_sort');
        Route::get('butuhkan_title', 'Admin\CabangController@title')
            ->name('cabang_title_view');
        Route::post('butuhkan_title/update', 'Admin\CabangController@update_title');

        Route::get('mitra', 'Admin\MitraController@view')
            ->name('mitra_view');
        Route::get('mitra/create', 'Admin\MitraController@create');
        Route::get('mitra/edit/{id}', 'Admin\MitraController@edit');
        Route::post('mitra/insert', 'Admin\MitraController@insert');
        Route::post('mitra/update', 'Admin\MitraController@update');
        Route::get('mitra/delete/{id}', 'Admin\MitraController@delete');
        Route::get('mitra/status/{id}/{status}', 'Admin\MitraController@status');
        Route::post('mitra/update_sort', 'Admin\MitraController@update_sort');
        Route::get('mitra_title', 'Admin\MitraController@title')
            ->name('mitra_title_view');
        Route::post('mitra_title/update', 'Admin\MitraController@update_title');

        Route::get('jadwal', 'Admin\JadwalController@title')
            ->name('jadwal_view');
        Route::post('jadwal/update', 'Admin\JadwalController@update_title');

        Route::get('landasan_hukum', 'Admin\LandasanHukumController@view')
            ->name('landasan_hukum_view');
        Route::get('landasan_hukum/create', 'Admin\LandasanHukumController@create');
        Route::get('landasan_hukum/edit/{id}', 'Admin\LandasanHukumController@edit');
        Route::post('landasan_hukum/insert', 'Admin\LandasanHukumController@insert');
        Route::post('landasan_hukum/update', 'Admin\LandasanHukumController@update');
        Route::get('landasan_hukum/delete/{id}', 'Admin\LandasanHukumController@delete');
        Route::get('landasan_hukum/status/{id}/{status}', 'Admin\LandasanHukumController@status');
        Route::post('landasan_hukum/update_sort', 'Admin\LandasanHukumController@update_sort');
        Route::get('landasan_hukum_title', 'Admin\LandasanHukumController@title')
            ->name('landasan_hukum_title_view');
        Route::post('landasan_hukum_title/update', 'Admin\LandasanHukumController@update_title');

        Route::get('pengurus', 'Admin\PengurusController@view')
            ->name('pengurus_view');
        Route::get('pengurus/create', 'Admin\PengurusController@create');
        Route::get('pengurus/edit/{id}', 'Admin\PengurusController@edit');
        Route::post('pengurus/insert', 'Admin\PengurusController@insert');
        Route::post('pengurus/update', 'Admin\PengurusController@update');
        Route::get('pengurus/delete/{id}', 'Admin\PengurusController@delete');
        Route::get('pengurus/status/{id}/{status}', 'Admin\PengurusController@status');
        Route::post('pengurus/update_sort', 'Admin\PengurusController@update_sort');
        Route::get('pengurus_title', 'Admin\PengurusController@title')
            ->name('pengurus_title_view');
        Route::post('pengurus_title/update', 'Admin\PengurusController@update_title');

        Route::get('menu_rename', 'Admin\MenuController@rename')
            ->name('menu_rename_view');
        Route::get('menu_rename/edit/{id}', 'Admin\MenuController@renameEdit');
        Route::post('menu_rename/update', 'Admin\MenuController@renameUpdate');
        Route::get('menu_rename/status/{id}/{status}', 'Admin\MenuController@renamestatus');

        Route::get('tentang_kami_title', 'Admin\TentangKamiController@title')
            ->name('tentang_kami_title_view');
        Route::post('tentang_kami_title/update', 'Admin\TentangKamiController@update_title');

        Route::get('kegiatan_title', 'Admin\KegiatanController@title')
            ->name('kegiatan_title_view');
        Route::post('kegiatan_title/update', 'Admin\KegiatanController@update_title');

        Route::get('profil', 'Admin\ProfilController@view')
            ->name('profil_view');
        Route::get('profil/create', 'Admin\ProfilController@create');
        Route::get('profil/edit/{id}', 'Admin\ProfilController@edit');
        Route::post('profil/insert', 'Admin\ProfilController@insert');
        Route::post('profil/update', 'Admin\ProfilController@update');
        Route::get('profil/delete/{id}', 'Admin\ProfilController@delete');
        Route::get('profil/status/{id}/{status}', 'Admin\ProfilController@status');
        Route::post('profil/update_sort', 'Admin\ProfilController@update_sort');
        Route::get('profil_title', 'Admin\ProfilController@title')
            ->name('profil_title_view');
        Route::post('profil_title/update', 'Admin\ProfilController@update_title');
    });
});

Route::group(['prefix' => 'laravel-filemanager'], function () {
     \UniSharp\LaravelFilemanager\Lfm::routes();
 });
Route::get('/linkstorage', function () {
    Artisan::call('storage:link');
});
Route::get('/reouteclear', function () {
    Artisan::call('route:clear');
});
Route::get('/configclear', function () {
    Artisan::call('config:clear');
});
