-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jul 01, 2021 at 04:19 PM
-- Server version: 10.3.28-MariaDB-cll-lve
-- PHP Version: 7.3.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `u1444156_plj`
--

-- --------------------------------------------------------

--
-- Table structure for table `about`
--

CREATE TABLE `about` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `about`
--

INSERT INTO `about` (`id`, `content`, `image`, `created_at`, `updated_at`) VALUES
(1, '<h3 class=\"rhpdm\">MENS ORIGINAL SPORT &amp; FASHION</h3>\r\n<p>Shopee : BRANDEDDTERMURAH<br />WA : 0812 8698 8388 (NO CALL)<br />LINE :&nbsp;<a class=\"notranslate\" href=\"https://www.instagram.com/brandedtermurah/\">@brandedtermurah</a>&nbsp;(use &ldquo;@&ldquo;)<br />Cek RESI (IG) :&nbsp;<a class=\"notranslate\" href=\"https://www.instagram.com/resi.testi.branded/\">@resi.testi.branded</a><br />Fast Response &darr; &darr;<a class=\"yLUwa\" href=\"https://l.instagram.com/?u=http%3A%2F%2Fgoo.gl%2Fzh3vuy&amp;e=ATOP5DoGHm1nh1wdaZ6z6e1Dsc-5-LMRZkTibwTWgcGMXC4SqmkCdOzEyrvhHPUeLzEwUmrza8ZIJ9c&amp;s=1\" target=\"_blank\" rel=\"me nofollow noopener noreferrer\">goo.gl/zh3vuy</a></p>\r\n<p>&nbsp;</p>\r\n<p>Contoh About</p>', NULL, '2019-12-17 00:38:26', '2020-02-13 11:27:16');

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `last_login` datetime NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `username`, `password`, `email`, `status`, `last_login`, `created_at`, `updated_at`) VALUES
(1, 'admin@pljindonesia.org', 'd9c3615e51efe36df7a4de4e0bd3af52', 'admin@pljindonesia.org', 1, '2021-07-01 09:18:20', NULL, '2021-07-01 02:18:20');

-- --------------------------------------------------------

--
-- Table structure for table `admin_color`
--

CREATE TABLE `admin_color` (
  `id` int(11) NOT NULL,
  `header` varchar(191) DEFAULT NULL,
  `sidebar` varchar(191) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin_color`
--

INSERT INTO `admin_color` (`id`, `header`, `sidebar`, `created_at`, `updated_at`) VALUES
(1, '#003e70', '#005495', '2020-10-21 14:32:39', '2021-06-23 06:17:07');

-- --------------------------------------------------------

--
-- Table structure for table `apakah`
--

CREATE TABLE `apakah` (
  `id` int(11) NOT NULL,
  `image` varchar(191) DEFAULT NULL,
  `title` varchar(191) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `apakah`
--

INSERT INTO `apakah` (`id`, `image`, `title`, `description`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Apa itu PLJ?', '<p>PLJ (Pusat Layanan Juru Bahasa Isyarat) adalah lembaga yang mengelola juru bahasa isyarat di Indonesia yang dikelola oleh tuli komunitas pengguna bahasa isyarat. Dengan adanya lembaga ini diharapkan pemerintah, lembaga/institusi ataupun masyarakat pada umumnya serta warga masyarakat tuli pada khususnya tidak lagi mengalami kebingungan dalam mencari referensi juru bahasa isyarat yang berkompeten serta bertanggung jawab.</p>', '2021-06-15 08:46:30', '2021-06-28 10:08:51');

-- --------------------------------------------------------

--
-- Table structure for table `artikel`
--

CREATE TABLE `artikel` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `banner` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `view` int(11) DEFAULT NULL,
  `short_description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` date DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 0,
  `sumber` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `featured` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `artikel_kategori`
--

CREATE TABLE `artikel_kategori` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `artikel_id` int(11) DEFAULT NULL,
  `kategori_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `bank`
--

CREATE TABLE `bank` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `bank` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sort` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `bank`
--

INSERT INTO `bank` (`id`, `bank`, `image`, `created_at`, `updated_at`, `deleted_at`, `slug`, `sort`) VALUES
(1, 'BCA', 'bca.png', '2020-10-14 04:25:42', '2020-10-16 08:47:35', NULL, 'bca', 1),
(2, 'MANDIRI', 'mandiri.png', '2020-10-16 08:47:50', '2020-10-16 08:47:50', NULL, 'mandiri', 2);

-- --------------------------------------------------------

--
-- Table structure for table `banner`
--

CREATE TABLE `banner` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 0,
  `link` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `type` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `banner`
--

INSERT INTO `banner` (`id`, `title`, `slug`, `image`, `status`, `link`, `link_name`, `description`, `created_at`, `updated_at`, `deleted_at`, `sort`, `type`) VALUES
(1, '<p>Menghapus Batasan Bahasa Melalui Juru Bahasa Isyarat</p>', 'pmenghapus-batasan-bahasa-melalui-juru-bahasa-isyaratp', 'pmenghapus-batasan-bahasa-melalui-juru-bahasa-isyaratp.jpg', 1, 'https://wa.link/3l544p', 'Pesan JBI', '<p>Dengan tersedia Juru Bahasa Isyarat maka hak memperoleh informasi untuk Tuli dapat terpenuhi.</p>', '2021-06-15 01:46:15', '2021-06-20 22:56:40', NULL, 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cabang`
--

CREATE TABLE `cabang` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 0,
  `whatsapp` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cabang`
--

INSERT INTO `cabang` (`id`, `name`, `image`, `status`, `whatsapp`, `description`, `created_at`, `updated_at`, `deleted_at`, `sort`, `slug`, `link`, `link_name`) VALUES
(1, 'Pemintaan JBI', 'pemintaan-jbi_cabang.png', 1, NULL, NULL, '2021-06-15 01:49:56', '2021-06-18 09:03:32', NULL, 1, 'pemintaan-jbi', '#', NULL),
(2, 'Perekrutan JBI', 'perekrutan-jbi_cabang.png', 1, NULL, NULL, '2021-06-15 01:50:07', '2021-06-18 09:03:37', NULL, 2, 'perekrutan-jbi', '#', NULL),
(3, 'Saran / Keluhan', 'saran-keluhan_cabang.png', 1, NULL, NULL, '2021-06-15 01:50:17', '2021-06-18 09:03:42', NULL, 3, 'saran-keluhan', '#', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cabang_title`
--

CREATE TABLE `cabang_title` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cabang_title`
--

INSERT INTO `cabang_title` (`id`, `title`, `link`, `description`, `created_at`, `updated_at`, `deleted_at`, `slug`, `image`) VALUES
(1, 'Apa yang Anda Butuhkan?', NULL, '<p>PLJ siap melayani dan menyediakan berbagai hal sesuai dengan kebutuhan Anda</p>', '2021-06-15 01:49:04', '2021-06-15 01:49:04', NULL, 'apa-yang-anda-butuhkan', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cara_kerja`
--

CREATE TABLE `cara_kerja` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 0,
  `link` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `company_data`
--

CREATE TABLE `company_data` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `facebook` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `instagram` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pinterest` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bridestory` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `line` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `whatsapp` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hours` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telephone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `company_data`
--

INSERT INTO `company_data` (`id`, `facebook`, `instagram`, `pinterest`, `bridestory`, `email`, `line`, `whatsapp`, `address`, `hours`, `telephone`, `created_at`, `updated_at`) VALUES
(1, NULL, NULL, NULL, NULL, 'lakucreative@gmail.com', NULL, '+6282111157461', '', '', '', '2019-12-17 20:06:31', '2020-10-16 10:27:17');

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE `contact` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `contact`
--

INSERT INTO `contact` (`id`, `first_name`, `last_name`, `email`, `phone`, `message`, `created_at`, `updated_at`) VALUES
(10, 'stev', 'stev', 'stev3nlee@yahoo.com', '7575756', 'test', '2020-07-24 09:38:30', '2020-07-24 09:38:30');

-- --------------------------------------------------------

--
-- Table structure for table `dasar_pemikiran`
--

CREATE TABLE `dasar_pemikiran` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 0,
  `link` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `dasar_pemikiran`
--

INSERT INTO `dasar_pemikiran` (`id`, `title`, `image`, `status`, `link`, `description`, `created_at`, `updated_at`, `deleted_at`, `sort`, `slug`, `type`) VALUES
(1, NULL, NULL, 1, NULL, '<p>Disabilitas adalah orang yang mengalami hambatan karena lingkungannya tidak mendukung atau tidak menyediakan akses/fasilitas. Juru bahasa Isyarat merupakan suatu media fasilitas aksesibilitas bagi warga Tuli/tunarungu untuk memenuhi haknya mendapatkan informasi dan komunikasi yang setara.</p>\r\n<p>&nbsp;</p>\r\n<p>Tanpa juru bahasa Isyarat, warga Tuli/tunarungu tidak akan dapat mengakses informasi dengan maksimal, yang mengakibatkan hambatan dan kemunduran warga Tuli/tunarungu di semua aspek kehidupannya.</p>', '2021-06-17 21:34:04', '2021-06-18 09:37:35', NULL, 1, '-1', NULL),
(2, NULL, NULL, 1, NULL, '<p>Di Indonesia belum ada juru bahasa Isyarat yang memiliki sertifikat, baru ada sukarelawan atau magang juru bahasa Isyarat yang dilatih oleh orang Tuli/tunarungu dalam pergaulan sehari-hari. Sukarelawan juru bahasa Isyarat dilatih dan dinilai oleh komunitas Tuli/tunarungu. Juru bahasa Isyarat juga wajib mematuhi aturan dan kode etik demi kenyamanan warga Tuli/tunarungu. Dalam Convention on the Rights of Persons with Disabilities (CRPD) yang dituangkan dalam Undang-Undang No. 19 Tahun 2011, banyak penekanan tentang hak bagi warga Tuli/tunarungu yang menyangkut bahasa Isyarat dan linguistik. Untuk itulah Pusat Layanan Juru Bahasa Isyarat ini didirikan.</p>', '2021-06-17 21:34:16', '2021-06-22 04:47:57', NULL, 2, '-2', NULL),
(3, NULL, NULL, 1, NULL, '<p>PLJ didirikan oleh warga Tuli/tunarungu melalui workshop, diskusi yang diikuti oleh para ketua DPD (Dewan Pengurus Daerah) GERKATIN, instruktur Bahasa Isyarat Indonesia dari Pusat Bahasa Isyarat Indonesia (Pusbisindo) dan Laboratorium Riset Bahasa Isyarat (LRBI) Fakultas Ilmu Pengetahuan Budaya Universitas Indonesia, dan beberapa juru bahasa Isyarat lokal.</p>', '2021-06-17 21:34:20', '2021-06-22 04:48:08', NULL, 3, '-3', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `dasar_pemikiran_image`
--

CREATE TABLE `dasar_pemikiran_image` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `dasar_pemikiran_image`
--

INSERT INTO `dasar_pemikiran_image` (`id`, `created_at`, `updated_at`, `deleted_at`, `image`) VALUES
(1, '2021-06-17 21:33:05', '2021-06-17 21:33:05', NULL, 'dasar_pemikiran_1888401051.png');

-- --------------------------------------------------------

--
-- Table structure for table `galeri`
--

CREATE TABLE `galeri` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 0,
  `link` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `galeri`
--

INSERT INTO `galeri` (`id`, `title`, `image`, `status`, `link`, `description`, `created_at`, `updated_at`, `deleted_at`, `sort`, `slug`, `type`) VALUES
(1, '1', NULL, 1, NULL, NULL, '2021-06-17 21:26:38', '2021-06-17 22:10:27', '2021-06-17 22:10:27', 1, '1-1', NULL),
(2, 'Acara Deafblind', 'acara-ulang-tahun-plj-ke-5-1.jpg', 1, NULL, '<p>Berkolaborasi dengan Komunitas Tulibuta, Pelita dalam menyediakan juru bahasa isyarat</p>', '2021-06-17 21:26:54', '2021-06-21 00:47:26', NULL, 2, 'acara-deafblind', NULL),
(3, 'Acara Ulang Tahun PLJ ke - 5', 'acara-ulang-tahun-plj-ke-5-2.jpg', 1, NULL, '<p>PLJ menyelenggarakan Ulang Tahun ke-5 dengan mengundang klien dan mitra penting PLJ</p>', '2021-06-17 21:27:12', '2021-06-17 21:27:12', NULL, 3, 'acara-ulang-tahun-plj-ke-5-2', NULL),
(4, '1', '', 1, NULL, NULL, '2021-06-17 22:08:11', '2021-06-17 22:08:22', '2021-06-17 22:08:22', 4, '1', NULL),
(5, 'Acara Ulang Tahun PLJ ke - 5', 'acara-ulang-tahun-plj-ke-5.jpg', 1, NULL, '<p>PLJ menyelenggarakan Ulang Tahun ke-5 dengan mengundang klien dan mitra penting PLJ</p>', '2021-06-18 09:28:26', '2021-06-18 09:28:27', NULL, 4, 'acara-ulang-tahun-plj-ke-5', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `galeri_title`
--

CREATE TABLE `galeri_title` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `galeri_title`
--

INSERT INTO `galeri_title` (`id`, `title`, `image`, `link`, `description`, `created_at`, `updated_at`, `deleted_at`, `slug`, `link_name`) VALUES
(1, 'Galeri Kegiatan', NULL, NULL, NULL, '2021-06-15 01:51:24', '2021-06-17 22:04:26', NULL, 'galeri-kegiatan', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `hubungi_kami`
--

CREATE TABLE `hubungi_kami` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `pesan_link` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pesan_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `chat_link` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `chat_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `chat_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `hubungi_kami`
--

INSERT INTO `hubungi_kami` (`id`, `pesan_link`, `pesan_image`, `chat_link`, `chat_name`, `chat_image`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, NULL, NULL, 'https://pljindonesia.org/', 'Chat & Order', '534880970111dbcb7f4bddfec22bca59.png', '2021-06-17 22:19:00', '2021-06-18 08:43:56', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `informasi`
--

CREATE TABLE `informasi` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `informasi_category_id` bigint(20) UNSIGNED DEFAULT NULL,
  `question` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `answer` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `sort` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `informasi`
--

INSERT INTO `informasi` (`id`, `informasi_category_id`, `question`, `answer`, `deleted_at`, `created_at`, `updated_at`, `status`, `sort`) VALUES
(1, 1, 'Produser Operasi Standar', '<p>1. Seluruh JBI PLJ (Tuli &amp; Dengar) berbahasa Isyarat Indonesia (BISINDO)</p>\r\n<p>2. Jumlah JBI dalam kegiatan berdasarkan durasi: <br />&bull; Half Day (1-4jam) <br />Indonesia/Inggris-BISINDO: 2JBI <br />&bull; Full Day (4-8jam) <br />Indonesia/Inggris-BISINDO: 2JBI <br />para JBI akan bergantian menerjemahkan per 15menit selama kegiatan berlangsung.</p>', NULL, '2021-06-17 21:47:09', '2021-06-22 23:18:39', 1, 1),
(2, 2, 'Mekanisme Layanan', '<p>Mekanisme layanan juru bahasa Isyarat adalah suatu tata cara dan aturan dalam pelayanan juru bahasa Isyarat yang mengatur tentang segala hal yang berkaitan dengan prosedur berlangsungnya sebuah pelayanan juru bahasa Isyarat sebagai berikut.</p>\r\n<p><br /><strong>1. Mekanisme pemanggilan dan pembayaran juru bahasa Isyarat dilakukan oleh Pemerintah/NGO atau organisasi Tuli/tunarungu atau PLJ dengan penjelasan sebagai berikut.</strong><br />a. Pengguna layanan yang mengundang organisasi Tuli/tunarungu menyediakan layanan juru bahasa Isyarat bagi peserta Tuli/tunarungu.<br />b. Dalam hal pemilihan juru bahasa Isyarat, pengguna layanan tidak diperkenankan memilih sendiri, tetapi diserahkan kepada organisasi Tuli/tunarungu yang bersangkutan.<br />c. Organisasi Tuli/tunarungu berhak memilih juru bahasa Isyarat berdasarkan tingkat kapasitas dan kemampuan juru bahasa Isyarat.<br />d. Juru bahasa Isyarat tidak diperkenankan secara langsung mengajukan diri kepada pengguna layanan tanpa rekomendasi dari organisasi Tuli/tunarungu.<br />e. Pengguna layanan wajib memberikan honorarium dalam pelayanan juru bahasa Isyarat yang sudah diberikan oleh juru bahasa Isyarat.<br />f. Dalam hal pemberian honorarium (di luar honorarium transportasi), pengguna layanan memberikan honorarium secara langsung melalui PLJ sebagai penyedia layanan juru bahasa, bukan melalui juru bahasa Isyarat secara langsung.<br />g. Pembagian honorarium diatur dalam poin tersendiri dan besarannya diatur sesuai kesepakatan.<br />h. Dalam hal komplain terhadap pelayanan juru bahasa Isyarat, klien Tuli/tunarungu berhak mengajukan keluhan dan evaluasi terhadap jasa pelayanan juru bahasa Isyarat melalui PLJ.<br />i. Klien Tuli/tunarungu berhak menilai pelayanan juru bahasa Isyarat dan memberikan potongan atau tambahan honorarium setelah melakukan evaluasi dan mendiskusikannya dengan PLJ.<br /><br /><strong>2. Mekanisme penilaian juru bahasa Isyarat, yang dilihat dari aspek-aspek sebagai berikut.</strong><br />a. Sikap juru bahasa Isyarat.<br />b. Keakuratan informasi juru bahasa Isyarat.</p>', NULL, '2021-06-17 21:47:40', '2021-06-23 01:16:53', 1, 5),
(3, 2, 'Mekanisme Pemesanan', '<p>1. Untuk acara seperti seminar, konferensi, rapat, pada umumnya menggunakan dua orang juru bahasa Isyarat yang menjurubahasakan secara bergantian setiap 20-30 menit, bergantung pada kemampuan juru bahasa Isyarat itu sendiri.<br />2. Untuk acara seperti dialog, diskusi, wawancara, cukup memakai satu orang juru bahasa Isyarat.<br />3. Mekanisme pemesanan juru bahasa Isyarat digambarkan dengan skema berikut.</p>\r\n<p><img style=\"display: block; margin-left: auto; margin-right: auto;\" src=\"https://pljindonesia.org/storage/photos/Bagan mekanisme-05.jpg\" alt=\"bagan mekanisme\" width=\"500\" /></p>\r\n<p><strong>Keterangan:</strong><br />1. Klien melakukan pengisian formulir<br />2. Formulir diterima oleh staf PLJ<br />3. Negosiasi jika tidak ada kesepakatan<br />4. Staf administrasi PLJ mengecek kesediaan JBI<br />5. Klien membayar tanda jadi<br />6. JBI siap melayani<br />7. JBI bekerja sampai acara selesai<br />8. Klien melunasi pembayaran melalui bagian keuangan PLJ</p>', NULL, '2021-06-18 10:21:11', '2021-06-23 03:07:34', 1, 6),
(4, 2, 'Mekanisme Pembayaran', '<p>Honorarium layanan juru bahasa Isyarat adalah honorarium yang diberikan oleh pengguna atas jasa pelayanan juru bahasa Isyarat yang telah diberikan. Honorarium yang dimaksud belum termasuk honorarium transportasi yang sudah disediakan atau dianggarkan untuk juru bahasa Isyarat. Berikut ketentuan yang berkaitan dengan pemesanan dan pembayaran yang harus dilakukan</p>\r\n<p>Untuk pemesanan juru bahasa Isyarat dengan biaya pembayaran sampai dengan Rp2.000.000,00 dikenakan uang muka sebesar Rp100.000,00. Untuk pemesanan juru bahasa Isyarat dengan biaya pembayaran di atas Rp2.000.000,00 dikenakan uang muka sebanyak 10% dari total pembayaran.</p>\r\n<p><br /><strong>2. Uang muka dibayarkan setelah pemesanan, atau pada saat pemesanan.</strong><br />Pihak penyelenggara atau pemesan dapat melunasi pembayaran selambat-lambatnya satu minggu setelah acara selesai dilaksanakan.</p>\r\n<p><br /><strong>3. Setiap kegiatan atau acara yang menggunakan jasa juru bahasa Isyarat akan diberikan atau dikirimi invoice/surat tagihan dari PLJ.</strong></p>\r\n<p>Untuk pembayaran jasa juru bahasa, magang, dan relawan, dapat dilakukan melalui metode pembayaran tunai maupun transfer ke rekening atas nama:<br /><strong>GERKATIN PUSAT-PLJ</strong><br /><strong>Bank Mandiri cabang Bintaro</strong><br /><strong>No. rekening: 101.00.0726712-1</strong><br /><br /><strong>4. Besaran honorarium akan dibicarakan secara pribadi antara PLJ dengan juru bahasa Isyarat yang bersangkutan.</strong><br />Di masa yang akan datang jika sistem sertifikasi sudah berjalan, penentuan besaran jasa juru bahasa Isyarat akan disesuaikan dengan sertifikasi dan kesepakatan dengan Pusat Layanan Juru Bahasa secara tertulis. Selama belum ada kebijakan anggaran dari pemerintah, pihak pemesan akan dikenai biaya kontribusi untuk membantu operasional PLJ dan GERKATIN. Untuk mengembangkan organisasi Tuli/tunarungu, kualitas layanan juru bahasa Isyarat, dan kapasitas SDM juru bahasa Isyarat, honorarium atas jasa layanan juru bahasa Isyarat dibagi menjadi beberapa bagian dengan ketentuan sebagai berikut.</p>\r\n<p>1. Honorarium layanan juru bahasa Isyarat diberikan kepada juru bahasa Isyarat setelah dikurangi pajak.<br />2. Honorarium organisasi Tuli/tunarungu yang berasal dari klien seluruhnya menjadi hak organisasi Tuli/tunarungu daerah setempat.<br />3. Jika terdapat keluhan dalam evaluasi klien terhadap kinerja juru bahasa Isyarat, honorarium juru bahasa Isyarat dapat dipotong sesuai ketentuan. Potongan honorarium tersebut digunakan untuk pengembangan kas operasional organisasi Tuli/tunarungu daerah setempat.<br />4. Mekanisme pembagian honorarium ini bersifat fleksibel dan dapat disesuaikan dengan kebutuhan tiap-tiap organisasi Tuli/tunarungu setempat.<br />5. Semua uang, dana, atau honorarium saat ini disentralkan di PLJ pusat agar pengelolaannya dapat lebih terkontrol. Dana yang dikelola akan dikembalikan untuk daerah sesuai dengan kebutuhan tiap-tiap daerah.</p>', NULL, '2021-06-18 10:21:33', '2021-06-23 03:08:48', 1, 7),
(5, 2, 'Mekanisme Pembatalan Pemesanan', '<p>Jika terdapat adanya suatu masalah atau hal lainnya yang menyebabkan klien membatalkan pesanan, berikut adalah hal-hal yang harus diperhatikan dan dilakukan :</p>\r\n<ul>\r\n<li>Jika kegiatan atau acara yang diadakan oleh klien ternyata tidak ada peserta Tuli/tunarungu yang datang, atau acara/kegiatan dibatalkan pada hari H, pembayaran yang harus dilakukan oleh klien atau pemesan adalah sebesar 50% dari total pembayaran yang disepakati. Pembayaran tersebut digunakan untuk membayar transportasi juru bahasa Isyarat dan biaya administrasi.</li>\r\n<li>Jika pembatalan tiga hari sebelum hari diadakannya acara, uang muka yang telah dibayarkan sebelumnya akan dikembalikan kepada klien.</li>\r\n<li>Jika pembatalan dilakukan satu hari sebelum diadakannya acara, uang muka yang telah dibayarkan tidak akan dikembalikan kepada klien.</li>\r\n<li>Jika juru bahasa Isyarat mendadak berhalangan dan tidak dapat bertugas, akan diusahakan penggantian tenaga juru bahasa Isyarat oleh PLJ.</li>\r\n</ul>', NULL, '2021-06-18 10:21:47', '2021-06-23 01:16:04', 1, 8),
(6, 2, 'Mekanisme Penanganan Keluhan', '<p style=\"text-align: left;\">Setelah kegiatan, formulir saran dan kritik atau penanganan keluhan akan<br />diberikan kepada klien Tuli/tunarungu dan panitia penyelenggara.</p>', NULL, '2021-06-18 10:22:12', '2021-06-23 02:32:06', 1, 9),
(7, 3, 'Juru Bahasa Bag. 1', '<p><strong>Juru bahasa</strong><br />1. Juru bahasa adalah orang yang menjurubahasakan bahasa A ke bahasa B dan sebaliknya dengan lisan (bicara).<br /><br /><strong>2. Juru bahasa Isyarat</strong><br />Juru bahasa Isyarat adalah orang (baik orang dengar maupun Tuli/tunarungu) yang memiliki kemampuan menjurubahasakan bahasa Isyarat secara langsung, tepat, dan akurat, yang telah mengenyam pendidikan juru bahasa Isyarat dari organisasi Tuli/tunarungu yang diakui oleh komunitas Tuli/tunarungu setempat. Dalam hal ini, yang dimaksud dengan organisasi Tuli/tunarungu setempat adalah GERKATIN (Gerakan untuk Kesejahteraan Tunarungu Indonesia).</p>\r\n<p><strong>3. Kriteria menjadi juru bahasa Isyarat</strong><br />Kriteria menjadi juru bahasa Isyarat menurut PBB, dari presentasi Rachel (juru bahasa isyarat dari New Zealand) di Universitas Indonesia pada tahun 2014 adalah sebagai berikut.<br />a. Memenuhi standar kompetensi bahasa Isyarat.<br />b. Menaati kode etik.<br />c. Memiliki sikap yang baik.</p>\r\n<p><strong>4. Kategori juru bahasa Isyarat</strong><br />Juru bahasa Isyarat dalam hal ini dibagi menjadi beberapa kategori dengan penjelasannya sebagai berikut.<br />a. Juru bahasa lisan Bahasa Indonesia<br />Juru bahasa lisan Bahasa Indonesia adalah seorang yang memiliki kemampuan menjurubahasakan dengan metode oral untuk Tuli/tunarungu yang memiliki kemampuan berbicara dan membaca gerakan bibir.<br />b. Juru bahasa Isyarat bersertifikasi dan terspesialisasi<br />Juru bahasa Isyarat bersertifikasi dan terspesialisasi adalah juru bahasa Isyarat yang memiliki keahlian khusus misalnya bekerja di kepolisian dan pengadilan tinggi.<br />c. Juru bahasa Isyarat Tuli/tunarungu<br />Juru bahasa Isyarat Tuli/tunarungu adalah seorang Tuli/tunarungu yang mampu menjadi juru bahasa Isyarat bagi Tuli/tunarungu yang lainnya.<br />d. Juru bahasa Isyarat umum<br />Juru bahasa Isyarat umum adalah seorang yang memiliki kemampuan dalam memahami dan bisa berkomunikasi dengan bahasa Isyarat atau yang sedang menjalani pelatihan maupun magang juru bahasa Isyarat dari organisasi Tuli/tunarungu. Juru bahasa Isyarat umum menjurubahasakan klien Tuli/tunarungu di sekolah, rumah sakit, rapat umum, dan aktivitas pelayanan publik lainnya.</p>\r\n<p><strong>5. Jenjang/klasifikasi anggota juru bahasa Isyarat</strong><br />Adapun jenjang atau klasifikasi juru bahasa Isyarat dibagi dalam tahapan sebagai berikut.<br />a. Juru bahasa Isyarat profesional adalah juru bahasa Isyarat yang sudah memenuhi kriteria profesional dan layak mendapatkan honor.<br />b. Magang adalah juru bahasa Isyarat yang sedang menuju profesional dan tidak wajib menerima honor.<br />c. Sukarelawan tidak termasuk dalam ketetapan masalah pembayaran.</p>\r\n<p><strong>6. Syarat-syarat menjadi juru bahasa Isyarat</strong><br />a. Juru bahasa Isyarat profesional<br />i. Juru bahasa Isyarat profesional memiliki jumlah jam terbang yang telah ditentukan dan menguasai bahasa bidang hukum, kesehatan, pendidikan, dan bidang lainnya.<br />ii. Memiliki sertifikat pelatihan spesialisasi.<br />iii. Memiliki sertifikat dari Lembaga Sertifikasi Juru Bahasa Isyarat.<br />iv. Mampu beradaptasi dan aktif dalam komunitas Tuli/tunarungu.<br />b. Magang<br />i. Juru bahasa Isyarat magang.<br />ii. Memiliki pengalaman sebagai juru bahasa Isyarat.<br />iii. Memiliki sertifikat pelatihan juru bahasa Isyarat.<br />iv. Aktif dalam komunitas Tuli/tunarungu.<br />c. Relawan<br />i. Juru bahasa Isyarat relawan tidak terikat waktu dan pembayaran.<br />ii. Memiliki pengalaman berkomunikasi dengan komunitas Tuli/tunarungu.<br />iii. Memiliki sertifikat sebagai relawan dari PLJ atau sertifikat pelatihan bahasa Isyarat.</p>\r\n<p><strong>7. Perekrutan juru bahasa Isyarat</strong><br />Perekrutan juru bahasa Isyarat dapat dilakukan dengan cara-cara sebagai berikut.<br />a. Inisiatif sendiri.<br />b. Usulan dari guru bahasa Isyarat (Pusat Bahasa Isyarat Indonesia/Pusbisindo).<br />c. Usulan dari komunitas Tuli/tunarungu (DPP, DPD, dan DPC GERKATIN).</p>\r\n<p><strong>8. Syarat perekrutan juru bahasa Isyarat</strong><br />Untuk dapat direkrut menjadi juru bahasa Isyarat di PLJ, calon juru bahasa Isyarat harus memenuhi persyaratan sebagai berikut.<br />a. Mengisi formulir pendaftaran<br />b. Melengkapi syarat-syarat yang ditentukan oleh PLJ berupa:<br />i. sanggup mengikuti pelatihan juru bahasa Isyarat,<br />ii. aktif dalam komunitas Tuli/tunarungu, dan<br />iii. hormat atau loyal pada komunitas Tuli/tunarungu.<br />c. Mengikuti tes bahasa Isyarat</p>\r\n<p><strong>9. Pendidikan juru bahasa Isyarat</strong><br />Pendidikan juru bahasa Isyarat dilakukan melalui pendidikan informal dari komunitas Tuli/tunarungu dan formal dari Pusat Bahasa Isyarat Indonesia (Pusbisindo) dan Pusat Layanan Juru Bahasa Isyarat (PLJ) untuk mendapatkan sertifikasi juru bahasa Isyarat.</p>\r\n<p><strong>10. Sertifikasi juru bahasa Isyarat</strong><br />Untuk mendapatkan sertifikasi, calon juru bahasa Isyarat akan melalui berbagai penilaian yang dilakukan oleh Pusat Layanan Juru Bahasa Isyarat dan diajukan ke Lembaga Sertifikasi Juru Bahasa Isyarat.</p>', NULL, '2021-06-18 10:22:30', '2021-06-23 01:15:49', 1, 2),
(8, 3, 'Kode Etik', '<p style=\"text-align: left;\"><strong>Pasal 1</strong><br /><strong>Pengertian Kode Etik Juru Bahasa Isyarat</strong><br />Kode etik adalah segala tata aturan juru bahasa Isyarat yang ditulis dan dibuat berdasarkan prinsip-prinsip moral yang sudah disepakati oleh GERKATIN dan beberapa organisasi Tuli/tunarungu yang terlibat. Keberadaan kode etik juru bahasa Isyarat Indonesia ini disebabkan oleh mendesaknya kebutuhan Tuli/tunarungu akan pemenuhan dan perlindungan hak-hak Tuli/tunarungu atas kesetaraan akses informasi.</p>\r\n<p style=\"text-align: left;\"><strong>Pasal 2</strong><br /><strong>Prinsip-prinsip Juru Bahasa Isyarat</strong><br />Adapun tujuh prinsip utama dalam Kode Etik Juru Bahasa Isyarat yang harus menjadi pegangan, adalah sebagai berikut.<br />a. Profesionalisme (Profesionalism)<br />Profesionalisme seorang juru bahasa Isyarat harus dijaga. Hal ini berkaitan dengan kemampuan dan pengetahuan juru bahasa Isyarat dalam mengolah dan menjurubahasakan muatan informasi secara akurat.<br />b. Netralisme (Neutralism)<br />Sikap netral juru bahasa Isyarat ketika bekerja harus dijaga.<br />c. Menghormati Tuli/tunarungu (Respect to Deaf)<br />Juru bahasa Isyarat wajib menghormati klien Tuli/tunarungu.<br />d. Menghormati Sesama Juru Bahasa Isyarat (Respect to Others)<br />Juru bahasa Isyarat wajib menghormati dan menghargai sesama kolega juru bahasa Isyarat, calon juru bahasa Isyarat junior maupun mahasiswa magang juru bahasa Isyarat.<br />e. Etika Bisnis (Business Ethic)<br />Juru bahasa Isyarat wajib memahami etika bisnis dalam menentukan tarif juru bahasa Isyarat dengan layak dan masuk akal.<br />f. Kerahasiaan (Confidential)<br />Juru bahasa Isyarat wajib menjaga kerahasiaan klien.<br />g. Pengembangan Profesi (Professional Development)<br />Juru bahasa Isyarat perlu meningkatkan kemampuan dan pengetahuan melalui pelatihan atau pendidikan level lebih tinggi.</p>\r\n<p style=\"text-align: left;\"><strong>Pasal 3</strong><br /><strong>Etika Juru Bahasa Isyarat</strong><br />Adapun etika juru bahasa Isyarat harus menaati peraturan tata tertib sebagai berikut.<br />a. Juru bahasa Isyarat harus bersikap sopan, jujur, dan terbuka.<br />b. Juru bahasa Isyarat harus datang tepat waktu sesuai jadwal yang sudah ditentukan.<br />c. Juru bahasa Isyarat menggunakan pakaian yang formal, rapi, sopan, tidak ketat dengan warna yang tidak mencolok, tidak memakai aksesoris dan make-up yang berlebihan. Warna pakaian yang digunakan adalah warna yang gelap dan tidak bercorak (polos).<br />d. Juru bahasa Isyarat harus memiliki pengetahuan dan kemampuan untuk menjurubahasakan Bahasa Indonesia ke dalam Bisindo yang dipahami klien Tuli/tunarungu (atau sebaliknya) dengan baik dan benar.<br />e. Juru bahasa Isyarat mampu menjurubahasakan secara tepat dan akurat, tanpa mengurangi ataupun menambah muatan informasi yang diperoleh.<br />f. Juru bahasa Isyarat dituntut mampu memperjuangkan hak-hak klien Tuli/tunarungu supaya mendapatkan kedudukan yang sama dengan orang lain (misalnya hak untuk bertanya, mengutarakan pendapat, melakukan klarifikasi jawaban atau hal-hal yang lainnya).<br />g. Juru bahasa Isyarat dilarang ikut campur dalam memberikan jawaban, opini atau pertanyaan yang disampaikan klien Tuli/tunarungu serta tidak diperkenankan memberikan saran atau pendapat pribadinya yang bersifat menggurui klien Tuli/tunarungu.<br />h. Apabila klien Tuli/tunarungu mengalami kesulitan dalam pemahaman kosa kata atau suatu konteks informasi, juru bahasa Isyarat harus menjelaskan kepada klien di waktu-waktu yang memungkinkan untuk memberikan penjelasan secara terperinci.<br />i. Juru bahasa Isyarat harus terbuka dalam menerima keluhan/kritikan/saran dari klien Tuli/tunarungu mengenai kualitas juru bahasa Isyarat, muatan informasi dan kemampuan dalam juru bahasa Isyarat serta mau melakukan perbaikan demi menjaga profesionalitas seorang juru bahasa Isyarat.<br />j. Juru bahasa Isyarat tidak diperkenankan melakukan interaksi dan komunikasi dengan orang lain yang bersifat urusan/kepentingan pribadi pada saat melaksanakan tugas.<br /><br /><strong>Pasal 4</strong><br /><strong>Hak dan Kewajiban Juru Bahasa Isyarat</strong><br />Kewajiban seorang juru bahasa Isyarat adalah sebagai berikut.<br />a. Memberikan pelayanan juru bahasa Isyarat secara tepat dan akurat dari Bahasa Indonesia lisan ke Bisindo atau sebaliknya.<br />b. Memperjuangkan dan memastikan hak-hak klien Tuli/tunarungu terpenuhi.<br /><br /><strong>Hak seorang juru bahasa Isyarat adalah sebagai berikut.</strong><br />a. Istirahat pada waktunya.<br />b. Ada pergantian juru bahasa Isyarat pada periode waktu yang ditentukan.<br />c. Dihargai secara layak.<br />d. Juru bahasa Isyarat berhak memperoleh pelatihan dan pendidikan lanjutan.<br />e. Juru bahasa Isyarat mendapatkan perlakuan yang manusiawi.<br />f. Mendapat/memperoleh imbalan jasa yang sesuai dengan ketentuan yang berlaku.<br /><br /><strong>Pasal 5</strong><br /><strong>Hak dan Kewajiban Klien (Tuli/tunarungu)</strong><br />Hak seorang klien adalah sebagai berikut.<br />a. Mendapatkan jasa pelayanan juru bahasa Isyarat.<br />b. Memberikan evaluasi terhadap jasa pelayanan juru bahasa Isyarat.<br />c. Mendapat pelayanan dan perlakuan yang sama dengan orang dengar.<br /><br />Kewajiban seorang klien adalah memberikan penilaian dan honorarium yang telah disepakati terhadap jasa pelayanan juru bahasa Isyarat yang sudah diberikan oleh juru bahasa Isyarat.<br /><br /><strong>Pasal 6</strong><br /><strong>Larangan Juru Bahasa Isyarat</strong><br />Juru bahasa Isyarat dilarang melakukan tindakan-tindakan sebagai berikut.<br />a. Juru bahasa Isyarat dilarang untuk mencermarkan nama baik klien, mitra PLJ, maupun organisasi PLJ.<br />b. Juru bahasa Isyarat dilarang meminta biaya lain di luar dari kesepakatan jasa tanpa sepengetahuan PLJ.<br />c. Juru bahasa Isyarat dilarang melakukan tawar-menawar dengan klien.<br />d. Juru bahasa Isyarat dilarang merebut klien, merangkap pekerjaan juru bahasa Isyarat dan bisnis pada saat bekerja.<br />e. Juru bahasa Isyarat dilarang meninggalkan tugas tanpa pemberitahuan selama bekerja.<br />f. Juru bahasa Isyarat dilarang merebut/memotong pembicaraan klien dengan orang dengar.<br />g. Juru bahasa Isyarat dilarang memanfaatkan klien Tuli/tunarungu selama bertugas.<br />h. Juru bahasa Isyarat dilarang menilai atau menyebutkan diri sendiri sebagai juru bahasa Isyarat yang profesional.<br />i. Juru bahasa Isyarat dilarang marah atau tersinggung jika klien meminta penggantian juru bahasa.<br />j. Juru bahasa Isyarat dilarang menetapkan dan membebankan biaya-biaya kepada klien dalam layanan.<br />k. Juru bahasa Isyarat dilarang menelantarkan klien.<br />l. Juru bahasa Isyarat dilarang merahasiakan biaya ataupun perjanjian dengan pihak penyelenggara tanpa sepengetahuan PLJ.<br />m. Juru bahasa Isyarat tidak boleh membocorkan rahasia pribadi Tuli/tunarungu.<br />n. Juru bahasa Isyarat tidak boleh marah atau membentak kepada Tuli/tunarungu, jika Tuli/tunarungu tidak memperhatikan juru bahasa Isyarat.<br />o. Juru bahasa Isyarat dilarang menjurubahasakan untuk kepentingan pribadi tanpa ada persetujuan dari klien dan PLJ.<br /><br /><strong>Pasal 7</strong><br /><strong>Hubungan Juru Bahasa Isyarat dengan Klien</strong><br />Untuk menjaga hubungan antara antara juru bahasa Isyarat dengan klien Tuli/tunarungu, terdapat beberapa hal yang harus dilakukan sebagai berikut.<br />a. Juru bahasa Isyarat membina hubungan baik dengan klien.<br />b. Juru bahasa Isyarat harus sabar dalam memberikan layanan kepada klien.<br />c. Juru bahasa Isyarat harus membangun rasa percaya diri klien Tuli/tunarungu.<br />d. Juru bahasa Isyarat harus menghormati pengetahuan, sikap, kebiasaan, budaya dan nilai yang dimiliki klien.<br />e. Juru bahasa Isyarat melibatkan klien jika terdapat istilah dalam bahasa Isyarat yang tidak diketahui oleh juru bahasa Isyarat.<br />f. Juru bahasa Isyarat wajib melakukan tindakan yang sesuai dengan hukum dan prosedur yang berlaku.<br />g. Juru bahasa Isyarat harus memberikan informasi yang sebenar-benarnya kepada klien.<br />h. Juru bahasa Isyarat harus menampung kritik dan saran yang disampaikan klien dan meneruskannya ke PLJ<br /><br /><strong>Pasal 8</strong><br /><strong>Hubungan Juru Bahasa Isyarat dengan Juru Bahasa Isyarat</strong><br />Untuk menjaga hubungan antara juru bahasa Isyarat dengan sesama juru bahasa Isyarat lainnya, terdapat beberapa hal yang harus dilakukan sebagai berikut.<br />a. Setiap juru bahasa Isyarat menjalani hubungan baik sesama juru bahasa dengan sikap saling menghargai dan menghormati.<br />b. Setiap juru bahasa Isyarat saling membantu dan bekerja sama dalam memberikan layanan.<br />c. Juru bahasa Isyarat tidak boleh berebut klien yang didampingi dari juru bahasa Isyarat lain.<br /><br /><strong>Pasal 9</strong><br /><strong>Sanksi Juru Bahasa Isyarat</strong><br />Sanksi terhadap juru bahasa Isyarat yang dikeluarkan oleh Tim PLJ terdiri atas:<br />a. teguran lisan,<br />b. teguran tertulis,<br />c. penonaktifan sementara/skorsing, dan<br />d. dinonaktifkan.</p>\r\n<p style=\"text-align: left;\"><strong>Pasal 10</strong><br /><strong>Pemberhentian Juru Bahasa Isyarat</strong><br />Juru bahasa Isyarat dinyatakan berhenti apabila:<br />a. meninggal dunia;<br />b. berhenti atas permintaan sendiri atau mengundurkan diri secara tertulis;<br />c. dinonaktifkan oleh Dewan Pengurus Pusat GERKATIN atas usul Ketua PLJ karena yang bersangkutan melanggar Anggaran Dasar/Anggaran Rumah Tangga, peraturan organisasi dan/atau beberapa kali membuat kesalahan yang merugikan nama baik klien secara sengaja;<br />d. menjadi anggota/pengurus organisasi lain;<br />e. melanggar hak Tuli/tunarungu, melakukan penipuan atau memanfaatkan Tuli/tunarungu, membocorkan rahasia pribadi Tuli/tunarungu, melakukan bisnis untuk kepentingan pribadi;<br />f. mencemarkan nama baik warga Tuli/tunarungu, organisasi Tuli/tunarungu atau PLJ; dan<br />g. mengadu domba antarkomunitas Tuli/tunarungu atau organisasi Tuli/tunarungu.</p>', NULL, '2021-06-18 10:22:43', '2021-06-23 06:04:51', 1, 4),
(9, 3, 'Juru Bahasa Bag. 2', '<p><strong>11. Mekanisme sertifikasi juru bahasa Isyarat</strong><br />Dalam memperoleh sertifikasi untuk juru bahasa Isyarat profesional, terdapat beberapa tahapan yang harus dilalui sebagai berikut.</p>\r\n<p><strong>a. Proses sertifikasi juru bahasa Isyarat</strong><br />i. Mengisi formulir pendaftaran sertifikasi dan melengkapi syarat-syarat sertifikasi.<br />ii. Mendapat jumlah nilai dari guru.<br />iii. Memenuhi jumlah jam terbang yang telah ditentukan.<br />iv. Memenuhi masa adaptasi dengan organisasi Tuli/tunarungu atau perseorangan selama 6-12 bulan.<br />v. Konduite yang baik.</p>\r\n<p><strong>b. Tahapan ujian sertifikasi juru bahasa Isyarat</strong><br />i. Ujian mendengar yang dilanjutkan dengan ujian menjurubahasakan bahasa lisan ke dalam bahasa Isyarat.<br />ii. Ujian menjurubahasakan bahasa Isyarat ke dalam bahasa lisan.<br />iii. Ujian berisyarat tentang pemahaman terhadap orang Tuli/tunarungu.<br />iv. Ujian tanya jawab.<br />v. Konduite selama ujian.</p>\r\n<p><strong>c. Nilai yang diperoleh dari ujian sertifikasi juru bahasa Isyarat</strong><br />i. Kemampuan mengisyaratkan dari pembicara lisan.<br />ii. Kemampuan memahami bahasa Isyarat orang Tuli/tunarungu dan menjurubahasakan dalam bahasa lisan.<br />iii. Menaati kode etik/konduite yang baik.<br />iv. Masa magang dan belajar di komunitas Tuli/tunarungu.</p>\r\n<p>d. Tim penguji untuk sertifikasi juru bahasa Isyarat (Lembaga Sertifikasi Juru Bahasa Isyarat)</p>\r\n<p><strong>Pengujian sertifikasi juru bahasa Isyarat dilakukan oleh Lembaga Sertifikasi Juru Bahasa Isyarat yang terdiri atas tim-tim sebagai berikut.</strong><br />i. Laboratorium Riset Bahasa Isyarat Fakultas Ilmu Pengetahuan Budaya Universitas Indonesia (LRBI FIB UI).<br />ii. Pusat Layanan Juru Bahasa Isyarat (PLJ).<br />iii. Pusat Bahasa Isyarat Indonesia (Pusbisindo).<br />iv. Koordinator juru bahasa Isyarat.<br />v. Kementerian terkait.</p>\r\n<p><strong>e. Penandatanganan sertifikat juru bahasa Isyarat</strong><br />Sertifikat ditandatangani oleh:<br />i. Lembaga Sertifikasi Juru Bahasa Isyarat, dan<br />ii. Kementerian yang bersangkutan.<br />f. Pengambilan sumpah juru bahasa Isyarat<br />Pengambilan sumpah juru bahasa Isyarat dilakukan juru bahasa Isyarat profesional dengan pejabat negara yang bersangkutan.</p>\r\n<p><strong>g. Evaluasi juru bahasa Isyarat</strong><br />Evaluasi juru bahasa Isyarat dilakukan untuk meningkatkan kualitas pelayanan dan kemampuan juru bahasa Isyarat dilakukan dengan:<br />i. penilaian juru bahasa dalam memberikan layanan dilakukan oleh pengguna melalui mekanisme yang telah disediakan oleh PLJ, dan<br />ii. penilaian dilakukan secara berkala oleh Pusat Layanan Juru Bahasa Isyarat melalui mekanisme yang telah disediakan oleh PLJ.</p>', NULL, '2021-06-22 23:38:03', '2021-06-23 01:15:59', 1, 3);

-- --------------------------------------------------------

--
-- Table structure for table `informasi_category`
--

CREATE TABLE `informasi_category` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `informasi_category`
--

INSERT INTO `informasi_category` (`id`, `name`, `slug`, `sort`, `status`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'SOP', 'sop', 1, 1, NULL, '2021-06-17 21:46:04', '2021-06-17 21:46:04'),
(2, 'Mekanisme', 'mekanisme', 3, 1, NULL, '2021-06-17 21:46:13', '2021-06-23 01:10:35'),
(3, 'Panduan JBI', 'panduan-jbi', 2, 1, NULL, '2021-06-17 21:46:25', '2021-06-23 01:10:35');

-- --------------------------------------------------------

--
-- Table structure for table `informasi_title`
--

CREATE TABLE `informasi_title` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `informasi_title`
--

INSERT INTO `informasi_title` (`id`, `title`, `image`, `link`, `description`, `created_at`, `updated_at`, `deleted_at`, `slug`) VALUES
(1, 'Informasi', 'informasi_857951180.jpg', NULL, NULL, '2021-06-18 10:35:19', '2021-06-18 10:35:19', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `jadwal`
--

CREATE TABLE `jadwal` (
  `id` int(11) NOT NULL,
  `image` varchar(191) DEFAULT NULL,
  `title` varchar(191) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jadwal`
--

INSERT INTO `jadwal` (`id`, `image`, `title`, `description`, `created_at`, `updated_at`) VALUES
(1, 'jadwal_1602308270.jpg', 'Jadwal Kegiatan JBI On TV', '<p>Sebagai bentuk transparansi kerja sama klien kami dengan Juru Bahasa Isyarat PLJ, terlampir jadwal kerja JBI on TV</p>', '2021-06-18 05:16:12', '2021-06-18 05:16:28');

-- --------------------------------------------------------

--
-- Table structure for table `kategori`
--

CREATE TABLE `kategori` (
  `id` int(11) NOT NULL,
  `name` varchar(191) DEFAULT NULL,
  `slug` varchar(191) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `kegiatan_title`
--

CREATE TABLE `kegiatan_title` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `kegiatan_title`
--

INSERT INTO `kegiatan_title` (`id`, `title`, `image`, `link`, `description`, `created_at`, `updated_at`, `deleted_at`, `slug`) VALUES
(1, 'Kegiatan', 'kegiatan_140268914.jpg', NULL, NULL, '2021-06-17 21:50:01', '2021-06-18 10:44:23', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `kerjasama`
--

CREATE TABLE `kerjasama` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 0,
  `link` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `kerjasama`
--

INSERT INTO `kerjasama` (`id`, `name`, `image`, `status`, `link`, `description`, `created_at`, `updated_at`, `deleted_at`, `sort`, `slug`) VALUES
(1, 'Gerkatin', 'gerkatin.jpg', 1, NULL, NULL, '2021-06-17 21:40:23', '2021-06-17 21:40:23', NULL, 1, 'gerkatin'),
(2, 'LRBI', 'lrbi.jpg', 1, NULL, NULL, '2021-06-17 21:40:36', '2021-06-17 21:40:37', NULL, 2, 'lrbi'),
(3, 'PUSBISINDO', 'pusbisindo.jpg', 1, NULL, NULL, '2021-06-17 21:40:53', '2021-06-17 21:40:53', NULL, 3, 'pusbisindo'),
(4, 'WORD FEDERATION OF THE DEAF', 'word-federation-of-the-deaf.jpg', 1, NULL, NULL, '2021-06-17 21:41:21', '2021-06-17 21:41:21', NULL, 4, 'word-federation-of-the-deaf'),
(5, 'WASLI', 'wasli.jpg', 1, NULL, NULL, '2021-06-17 21:41:34', '2021-06-17 21:41:34', NULL, 5, 'wasli');

-- --------------------------------------------------------

--
-- Table structure for table `kerjasama_title`
--

CREATE TABLE `kerjasama_title` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `kerjasama_title`
--

INSERT INTO `kerjasama_title` (`id`, `title`, `link`, `description`, `created_at`, `updated_at`, `deleted_at`, `slug`) VALUES
(1, 'Kerja Sama', NULL, '<div class=\"bdy\">\r\n<p>Bentuk kepercayaan dan komitmen kami dalam menyediakan akses komunikasi dan informasi bagi semua masyarakat sesuai</p>\r\n</div>', '2021-06-17 21:40:04', '2021-06-17 21:40:04', NULL, 'kerja-sama');

-- --------------------------------------------------------

--
-- Table structure for table `kolaborasi`
--

CREATE TABLE `kolaborasi` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 0,
  `link` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `kolaborasi`
--

INSERT INTO `kolaborasi` (`id`, `name`, `image`, `status`, `link`, `description`, `created_at`, `updated_at`, `deleted_at`, `sort`, `slug`) VALUES
(1, 'SCTV', 'sctv.png', 1, NULL, NULL, '2021-06-15 01:50:46', '2021-06-22 04:37:46', '2021-06-22 04:37:46', 1, 'sctv'),
(2, 'INEWS', 'inews.png', 1, NULL, NULL, '2021-06-15 01:50:56', '2021-06-22 04:37:49', '2021-06-22 04:37:49', 2, 'inews'),
(3, 'GTV', 'gtv.png', 1, NULL, NULL, '2021-06-15 01:51:04', '2021-06-22 04:37:51', '2021-06-22 04:37:51', 3, 'gtv'),
(4, 'KOMPAS TV', 'kompas-tv.png', 1, NULL, NULL, '2021-06-15 01:51:13', '2021-06-22 04:37:53', '2021-06-22 04:37:53', 4, 'kompas-tv'),
(5, 'RCTI', 'rcti.png', 1, NULL, NULL, '2021-06-21 00:42:28', '2021-06-22 22:38:14', '2021-06-22 22:38:14', 5, 'rcti'),
(6, 'RCTI', 'rcti-1.jpg', 1, NULL, NULL, '2021-06-22 22:38:33', '2021-06-22 22:38:33', NULL, 1, 'rcti-1'),
(7, 'Inews', 'inews-1.jpg', 1, NULL, NULL, '2021-06-22 22:38:42', '2021-06-22 23:09:04', '2021-06-22 23:09:04', 2, 'inews-1'),
(8, 'GTV', 'gtv-1.jpg', 1, NULL, NULL, '2021-06-22 22:38:59', '2021-06-22 22:38:59', NULL, 3, 'gtv-1'),
(9, 'SCTV', 'sctv-1.jpg', 1, NULL, NULL, '2021-06-22 22:39:16', '2021-06-22 23:07:41', '2021-06-22 23:07:41', 4, 'sctv-1'),
(10, 'Kompas TV', 'kompas-tv-1.jpg', 1, NULL, NULL, '2021-06-22 22:39:30', '2021-06-22 22:41:18', '2021-06-22 22:41:18', 5, 'kompas-tv-1'),
(11, 'Kompas TV', 'kompas-tv-2.jpg', 1, NULL, NULL, '2021-06-22 22:41:28', '2021-06-22 23:09:02', NULL, 4, 'kompas-tv-2'),
(12, 'SCTV', 'sctv-2.jpg', 1, NULL, NULL, '2021-06-22 23:07:56', '2021-06-22 23:09:02', NULL, 5, 'sctv-2'),
(13, 'Inews', 'inews-2.jpg', 1, NULL, NULL, '2021-06-22 23:09:15', '2021-06-22 23:09:15', NULL, 6, 'inews-2');

-- --------------------------------------------------------

--
-- Table structure for table `kolaborasi_title`
--

CREATE TABLE `kolaborasi_title` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `kolaborasi_title`
--

INSERT INTO `kolaborasi_title` (`id`, `title`, `link`, `description`, `created_at`, `updated_at`, `deleted_at`, `slug`) VALUES
(1, 'Klien Kami', NULL, '<p>Bentuk kepercayaan dan komitmen kami dalam menyediakan akses komunikasi dan informasi bagi semua masyarakat sesuai</p>', '2021-06-15 01:50:29', '2021-06-15 01:50:29', NULL, 'klien-kami');

-- --------------------------------------------------------

--
-- Table structure for table `kontak_kami`
--

CREATE TABLE `kontak_kami` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `catatan` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `whatsapp` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hours` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telephone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fax` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `website` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `donasi` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `kontak_kami`
--

INSERT INTO `kontak_kami` (`id`, `title`, `description`, `image`, `catatan`, `email`, `whatsapp`, `address`, `hours`, `telephone`, `fax`, `website`, `donasi`, `created_at`, `updated_at`) VALUES
(1, 'Hubungi Kami', '<p>Dukung dan berpartisipasilah dalam pelaksanaan program kegiatan kami dengan menjadi donor (memberikan donasi). Untuk melakukan donasi silahkan klik tombol dibawah ini :</p>', 'kontak_640154033.jpg', 'Yang ingin mengunjungi kantor PLJ, diharapkan untuk membuat janji terlebih dahulu melalui Email atau Whatsapp.', 'email@PLJindonesia.org', '+6281807900275', '<p>Komplek Depkes, Jl H. Umaidi No. Bambu 2, Rt. 10/07, Jl. Raya Pasar Minggu No.39 A, RT.1/RW.7, Rawa Barat, Kec. Pasar. Minggu, Jakarta Selatan - DKI Jakarta 12510 Indonesia</p>', NULL, NULL, '021-7892668', 'www.pljindonesia.org', '#', '2021-06-15 01:52:25', '2021-06-22 04:45:33');

-- --------------------------------------------------------

--
-- Table structure for table `landasan_hukum`
--

CREATE TABLE `landasan_hukum` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 0,
  `whatsapp` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `landasan_hukum`
--

INSERT INTO `landasan_hukum` (`id`, `name`, `image`, `status`, `whatsapp`, `description`, `created_at`, `updated_at`, `deleted_at`, `sort`, `slug`) VALUES
(1, 'Landasan Hukum', NULL, 1, NULL, '<p style=\"text-align: left;\">Pusat Layanan Juru Bahasa Isyarat berasaskan:<br /><br />1. Pancasila<br /><br />2. Undang-Undang Dasar 1945<br /><br />3. Surat Keputusan DPP GERKATIN tanggal 20 April 2015, nomor: 25/SK/DPP GERKATIN-VIII/IV/15<br /><br />4. Anggaran Dasar/Anggaran Rumah Tangga GERKATIN (Gerakan untuk Kesejahteraan Tunarungu Indonesia) yang disahkan di hadapan notaris Mansur Ishak, S.H., No. 17 tanggal 11 Mei 2011, dalam pasal VI yang berisi:<br />a. Menggali potensi dan meningkatkan sumber daya manusia tunarungu Indonesia.<br />b. Membina dan mengembangkan kerja sama dengan perkumpulan yang menangani tunarungu baik di dalam maupun di luar negeri.<br />c. Mengupayakan pemenuhan hak-hak penyandang tunarungu Indonesia.<br />d. Memperjuangkan kesamaan kesempatan penyandang tunarungu dalam segala aspek kehidupan dan penghidupan.<br />e. Mendirikan Pusat Bahasa Isyarat Indonesia (Pusbisindo), tempat pelatihan bahasa Isyarat guna menyediakan sumber daya manusia yang terampil dan siap pakai.<br />f. Menyelenggarakan dan mendirikan pendidikan serta pelatihan bagi para siswa dan guru.<br />g. Melakukan kerja sama dengan lembaga-lembaga pendidikan pemerintah dan swasta yang memberikan bantuan beasiswa.<br /><br />5. Surat dari World Federation of the Deaf (WFD), tanggal 1 Juli 2015, mengenai ucapan selamat atas pendirian Pusat Layanan Juru Bahasa Isyarat (Sign Language Interpreting Service Center)<br /><br />6. Undang-Undang No. 19 Tahun 2011 tentang Ratifikasi Konvensi Hak-hak Penyandang Disabilitas atau CRPD (Convention on the Rights of Persons with Disabilities) yang berkaitan dengan dunia Tuli/tunarungu.<br /><br />7. Undang-Undang No. 8 Tahun 2016 tentang Penyandang Disabilitas yang berkaitan dunia Tuli/tunarungu sebagai berikut.<br />Bagian Kedua Puluh: Hak Berekspresi, Berkomunikasi, dan Memperoleh Informasi</p>', '2021-06-17 21:38:29', '2021-06-22 22:32:58', NULL, 1, 'landasan-hukum'),
(2, 'Pasal 24', NULL, 1, NULL, '<p style=\"text-align: left;\">Hak berekspresi, berkomunikasi, dan memperoleh informasi untuk Penyandang Disabilitas meliputi hak:<br />a. memiliki kebebasan berekspresi dan berpendapat;<br />b. mendapatkan informasi dan berkomunikasi melalui media yang mudah diakses; dan<br />c. menggunakan dan memperoleh fasilitas informasi dan komunikasi berupa bahasa Isyarat, braille, dan komunikasi augmentatif dalam interaksi resmi.<br /><br />Bagian Ketiga: Pendidikan</p>', '2021-06-17 21:38:41', '2021-06-22 22:33:51', NULL, 2, 'pasal-24'),
(3, 'Pasal 41', NULL, 1, NULL, '<p style=\"text-align: left;\">(1) Pemerintah dan Pemerintah Daerah dalam menyelenggarakan pendidikan inklusif dan pendidikan khusus sebagaimana dimaksud dalam Pasal 40 ayat (2) wajib memfasilitasi Penyandang Disabilitas untuk mempelajari keterampilan dasar yang dibutuhkan untuk kemandirian dan partisipasi penuh dalam menempuh pendidikan dan pengembangan sosial.<br /><br />(2) Keterampilan dasar sebagaimana dimaksud pada ayat (1) meliputi:<br />a. keterampilan menulis dan membaca huruf braille untuk Penyandang Disabilitas netra;<br />b. keterampilan orientasi dan mobilitas;<br />c. keterampilan sistem dukungan dan bimbingan sesama Penyandang Disabilitas;<br />d. keterampilan komunikasi dalam bentuk, sarana, dan format yang bersifat augmentatif dan alternatif; dan<br />e. keterampilan bahasa Isyarat dan pemajuan identitas linguistik dari komunitas Penyandang Disabilitas rungu.<br /><br />Bagian Ketujuh: Keagamaan</p>', '2021-06-22 04:50:37', '2021-06-22 22:34:22', NULL, 3, 'pasal-41'),
(4, 'Pasal 82', NULL, 1, NULL, '<p style=\"text-align: left;\">Pemerintah dan Pemerintah Daerah mengupayakan ketersediaan penerjemah bahasa Isyarat dalam kegiatan peribadatan.<br /><br />Bagian Kesembilan: Kebudayaan dan Pariwisata</p>', '2021-06-22 04:50:56', '2021-06-22 22:37:05', NULL, 4, 'pasal-82'),
(5, 'Pasal 85', NULL, 1, NULL, '<p style=\"text-align: left;\">(1) Pemerintah dan Pemerintah Daerah wajib menjamin aksesibilitas bagi Penyandang Disabilitas untuk mendapatkan layanan kebudayaan dan pariwisata.<br /><br />(2) Layanan pariwisata yang mudah diakses bagi Penyandang Disabilitas sebagaimana dimaksud pada ayat (1) meliputi:<br />a. tersedianya informasi pariwisata dalam bentuk audio, visual, dan taktil; dan<br />b. tersedianya pemandu wisata yang memiliki kemampuan untuk mendeskripsikan objek wisata bagi wisatawan Penyandang Disabilitas netra, memandu wisatawan Penyandang Disabilitas rungu dengan bahasa Isyarat, dan memiliki keterampilan memberikan bantuan mobilitas.<br /><br />Bagian Ketujuh Belas Komunikasi dan Informasi<br />Paragraf 1: Komunikasi</p>', '2021-06-22 04:51:31', '2021-06-22 22:37:12', NULL, 5, 'pasal-85'),
(6, 'Pasal 122', NULL, 1, NULL, '<p style=\"text-align: left;\">(1) Pemerintah dan Pemerintah Daerah wajib mengakui, menerima, dan memfasilitasi komunikasi Penyandang Disabilitas dengan menggunakan cara tertentu.</p>\r\n<p style=\"text-align: left;\"><br />(2) Komunikasi dengan menggunakan cara tertentu sebagaimana dimaksud pada ayat (1) dilakukan dengan cara, alat, dan bentuk lainnya yang dapat dijangkau sesuai dengan pilihan Penyandang Disabilitas dalam berinteraksi.<br /><br />Paragraf 2: Informasi</p>', '2021-06-22 04:51:51', '2021-06-22 22:37:19', NULL, 6, 'pasal-122'),
(7, 'Pasal 123', NULL, 1, NULL, '<p style=\"text-align: left;\">(1) Pemerintah dan Pemerintah Daerah wajib menjamin akses atas informasi untuk Penyandang Disabilitas.</p>\r\n<p style=\"text-align: left;\"><br />(2) Akses atas informasi untuk Penyandang Disabilitas sebagaimana dimaksud pada ayat (1) dalam bentuk audio dan visual.</p>', '2021-06-22 04:52:24', '2021-06-22 22:37:25', NULL, 7, 'pasal-123'),
(8, 'Pasal 124', NULL, 1, NULL, '<p style=\"text-align: left;\">(1) Pemerintah dan Pemerintah Daerah wajib menyediakan informasi dalam bentuk yang dapat dijangkau dan dipahami sesuai dengan keragaman disabilitas dan kondisi tempat tinggalnya.</p>\r\n<p style=\"text-align: left;\"><br />(2) Informasi sebagaimana dimaksud pada ayat (1) didapatkan secara tepat waktu dan tanpa biaya tambahan.</p>', '2021-06-22 04:52:39', '2021-06-22 22:37:31', NULL, 8, 'pasal-124');

-- --------------------------------------------------------

--
-- Table structure for table `landasan_hukum_title`
--

CREATE TABLE `landasan_hukum_title` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `landasan_hukum_title`
--

INSERT INTO `landasan_hukum_title` (`id`, `title`, `link`, `description`, `created_at`, `updated_at`, `deleted_at`, `slug`, `image`) VALUES
(1, 'Landasan Hukum di badan PLJ', NULL, '<p>Bentuk kepercayaan dan komitmen kami dalam menyediakan akses komunikasi dan informasi bagi semua masyarakat sesuai</p>', '2021-06-17 21:37:11', '2021-06-17 21:37:11', NULL, 'landasan-hukum-di-badan-plj', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `latar_belakang`
--

CREATE TABLE `latar_belakang` (
  `id` int(11) NOT NULL,
  `title` varchar(191) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `image` varchar(191) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `layanan`
--

CREATE TABLE `layanan` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 0,
  `link` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `layanan`
--

INSERT INTO `layanan` (`id`, `title`, `image`, `status`, `link`, `description`, `created_at`, `updated_at`, `deleted_at`, `sort`, `slug`, `type`) VALUES
(1, 'Sifat', NULL, 1, NULL, '<div class=\"bold\" style=\"text-align: center;\">Pusat Layanan Juru Bahasa Isyarat bersifat nirlaba yang melayani akses informasi dan komunikasi kepada warga Tuli/tunarungu di Indonesia.</div>', '2021-06-17 21:35:48', '2021-06-22 23:10:17', NULL, 1, 'sifat', NULL),
(2, 'Fisik', '', 1, NULL, '<ul>\r\n<li>\r\n<div class=\"bold\">a. Menyediakan layanan:</div>\r\n<ol>\r\n<li>i. Pelatihan juru bahasa Isyarat</li>\r\n<li>ii. Bimbingan konsultasi</li>\r\n<li>iii. Advokasi pelayanan publik</li>\r\n<li>iv. Pelayanan juru bahasa Isyarat di tingkat nasional dan internasional</li>\r\n<li>v. Pengembangan kualitas dan kuantitas juru bahasa Isyarat</li>\r\n<li>vi. Pembiayaan jasa juru bahasa Isyarat</li>\r\n</ol>\r\n</li>\r\n<li>\r\n<div class=\"bold\">b. Memberikan sertifikat&nbsp;<span class=\"normal\">sebagai bukti kelulusan setelah mengikuti pendidikan dan pelatihan mulai dari tingkat dasar sampai pada tingkat profesional.</span></div>\r\n</li>\r\n</ul>', '2021-06-17 21:35:59', '2021-06-17 21:35:59', NULL, 2, 'fisik', NULL),
(3, 'Bentuk', NULL, 1, NULL, '<p style=\"text-align: center;\">Lembaga pelayanan publik yang berwawasan terbuka, independen, dan mandiri.</p>', '2021-06-17 21:36:07', '2021-06-22 23:10:24', NULL, 3, 'bentuk', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `logo`
--

CREATE TABLE `logo` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `logo`
--

INSERT INTO `logo` (`id`, `image`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'logo.png', NULL, '2020-10-19 06:54:05', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `mengapa`
--

CREATE TABLE `mengapa` (
  `id` int(11) NOT NULL,
  `image` varchar(191) DEFAULT NULL,
  `title` varchar(191) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mengapa`
--

INSERT INTO `mengapa` (`id`, `image`, `title`, `description`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Keunggulan Kami', NULL, '2021-06-15 08:47:32', '2021-06-15 08:47:32');

-- --------------------------------------------------------

--
-- Table structure for table `mengapa_belajar`
--

CREATE TABLE `mengapa_belajar` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 0,
  `link` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `mengapa_belajar`
--

INSERT INTO `mengapa_belajar` (`id`, `title`, `image`, `status`, `link`, `description`, `created_at`, `updated_at`, `deleted_at`, `sort`, `slug`) VALUES
(1, 'Respon Cepat', 'respon-cepat.png', 1, NULL, '<p>Admin PLJ selalu melayani Anda dengan tawaran harga terbaik dan pemrosesan untuk jasa Juru Bahasa Isyarat dan penerjemahan dalam bentuk bahasa isyarat ke lisan / suara dan sebaliknya</p>', '2021-06-15 01:47:55', '2021-06-15 01:47:55', NULL, 1, 'respon-cepat'),
(2, 'Juru Bahasa Isyarat Profesional', 'juru-bahasa-isyarat-profesional.png', 1, NULL, '<p>Kami punya Juru Bahasa Isyarat yang berpengalaman dan profesional, yang telah mengikuti pelatihan terintegritasi, serta menguasai bahasa isyarat Indonesia serta Bahasa Isyarat Internasional</p>', '2021-06-15 01:48:11', '2021-06-15 01:48:11', NULL, 2, 'juru-bahasa-isyarat-profesional'),
(3, 'Juru Bahasa Isyarat On Air, Online dan Offline', 'juru-bahasa-isyarat-on-air-online-dan-offline.png', 1, NULL, '<p>Kami dapat menghadirkan JBI untuk semua kebutuhan disesuaikan dengan permintaan khusus dan kebutuhan klien, seperti stasiun TV, webinar, JBI pendamping Tuli, dsb.</p>', '2021-06-15 01:48:33', '2021-06-15 01:48:33', NULL, 3, 'juru-bahasa-isyarat-on-air-online-dan-offline'),
(4, 'Pengembangan skill bahasa isyarat yang terupdate', 'pengembangan-skill-bahasa-isyarat-yang-terupdate.png', 1, NULL, '<p>Pelatihan serta pengalaman kerja yang profesional dengan berbagai klien, komunitas Tuli dan teman-teman Tuli menjadikan JBI kami semakin berintegritas, kompeten dan berkualifikasi</p>', '2021-06-15 01:48:47', '2021-06-15 01:48:47', NULL, 4, 'pengembangan-skill-bahasa-isyarat-yang-terupdate');

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `menu` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `sort` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`id`, `menu`, `slug`, `link`, `created_at`, `updated_at`, `deleted_at`, `sort`) VALUES
(1, 'Beranda', 'beranda', 'https://pljindonesia.org', NULL, '2021-06-18 09:33:32', NULL, 1),
(2, 'Layanan', 'layanan', 'https://pljindonesia.org/#layanan', NULL, '2021-06-17 03:26:03', NULL, 3),
(3, 'Informasi', 'informasi', 'https://pljindonesia.org/informasi', NULL, '2021-06-17 03:27:45', NULL, 4),
(4, 'Kegiatan', 'kegiatan', 'https://pljindonesia.org/kegiatan', NULL, '2021-06-17 03:28:00', NULL, 5),
(5, 'Hubungi Kami', 'kontak-kami', 'https://pljindonesia.org/#kontak-kami', NULL, '2021-06-17 03:28:20', NULL, 6),
(6, 'Tentang Kami', 'tentang-kami', 'https://pljindonesia.org/tentang-kami', NULL, '2021-06-18 09:33:26', NULL, 2);

-- --------------------------------------------------------

--
-- Table structure for table `menu_admin`
--

CREATE TABLE `menu_admin` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `menu` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `sort` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menu_admin`
--

INSERT INTO `menu_admin` (`id`, `menu`, `slug`, `link`, `status`, `created_at`, `updated_at`, `deleted_at`, `sort`) VALUES
(1, 'Banner', 'banner', NULL, 1, NULL, '2021-06-18 08:47:22', NULL, NULL),
(2, 'Apakah', NULL, NULL, 1, NULL, NULL, NULL, NULL),
(3, 'Sejarah', NULL, NULL, 1, NULL, NULL, NULL, NULL),
(4, 'Keunggulan Kami', NULL, NULL, 1, NULL, NULL, NULL, NULL),
(5, 'Pelayanan', NULL, NULL, 1, NULL, NULL, NULL, NULL),
(6, 'Klien', NULL, NULL, 1, NULL, NULL, NULL, NULL),
(7, 'Galeri', 'latar-belakang', NULL, 1, NULL, '2021-01-12 15:43:03', NULL, NULL),
(8, 'Hubungi Kami', 'kontak-kami', NULL, 1, NULL, '2021-01-12 15:43:05', NULL, NULL),
(9, 'Dasar Pemikiran', NULL, NULL, 1, NULL, NULL, NULL, NULL),
(10, 'Layanan', NULL, NULL, 1, NULL, NULL, NULL, NULL),
(11, 'Landasan Hukum', NULL, NULL, 1, NULL, NULL, NULL, NULL),
(12, 'Kerja Sama', NULL, NULL, 1, NULL, NULL, NULL, NULL),
(13, 'Kepengurusan PLJ', 'kolaborasi-kelas', NULL, 1, NULL, '2021-01-12 15:46:32', NULL, NULL),
(14, 'Informasi', NULL, NULL, 1, NULL, NULL, NULL, NULL),
(15, 'Kegiatan', 'apakah', NULL, 1, NULL, NULL, NULL, NULL),
(16, 'Mitra', 'sejarah', NULL, 1, NULL, NULL, NULL, NULL),
(17, 'Jadwal JBI', 'layanan', NULL, 1, NULL, NULL, NULL, NULL),
(18, 'Profil JBI', NULL, NULL, 1, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `metadata`
--

CREATE TABLE `metadata` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keyword` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `h1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `page` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `metadata`
--

INSERT INTO `metadata` (`id`, `title`, `description`, `keyword`, `h1`, `page`, `link`, `created_at`, `updated_at`) VALUES
(1, 'PLJ', 'PLJ (Pusat Layanan Juru Bahasa Isyarat) adalah lembaga yang mengelola juru bahasa isyarat di Indonesia yang dikelola oleh tuli komunitas pengguna bahasa isyarat.', 'PLJ (Pusat Layanan Juru Bahasa Isyarat) adalah lembaga yang mengelola juru bahasa isyarat di Indonesia yang dikelola oleh tuli komunitas pengguna bahasa isyarat.', NULL, 'Homepage', 'https://pljindonesia.org', '2020-02-13 05:27:25', '2021-06-18 08:46:06'),
(2, 'PLJ', 'PLJ (Pusat Layanan Juru Bahasa Isyarat) adalah lembaga yang mengelola juru bahasa isyarat di Indonesia yang dikelola oleh tuli komunitas pengguna bahasa isyarat.', 'PLJ (Pusat Layanan Juru Bahasa Isyarat) adalah lembaga yang mengelola juru bahasa isyarat di Indonesia yang dikelola oleh tuli komunitas pengguna bahasa isyarat.', NULL, 'Tentang Kami', 'https://pljindonesia.org/tentang-kami', '2020-10-19 05:20:26', '2021-06-18 08:46:30');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `mitra`
--

CREATE TABLE `mitra` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `video` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `mitra`
--

INSERT INTO `mitra` (`id`, `title`, `name`, `image`, `video`, `created_at`, `updated_at`, `sort`, `status`) VALUES
(1, NULL, 'Laura Lesmana Wijaya', '', 'https://www.youtube.com/embed/Dy0q96bSc4s', '2020-12-04 08:12:29', '2021-06-23 20:06:05', 3, 1),
(2, NULL, NULL, '', 'https://www.youtube.com/embed/0tC4deUeY0g', '2021-06-17 22:08:28', '2021-06-23 20:04:21', 1, 1),
(5, NULL, NULL, '', 'https://www.youtube.com/embed/6G9-4HxaMdo', '2021-06-23 06:07:41', '2021-06-23 20:07:05', 4, 1),
(7, NULL, NULL, '', 'https://www.youtube.com/embed/MMtycWGinms', '2021-06-23 06:08:53', '2021-06-23 20:05:04', 2, 1),
(9, NULL, NULL, '', 'https://www.youtube.com/embed/fe7L7GGefds', '2021-06-23 20:07:43', '2021-06-23 20:07:43', 5, 1),
(10, NULL, NULL, '', 'https://www.youtube.com/embed/rd5RtAduE2o', '2021-06-23 20:09:03', '2021-06-23 20:09:03', 6, 1),
(11, NULL, NULL, '', 'https://www.youtube.com/embed/3UacmAngPgY', '2021-06-23 20:10:18', '2021-06-23 20:10:18', 7, 1);

-- --------------------------------------------------------

--
-- Table structure for table `mitra_title`
--

CREATE TABLE `mitra_title` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `youtube_link1` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `youtube_link_name1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `youtube_link2` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `youtube_link_name2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `mitra_title`
--

INSERT INTO `mitra_title` (`id`, `title`, `link`, `description`, `created_at`, `updated_at`, `deleted_at`, `slug`, `image`, `youtube_link1`, `youtube_link_name1`, `youtube_link2`, `youtube_link_name2`) VALUES
(1, 'Bermitra dengan BNPB dan Kementerian Keuangan dalam menyediakan akses Juru Bahasa Isyarat', NULL, '<p>Sebagai bentuk transparansi kerja sama klien kami dengan Juru Bahasa Isyarat PLJ, terlampir jadwal kerja JBI on TV</p>', '2021-06-17 22:04:08', '2021-06-17 22:05:21', NULL, 'bermitra-dengan-bnpb-dan-kementerian-keuangan-dalam-menyediakan-akses-juru-bahasa-isyarat', NULL, 'https://www.youtube.com/channel/UCygLV2FOMij6PoAZqkN5kFg', 'KemenKeu PPID', 'https://www.youtube.com/user/BNPBIndonesia', 'BNPB');

-- --------------------------------------------------------

--
-- Table structure for table `pengurus`
--

CREATE TABLE `pengurus` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cabang_id` int(11) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 0,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pengurus_title`
--

CREATE TABLE `pengurus_title` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pertanyaan`
--

CREATE TABLE `pertanyaan` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `judul` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 0,
  `link` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pertanyaan_image`
--

CREATE TABLE `pertanyaan_image` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `profil`
--

CREATE TABLE `profil` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 0,
  `link` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `profil`
--

INSERT INTO `profil` (`id`, `name`, `title`, `image`, `status_name`, `no_id`, `status`, `link`, `description`, `created_at`, `updated_at`, `deleted_at`, `sort`, `slug`) VALUES
(1, 'nama 1', NULL, 'nama-1.jpg', NULL, NULL, 1, NULL, NULL, '2021-07-01 02:18:39', '2021-07-01 02:18:39', NULL, 1, 'nama-1'),
(2, 'nama 2', NULL, 'nama-2.jpg', NULL, NULL, 1, NULL, NULL, '2021-07-01 02:18:51', '2021-07-01 02:18:51', NULL, 2, 'nama-2'),
(3, 'nama 3', NULL, 'nama-3.jpg', NULL, NULL, 1, NULL, NULL, '2021-07-01 02:19:07', '2021-07-01 02:19:07', NULL, 3, 'nama-3'),
(4, 'nama 4', NULL, 'nama-4.jpg', NULL, NULL, 1, NULL, NULL, '2021-07-01 02:19:17', '2021-07-01 02:19:17', NULL, 4, 'nama-4'),
(5, 'nama 5', NULL, 'nama-5.jpg', NULL, NULL, 1, NULL, NULL, '2021-07-01 02:19:28', '2021-07-01 02:19:28', NULL, 5, 'nama-5'),
(6, 'nama 6', NULL, 'nama-6.jpg', NULL, NULL, 1, NULL, NULL, '2021-07-01 02:19:42', '2021-07-01 02:19:42', NULL, 6, 'nama-6');

-- --------------------------------------------------------

--
-- Table structure for table `profil_title`
--

CREATE TABLE `profil_title` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `profil_title`
--

INSERT INTO `profil_title` (`id`, `title`, `link`, `description`, `created_at`, `updated_at`, `deleted_at`, `slug`) VALUES
(1, 'Profil Juru Bahasa Isyarat PLJ', NULL, '<p>Bentuk kepercayaan dan komitmen kami dalam menyediakan akses komunikasi dan informasi bagi semua masyarakat sesuai</p>', '2021-07-01 02:06:49', '2021-07-01 02:10:50', NULL, 'profil-juru-bahasa-isyarat-plj');

-- --------------------------------------------------------

--
-- Table structure for table `sejarah`
--

CREATE TABLE `sejarah` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 0,
  `link` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sejarah`
--

INSERT INTO `sejarah` (`id`, `title`, `image`, `status`, `link`, `description`, `created_at`, `updated_at`, `deleted_at`, `sort`, `slug`, `type`) VALUES
(1, 'Sejarah Singkat', 'sejarah-singkat.jpg', 1, NULL, '<p>PLJ didirikan oleh warga Tuli/tunarungu melalui workshop, diskusi yang diikuti oleh para ketua DPD (Dewan Pengurus Daerah) GERKATIN, instruktur Bahasa Isyarat Indonesia dari Pusat Bahasa Isyarat Indonesia (Pusbisindo) dan Laboratorium Riset Bahasa Isyarat (LRBI) Fakultas Ilmu Pengetahuan Budaya Universitas Indonesia, dan beberapa juru bahasa Isyarat lokal.</p>', '2021-06-15 01:47:00', '2021-06-15 01:47:00', NULL, 1, 'sejarah-singkat', 1),
(2, 'Visi Misi', NULL, 1, NULL, '<p style=\"text-align: left;\">1. Visi<br />Menjadi lembaga layanan juru bahasa Isyarat yang profesional dan mampu mewujudkan kemudahan untuk mendapatkan dan memberikan informasi melalui juru bahasa Isyarat serta menjadikan orang Tuli/tunarungu mampu berperan aktif di berbagai aspek kehidupan.</p>\r\n<p style=\"text-align: left;\"><br />2. Misi<br />a. Memantapkan profesionalisme PLJ sebagai pusat penyedia layanan juru bahasa Isyarat.<br />b. Memperjuangkan peningkatan peran warga Tuli/tunarungu dalam berbagai aspek kehidupan melalui bahasa Isyarat.<br />c. Memantapkan dan meningkatkan harkat dan martabat warga Tuli/tunarungu sebagai warga negara yang mampu berperan dalam penyebaran informasi.</p>', '2021-06-15 01:47:20', '2021-06-21 00:37:21', NULL, 2, 'visi-misi', 1);

-- --------------------------------------------------------

--
-- Table structure for table `social_media`
--

CREATE TABLE `social_media` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `link` text COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `social_media`
--

INSERT INTO `social_media` (`id`, `name`, `image`, `created_at`, `updated_at`, `deleted_at`, `slug`, `sort`, `link`) VALUES
(1, 'twitter', NULL, '2020-12-12 14:31:45', '2021-06-17 22:17:55', NULL, 'twitter', 3, 'https://pljindonesia.org/#'),
(2, 'instagram', NULL, '2020-12-12 14:31:54', '2021-06-17 22:17:39', NULL, 'instagram', 1, 'https://www.instagram.com/plj.indonesia/'),
(3, 'facebook', NULL, '2020-12-12 14:32:04', '2021-06-17 22:17:45', NULL, 'facebook', 2, 'https://www.facebook.com/PusatLayananJuruBahasaIsyaratIndonesia/');

-- --------------------------------------------------------

--
-- Table structure for table `struktur_kepengurusan`
--

CREATE TABLE `struktur_kepengurusan` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 0,
  `link` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `struktur_kepengurusan`
--

INSERT INTO `struktur_kepengurusan` (`id`, `name`, `title`, `image`, `status`, `link`, `description`, `created_at`, `updated_at`, `deleted_at`, `sort`, `slug`) VALUES
(1, 'Nama Lengkap', 'Posisi', 'nama-lengkap.jpg', 1, NULL, NULL, '2021-06-17 21:44:21', '2021-06-17 21:44:21', NULL, 1, 'nama-lengkap'),
(2, 'Nama Lengkap 2', 'Posisi', 'nama-lengkap-2.jpg', 1, NULL, NULL, '2021-06-18 09:51:41', '2021-06-18 09:51:41', NULL, 2, 'nama-lengkap-2'),
(3, 'Nama Lengkap 3', 'Posisi', 'nama-lengkap-3.jpg', 1, NULL, NULL, '2021-06-18 10:13:31', '2021-06-18 10:13:31', NULL, 3, 'nama-lengkap-3'),
(4, 'Nama Lengkap 4', 'Posisi', 'nama-lengkap-4.jpg', 1, NULL, NULL, '2021-06-18 10:13:44', '2021-06-18 10:13:44', NULL, 4, 'nama-lengkap-4'),
(5, 'Nama Lengkap 5', 'Posisi', 'nama-lengkap-5.jpg', 1, NULL, NULL, '2021-06-18 10:14:00', '2021-06-18 10:14:00', NULL, 5, 'nama-lengkap-5'),
(6, 'Nama Lengkap 6', 'Posisi', 'nama-lengkap-6.jpg', 1, NULL, NULL, '2021-06-18 10:14:19', '2021-06-18 10:14:19', NULL, 6, 'nama-lengkap-6');

-- --------------------------------------------------------

--
-- Table structure for table `struktur_kepengurusan_title`
--

CREATE TABLE `struktur_kepengurusan_title` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `struktur_kepengurusan_title`
--

INSERT INTO `struktur_kepengurusan_title` (`id`, `title`, `link`, `description`, `created_at`, `updated_at`, `deleted_at`, `slug`) VALUES
(1, 'Kepengurusan PLJ', NULL, NULL, '2021-06-17 21:43:14', '2021-06-17 21:43:14', NULL, 'kepengurusan-plj');

-- --------------------------------------------------------

--
-- Table structure for table `tentang_kami`
--

CREATE TABLE `tentang_kami` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 0,
  `link` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tentang_kami_title`
--

CREATE TABLE `tentang_kami_title` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tentang_kami_title`
--

INSERT INTO `tentang_kami_title` (`id`, `title`, `image`, `link`, `description`, `created_at`, `updated_at`, `deleted_at`, `slug`) VALUES
(1, 'Tentang Kami', 'tentang_kami_1239414563.jpg', NULL, NULL, '2021-06-15 04:50:23', '2021-06-18 10:52:57', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `terms`
--

CREATE TABLE `terms` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `terms` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `privacy_policy` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `testimonial`
--

CREATE TABLE `testimonial` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `testimonial` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `timeline`
--

CREATE TABLE `timeline` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 0,
  `link` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `timeline_title`
--

CREATE TABLE `timeline_title` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `about`
--
ALTER TABLE `about`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_color`
--
ALTER TABLE `admin_color`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `apakah`
--
ALTER TABLE `apakah`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `artikel`
--
ALTER TABLE `artikel`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `artikel_kategori`
--
ALTER TABLE `artikel_kategori`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bank`
--
ALTER TABLE `bank`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `banner`
--
ALTER TABLE `banner`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cabang`
--
ALTER TABLE `cabang`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cabang_title`
--
ALTER TABLE `cabang_title`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cara_kerja`
--
ALTER TABLE `cara_kerja`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `company_data`
--
ALTER TABLE `company_data`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dasar_pemikiran`
--
ALTER TABLE `dasar_pemikiran`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dasar_pemikiran_image`
--
ALTER TABLE `dasar_pemikiran_image`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `galeri`
--
ALTER TABLE `galeri`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `galeri_title`
--
ALTER TABLE `galeri_title`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hubungi_kami`
--
ALTER TABLE `hubungi_kami`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `informasi`
--
ALTER TABLE `informasi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `informasi_category`
--
ALTER TABLE `informasi_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `informasi_title`
--
ALTER TABLE `informasi_title`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jadwal`
--
ALTER TABLE `jadwal`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kategori`
--
ALTER TABLE `kategori`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kegiatan_title`
--
ALTER TABLE `kegiatan_title`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kerjasama`
--
ALTER TABLE `kerjasama`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kerjasama_title`
--
ALTER TABLE `kerjasama_title`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kolaborasi`
--
ALTER TABLE `kolaborasi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kolaborasi_title`
--
ALTER TABLE `kolaborasi_title`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kontak_kami`
--
ALTER TABLE `kontak_kami`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `landasan_hukum`
--
ALTER TABLE `landasan_hukum`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `landasan_hukum_title`
--
ALTER TABLE `landasan_hukum_title`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `latar_belakang`
--
ALTER TABLE `latar_belakang`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `layanan`
--
ALTER TABLE `layanan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `logo`
--
ALTER TABLE `logo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mengapa`
--
ALTER TABLE `mengapa`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mengapa_belajar`
--
ALTER TABLE `mengapa_belajar`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu_admin`
--
ALTER TABLE `menu_admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `metadata`
--
ALTER TABLE `metadata`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mitra`
--
ALTER TABLE `mitra`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mitra_title`
--
ALTER TABLE `mitra_title`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pengurus`
--
ALTER TABLE `pengurus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pengurus_title`
--
ALTER TABLE `pengurus_title`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pertanyaan`
--
ALTER TABLE `pertanyaan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pertanyaan_image`
--
ALTER TABLE `pertanyaan_image`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profil`
--
ALTER TABLE `profil`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profil_title`
--
ALTER TABLE `profil_title`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sejarah`
--
ALTER TABLE `sejarah`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `social_media`
--
ALTER TABLE `social_media`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `struktur_kepengurusan`
--
ALTER TABLE `struktur_kepengurusan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `struktur_kepengurusan_title`
--
ALTER TABLE `struktur_kepengurusan_title`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tentang_kami`
--
ALTER TABLE `tentang_kami`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tentang_kami_title`
--
ALTER TABLE `tentang_kami_title`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `terms`
--
ALTER TABLE `terms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `testimonial`
--
ALTER TABLE `testimonial`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `timeline`
--
ALTER TABLE `timeline`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `timeline_title`
--
ALTER TABLE `timeline_title`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `about`
--
ALTER TABLE `about`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `admin_color`
--
ALTER TABLE `admin_color`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `apakah`
--
ALTER TABLE `apakah`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `artikel`
--
ALTER TABLE `artikel`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `artikel_kategori`
--
ALTER TABLE `artikel_kategori`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bank`
--
ALTER TABLE `bank`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `banner`
--
ALTER TABLE `banner`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `cabang`
--
ALTER TABLE `cabang`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `cabang_title`
--
ALTER TABLE `cabang_title`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `cara_kerja`
--
ALTER TABLE `cara_kerja`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `company_data`
--
ALTER TABLE `company_data`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `contact`
--
ALTER TABLE `contact`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `dasar_pemikiran`
--
ALTER TABLE `dasar_pemikiran`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `dasar_pemikiran_image`
--
ALTER TABLE `dasar_pemikiran_image`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `galeri`
--
ALTER TABLE `galeri`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `galeri_title`
--
ALTER TABLE `galeri_title`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `hubungi_kami`
--
ALTER TABLE `hubungi_kami`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `informasi`
--
ALTER TABLE `informasi`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `informasi_category`
--
ALTER TABLE `informasi_category`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `informasi_title`
--
ALTER TABLE `informasi_title`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `jadwal`
--
ALTER TABLE `jadwal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `kategori`
--
ALTER TABLE `kategori`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `kegiatan_title`
--
ALTER TABLE `kegiatan_title`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `kerjasama`
--
ALTER TABLE `kerjasama`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `kerjasama_title`
--
ALTER TABLE `kerjasama_title`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `kolaborasi`
--
ALTER TABLE `kolaborasi`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `kolaborasi_title`
--
ALTER TABLE `kolaborasi_title`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `kontak_kami`
--
ALTER TABLE `kontak_kami`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `landasan_hukum`
--
ALTER TABLE `landasan_hukum`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `landasan_hukum_title`
--
ALTER TABLE `landasan_hukum_title`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `latar_belakang`
--
ALTER TABLE `latar_belakang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `layanan`
--
ALTER TABLE `layanan`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `logo`
--
ALTER TABLE `logo`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `mengapa`
--
ALTER TABLE `mengapa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `mengapa_belajar`
--
ALTER TABLE `mengapa_belajar`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `menu_admin`
--
ALTER TABLE `menu_admin`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `metadata`
--
ALTER TABLE `metadata`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `mitra`
--
ALTER TABLE `mitra`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `mitra_title`
--
ALTER TABLE `mitra_title`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `pengurus`
--
ALTER TABLE `pengurus`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pengurus_title`
--
ALTER TABLE `pengurus_title`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pertanyaan`
--
ALTER TABLE `pertanyaan`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pertanyaan_image`
--
ALTER TABLE `pertanyaan_image`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `profil`
--
ALTER TABLE `profil`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `profil_title`
--
ALTER TABLE `profil_title`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `sejarah`
--
ALTER TABLE `sejarah`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `social_media`
--
ALTER TABLE `social_media`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `struktur_kepengurusan`
--
ALTER TABLE `struktur_kepengurusan`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `struktur_kepengurusan_title`
--
ALTER TABLE `struktur_kepengurusan_title`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tentang_kami`
--
ALTER TABLE `tentang_kami`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tentang_kami_title`
--
ALTER TABLE `tentang_kami_title`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `terms`
--
ALTER TABLE `terms`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `testimonial`
--
ALTER TABLE `testimonial`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `timeline`
--
ALTER TABLE `timeline`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `timeline_title`
--
ALTER TABLE `timeline_title`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
