<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;

class Jadwal extends Model
{
    public $timestamps = true;
    protected $table = 'jadwal';
    protected $fillable = ['image', 'title', 'description'];

}
