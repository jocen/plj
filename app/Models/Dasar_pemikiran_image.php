<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;

class Dasar_pemikiran_image extends Model
{
    use SoftDeletes;
    public $timestamps = true;
    protected $table = 'dasar_pemikiran_image';
    protected $fillable = ['image'];

}
