<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;

class Tentang_kami_title extends Model
{
    public $timestamps = true;
    protected $table = 'tentang_kami_title';
    protected $fillable = ['title', 'description','image'];

}
