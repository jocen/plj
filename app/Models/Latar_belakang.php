<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;

class Latar_belakang extends Model
{
    use SoftDeletes;
    public $timestamps = true;
    protected $table = 'latar_belakang';
    protected $fillable = ['description', 'title', 'image'];
}
