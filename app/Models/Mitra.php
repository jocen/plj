<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Mitra extends Model
{    
    public $timestamps = true;
    protected $table = 'mitra';
    protected $fillable = ['title', 'image', 'video', 'status'];
        protected $attributes = [
        'status' => 1,
    ];
}
