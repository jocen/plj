<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;

class Informasi extends Model
{
    use SoftDeletes;
    public $timestamps = true;
    protected $table = 'informasi';
    protected $fillable = ['question', 'answer','status','informasi_category_id','sort'];
    protected $attributes = [
        'status' => 1,
    ];

    public function informasi_category()
    {
        return $this->belongsTo('App\Models\Informasi_category');
    }

}
