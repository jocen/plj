<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;

class Pertanyaan_image extends Model
{
    use SoftDeletes;
    public $timestamps = true;
    protected $table = 'pertanyaan_image';
    protected $fillable = ['image'];

}
