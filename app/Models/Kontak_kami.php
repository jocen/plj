<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Kontak_kami extends Model
{    
    public $timestamps = true;
    protected $table = 'kontak_kami';
    protected $fillable = ['title', 'description', 'image', 'catatan', 'email', 'whatsapp', 'address', 'hours', 'telephone'];
}
