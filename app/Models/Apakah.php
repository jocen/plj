<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;

class Apakah extends Model
{
    public $timestamps = true;
    protected $table = 'apakah';
    protected $fillable = ['image', 'title', 'description'];

}
