<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use App\Http\Controllers\BaseController;
use App\Models\Kontak_kami;
use App\Models\Banner;
use App\Models\Tentang_kami;
use App\Models\Hubungi_kami;
use App\Models\Social_media;
use App\Models\Menu;
use App\Models\Logo;
use App\Models\Apakah;
use App\Models\Cabang;
use App\Models\Cabang_title;
use App\Models\Kolaborasi;
use App\Models\Kolaborasi_title;
use App\Models\Timeline;
use App\Models\Timeline_title;
use App\Models\Struktur_kepengurusan_title;
use App\Models\Struktur_kepengurusan;
use App\Models\Pengurus_title;
use App\Models\Pengurus;
use App\Models\Informasi_title;
use App\Models\Informasi_category;
use App\Models\Informasi;
use App\Models\Video;
use App\Models\Artikel;
use App\Models\Sejarah;
use App\Models\Mengapa;
use App\Models\Mengapa_belajar;
use App\Models\Galeri;
use App\Models\Galeri_title;
use App\Models\Dasar_pemikiran;
use App\Models\Dasar_pemikiran_image;
use App\Models\Layanan;
use App\Models\Landasan_hukum;
use App\Models\Landasan_hukum_title;
use App\Models\Kerjasama;
use App\Models\Kerjasama_title;
use App\Models\Mitra;
use App\Models\Mitra_title;
use App\Models\Jadwal;
use App\Models\Kegiatan_title;
use App\Models\Tentang_kami_title;
use App\Models\Profil;
use App\Models\Profil_title;
use View;
use App\Helper\HelperFunction;
use Session;
use Output;
use Status;
use Mail;
use DB;

class PageController extends Controller
{

    public function home(){
        $data['banner'] = Banner::where('status',1)->orderby('sort','asc')->get();
        $data['hubungi_kami'] = Hubungi_kami::first();
        $data['kontak_kami'] = Kontak_kami::first();
        $data['social_media'] = Social_media::orderby('sort','asc')->get();
        $data['menu'] = Menu::orderby('sort','asc')->get();
        $data['logo'] = Logo::first();
        $data['apakah'] = Apakah::first();
        $data['sejarah'] = Sejarah::where('status',1)->orderby('sort','asc')->get();
        $data['keunggulan_title'] = Mengapa::first();
        $data['keunggulan'] = Mengapa_belajar::where('status',1)->orderby('sort','asc')->get();
        $data['butuhkan'] = Cabang::where('status',1)->orderby('sort','asc')->get();
        $data['butuhkan_title'] = Cabang_title::first();
        $data['klien'] = Kolaborasi::where('status',1)->orderby('sort','asc')->get();
        $data['klien_title'] = Kolaborasi_title::first();
        $data['galeri'] = Galeri::where('status',1)->orderby('sort','asc')->limit(6)->get();
        $data['galeri_title'] = Galeri_title::first();

        return view('/index', $data);  
    }

    public function tentang_kami(){
        $data['tentang_kami_title'] = Tentang_kami_title::first();
        $data['hubungi_kami'] = Hubungi_kami::first();
        $data['social_media'] = Social_media::orderby('sort','asc')->get();
        $data['menu'] = Menu::orderby('sort','asc')->get();
        $data['logo'] = Logo::first();
        $data['kontak_kami'] = Kontak_kami::first();
        $data['apakah'] = Apakah::first();
        $data['sejarah'] = Sejarah::where('status',1)->orderby('sort','asc')->get();
        $data['dasar_pemikiran'] = Dasar_pemikiran::where('status',1)->orderby('sort','asc')->get();
        $data['dasar_pemikiran_image'] = Dasar_pemikiran_image::first();
        $data['layanan'] = Layanan::where('status',1)->orderby('sort','asc')->get();
        $data['landasan_hukum'] = Landasan_hukum::where('status',1)->orderby('sort','asc')->get();
        $data['landasan_hukum_title'] = Landasan_hukum_title::first();
        $data['kerjasama'] = Kerjasama::where('status',1)->orderby('sort','asc')->get();
        $data['kerjasama_title'] = Kerjasama_title::first();
        $data['struktur_kepengurusan'] = Struktur_kepengurusan::where('status',1)->orderby('sort','asc')->get();
        $data['struktur_kepengurusan_title'] = Struktur_kepengurusan_title::first();
        $data['profil'] = Profil::where('status',1)->orderby('sort','asc')->get();
        $data['profil_title'] = Profil_title::first();

        return view('/tentang-kami', $data);  
    }

    public function informasi(Request $request){
        $data['hubungi_kami'] = Hubungi_kami::first();
        $data['social_media'] = Social_media::orderby('sort','asc')->get();
        $data['menu'] = Menu::orderby('sort','asc')->get();
        $data['logo'] = Logo::first();
        $data['kontak_kami'] = Kontak_kami::first();
        $data['informasi'] = Informasi_category::where('status',1)->orderby('sort','asc')->with(['informasi'])->get();
        $data['informasi_title'] = Informasi_title::first();

        return view('/informasi', $data);  
    }

    public function kegiatan(Request $request){
        $data['hubungi_kami'] = Hubungi_kami::first();
        $data['social_media'] = Social_media::orderby('sort','asc')->get();
        $data['menu'] = Menu::orderby('sort','asc')->get();
        $data['logo'] = Logo::first();
        $data['mitra'] = Mitra::where('status',1)->orderby('sort','asc')->get();
        $data['mitra_title'] = Mitra_title::first();
        $data['kontak_kami'] = Kontak_kami::first();
        $data['jadwal'] = Jadwal::first();
        $data['galeri'] = Galeri::where('status',1)->orderby('sort','asc')->paginate(12);
        $data['galeri_title'] = Galeri_title::first();
        $data['kegiatan_title'] = Kegiatan_title::first();

        return view('/kegiatan', $data);  
    }
}


