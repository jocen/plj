<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Kegiatan;
use App\Models\Kegiatan_title;
use Illuminate\Http\Request;

class KegiatanController extends Controller
{
    public function view()
    {
        $data = kegiatan::orderby('sort','asc')->get();
        return view('vendor.backpack.base.kegiatan.list', ['data' => $data]);
    }
    public function create()
    {
        return view('vendor.backpack.base.kegiatan.create');
    }
    public function edit($id)
    {
        $data = kegiatan::find($id);
        return view('vendor.backpack.base.kegiatan.edit', ['data' => $data]);
    }
    public function insert(Request $request)
    {
        // $validatedData = $request->validate([
        //     'title' => 'required|max:255|unique:kegiatan',
        // ]);

        $imageName = "";
        $imageNameMobile = "";
        $last_sort = kegiatan::orderby('sort', 'desc')->first();
            $sort = 1;
            if($last_sort){
                $sort = $last_sort->sort + 1;
            }

        $table = new kegiatan;
        $table->title = $request->input('title');
        $table->image = $imageName;
        $table->link = $request->input('link');
        $table->description = $request->input('description');
        $table->sort = $sort;
        $table->save();

        $detail = kegiatan::where('id',$table->id)->first();
        if ($request->hasFile('image')) {
            $imageTempName = $request->file('image')->getPathname();
            $imageName = $detail->slug . '.' . $request->file('image')->getClientOriginalExtension();
            $path = base_path() . '/public/upload';
            $request->file('image')->move($path, $imageName);

            $detail->image = $imageName;
            $detail->save();
        }

        $request->session()->flash('insert', 'Success');
        return redirect()->route('kegiatan_view');
    }
    public function update(Request $request)
    {
        // $validatedData = $request->validate([
        //     'title' => 'required|max:255|unique:kegiatan,title,'.$request->input('id'),
        // ]);

        $imageName = "";
        $imageNameMobile = "";

        $table = kegiatan::find($request->input('id'));
        $table->title = $request->input('title');
        $table->image = $imageName;
        $table->link = $request->input('link');
        $table->description = $request->input('description');
        $table->save();

        $detail = kegiatan::where('id',$request->input('id'))->first();
        if ($request->hasFile('image')) {
            if ($request->input('old_image') != null) {
                $oldimage = base_path() . '/public/upload/' . $request->input('old_image');
                if (file_exists($oldimage)) {
                    unlink($oldimage);
                }
            }

            $imageTempName = $request->file('image')->getPathname();
            $imageName = $detail->slug . '.' . $request->file('image')->getClientOriginalExtension();
            $path = base_path() . '/public/upload';
            $request->file('image')->move($path, $imageName);
            
        } else {
            $imageName = $request->input('old_image');
        }


        $detail->image = $imageName;
        $detail->save();
        $request->session()->flash('update', 'Success');
        return redirect()->route('kegiatan_view');
    }
    public function delete($id, Request $request)
    {
        $table = kegiatan::find($id);
        $table->delete();

        $request->session()->flash('delete', 'Success');
        return redirect()->route('kegiatan_view');
    }

    public function status($id,$status){
        $table = kegiatan::find($id);
        $table->status = $status;
        $table->Save();

        return redirect()->route('kegiatan_view');
    }

    public function update_sort(Request $request){
        $maincontent_id = $request->input('maincontent_id');
        $oldsort = $request->input('oldsort');
        $newsort = $request->input('newsort');

        $getTable = kegiatan::where('id',$maincontent_id)->where('sort',$oldsort)->first();
        $getTable->sort = $newsort;
        $getTable->save();

        $status=array('status'=>'1','message'=>'Success');
        return response()->json($status);
    }

    public function title(){
        $data = Kegiatan_title::first();
        return view('vendor.backpack.base.kegiatan.title', ['data'=>$data]);
    }

    public function update_title(Request $request)
    {
        $company_profile = Kegiatan_title::first();
        if (empty($company_profile)) {
            $company_profile = new Kegiatan_title;
        }

        if ($request->hasFile('image')) {
            if ($request->input('old_image') != null) {
                $oldimage = base_path() . '/public/upload/' . $request->input('old_image');
                if (file_exists($oldimage)) {
                    unlink($oldimage);
                }
            }
            $imageName = 'kegiatan_'.rand().'.'.$request->file('image')->getClientOriginalExtension();
            $path = base_path() . '/public/upload';
            $request->file('image')->move($path, $imageName);
            
        } else {
            $imageName = $request->input('old_image');
        }

       $company_profile->image =$imageName;

        $company_profile->title =$request->input('title');
        $company_profile->description =$request->input('description');
        $company_profile->save();

        $request->session()->flash('update', 'Success');
        return redirect()->route('kegiatan_title_view');
    }
}
