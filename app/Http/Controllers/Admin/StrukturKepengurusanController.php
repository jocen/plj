<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Struktur_kepengurusan;
use App\Models\Struktur_kepengurusan_title;
use Illuminate\Http\Request;

class StrukturKepengurusanController extends Controller
{
    public function view()
    {
        $data = Struktur_kepengurusan::orderby('sort','asc')->get();
        return view('vendor.backpack.base.struktur_kepengurusan.list', ['data' => $data]);
    }
    public function create()
    {
        return view('vendor.backpack.base.struktur_kepengurusan.create');
    }
    public function edit($id)
    {
        $data = Struktur_kepengurusan::find($id);
        return view('vendor.backpack.base.struktur_kepengurusan.edit', ['data' => $data]);
    }
    public function insert(Request $request)
    {

        $imageName = "";
        $imageNameMobile = "";
        $last_sort = Struktur_kepengurusan::orderby('sort', 'desc')->first();
            $sort = 1;
            if($last_sort){
                $sort = $last_sort->sort + 1;
            }

        $table = new Struktur_kepengurusan;
        $table->name = $request->input('name');
        $table->title = $request->input('title');
        $table->image = $imageName;
        $table->link = $request->input('link');
        $table->description = $request->input('description');
        $table->sort = $sort;
        $table->save();

        $detail = Struktur_kepengurusan::where('id',$table->id)->first();
        if ($request->hasFile('image')) {
            $imageTempName = $request->file('image')->getPathname();
            $imageName = $detail->slug . '.' . $request->file('image')->getClientOriginalExtension();
            $path = base_path() . '/public/upload';
            $request->file('image')->move($path, $imageName);

            $detail->image = $imageName;
            $detail->save();
        }

        $request->session()->flash('insert', 'Success');
        return redirect()->route('struktur_kepengurusan_view');
    }
    public function update(Request $request)
    {

        $imageName = "";
        $imageNameMobile = "";

        $table = Struktur_kepengurusan::find($request->input('id'));
        $table->name = $request->input('name');
        $table->image = $imageName;
        $table->title = $request->input('title');
        $table->description = $request->input('description');
        $table->save();

        $detail = Struktur_kepengurusan::where('id',$request->input('id'))->first();
        if ($request->hasFile('image')) {
            if ($request->input('old_image') != null) {
                $oldimage = base_path() . '/public/upload/' . $request->input('old_image');
                if (file_exists($oldimage)) {
                    unlink($oldimage);
                }
            }

            $imageTempName = $request->file('image')->getPathname();
            $imageName = $detail->slug . '.' . $request->file('image')->getClientOriginalExtension();
            $path = base_path() . '/public/upload';
            $request->file('image')->move($path, $imageName);
            
        } else {
            $imageName = $request->input('old_image');
        }


        $detail->image = $imageName;
        $detail->save();
        $request->session()->flash('update', 'Success');
        return redirect()->route('struktur_kepengurusan_view');
    }
    public function delete($id, Request $request)
    {
        $table = Struktur_kepengurusan::find($id);
        $table->delete();

        $request->session()->flash('delete', 'Success');
        return redirect()->route('struktur_kepengurusan_view');
    }

    public function status($id,$status){
        $table = Struktur_kepengurusan::find($id);
        $table->status = $status;
        $table->Save();

        return redirect()->route('struktur_kepengurusan_view');
    }

    public function update_sort(Request $request){
        $maincontent_id = $request->input('maincontent_id');
        $oldsort = $request->input('oldsort');
        $newsort = $request->input('newsort');

        $getTable = Struktur_kepengurusan::where('id',$maincontent_id)->where('sort',$oldsort)->first();
        $getTable->sort = $newsort;
        $getTable->save();

        $status=array('status'=>'1','message'=>'Success');
        return response()->json($status);
    }

    public function title(){
        $data = Struktur_kepengurusan_title::first();
        return view('vendor.backpack.base.struktur_kepengurusan.title', ['data'=>$data]);
    }

    public function update_title(Request $request)
    {
        $company_profile = Struktur_kepengurusan_title::first();
        if (empty($company_profile)) {
            $company_profile = new Struktur_kepengurusan_title;
        }

        // if ($request->hasFile('image')) {
        //     if ($request->input('old_image') != null) {
        //         $oldimage = base_path() . '/public/upload/' . $request->input('old_image');
        //         if (file_exists($oldimage)) {
        //             unlink($oldimage);
        //         }
        //     }
        //     $imageName = 'mengapa_'.rand().'.'.$request->file('image')->getClientOriginalExtension();
        //     $path = base_path() . '/public/upload';
        //     $request->file('image')->move($path, $imageName);
            
        // } else {
        //     $imageName = $request->input('old_image');
        // }

//        $company_profile->image =$imageName;

        $company_profile->title =$request->input('title');
        $company_profile->description =$request->input('description');
        $company_profile->link =$request->input('link');
        $company_profile->save();

        $request->session()->flash('update', 'Success');
        return redirect()->route('struktur_kepengurusan_title_view');
    }
}
