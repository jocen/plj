<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Informasi;
use App\Models\Informasi_category;
use App\Models\Informasi_title;
use Illuminate\Http\Request;

class InformasiController extends Controller
{
    public function view()
    {
        $data = Informasi::orderby('sort','asc')->get();
        return view('vendor.backpack.base.informasi.list', ['data' => $data]);
    }
    public function create()
    {
        $category = Informasi_category::get();
        return view('vendor.backpack.base.informasi.create', ['category' => $category]);
    }
    public function edit($id)
    {
        $data = Informasi::find($id);
        $category = Informasi_category::get();
        return view('vendor.backpack.base.informasi.edit', [
            'data' => $data,
            'category' => $category,
        ]);
    }
    public function insert(Request $request)
    {
        $validatedData = $request->validate([
            'category_id' => 'required',
        ]);

        $last_sort = Informasi::orderby('sort', 'desc')->first();
        $sort = 1;
        if($last_sort){
            $sort = $last_sort->sort + 1;
        }

        $table = new Informasi;
        $table->question = $request->input('question');
        $table->answer = $request->input('answer');
        $table->informasi_category_id = $request->input('category_id');
        $table->sort = $sort;
        $table->save();

        $request->session()->flash('insert', 'Success');
        return redirect()->route('informasi_view');
    }
    public function update(Request $request)
    {
        $validatedData = $request->validate([
            'category_id' => 'required',
        ]);

        $imageName = "";

        $table = Informasi::find($request->input('id'));
        $table->question = $request->input('question');
        $table->answer = $request->input('answer');
        $table->informasi_category_id = $request->input('category_id');
        $table->save();

        $request->session()->flash('update', 'Success');
        return redirect()->route('informasi_view');
    }
    public function delete($id, Request $request)
    {
        $table = Informasi::find($id);
        $table->delete();

        $request->session()->flash('delete', 'Success');
        return redirect()->route('informasi_view');
    }

    public function status($id,$status){
        $table = Informasi::find($id);
        $table->status = $status;
        $table->Save();

        return redirect()->route('informasi_view');
    }

    public function update_sort(Request $request){
        $maincontent_id = $request->input('maincontent_id');
        $oldsort = $request->input('oldsort');
        $newsort = $request->input('newsort');

        $getTable = Informasi::where('id',$maincontent_id)->where('sort',$oldsort)->first();
        $getTable->sort = $newsort;
        $getTable->save();

        $status=array('status'=>'1','message'=>'Success');
        return response()->json($status);
    }  

    public function title(){
        $data = Informasi_title::first();
        return view('vendor.backpack.base.informasi.title', ['data'=>$data]);
    }

    public function update_title(Request $request)
    {
        $company_profile = Informasi_title::first();
        if (empty($company_profile)) {
            $company_profile = new Informasi_title;
        }

        if ($request->hasFile('image')) {
            if ($request->input('old_image') != null) {
                $oldimage = base_path() . '/public/upload/' . $request->input('old_image');
                if (file_exists($oldimage)) {
                    unlink($oldimage);
                }
            }
            $imageName = 'informasi_'.rand().'.'.$request->file('image')->getClientOriginalExtension();
            $path = base_path() . '/public/upload';
            $request->file('image')->move($path, $imageName);
            
        } else {
            $imageName = $request->input('old_image');
        }

       $company_profile->image =$imageName;

        $company_profile->title =$request->input('title');
        $company_profile->description =$request->input('description');
        $company_profile->save();

        $request->session()->flash('update', 'Success');
        return redirect()->route('informasi_title_view');
    }
}
