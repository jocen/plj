<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Dasar_pemikiran;
use App\Models\Dasar_pemikiran_image;
use Illuminate\Http\Request;

class DasarPemikiranController extends Controller
{
    public function view()
    {
        $data = Dasar_pemikiran::orderby('sort','asc')->get();
        $image = Dasar_pemikiran_image::first();
        return view('vendor.backpack.base.dasar_pemikiran.list', ['data' => $data, 'image'=>$image]);
    }
    public function create()
    {
        return view('vendor.backpack.base.dasar_pemikiran.create');
    }
    public function edit($id)
    {
        $data = Dasar_pemikiran::find($id);
        return view('vendor.backpack.base.dasar_pemikiran.edit', ['data' => $data]);
    }
    public function insert(Request $request)
    {

        $imageName = "";
        $imageNameMobile = "";
        $last_sort = Dasar_pemikiran::orderby('sort', 'desc')->first();
            $sort = 1;
            if($last_sort){
                $sort = $last_sort->sort + 1;
            }

        $table = new Dasar_pemikiran;
        $table->title = $request->input('title');
        $table->image = $imageName;
        $table->link = $request->input('link');
        $table->description = $request->input('description');
        $table->type = $request->input('type');
        $table->sort = $sort;
        $table->save();

        $detail = Dasar_pemikiran::where('id',$table->id)->first();
        if ($request->hasFile('image')) {
            $imageTempName = $request->file('image')->getPathname();
            $imageName = $detail->slug .rand(). '.' . $request->file('image')->getClientOriginalExtension();
            $path = base_path() . '/public/upload';
            $request->file('image')->move($path, $imageName);

            $detail->image = $imageName;
            $detail->save();
        }

        $request->session()->flash('insert', 'Success');
        return redirect()->route('dasar_pemikiran_view');
    }
    public function update(Request $request)
    {

        $imageName = "";
        $imageNameMobile = "";

        $table = Dasar_pemikiran::find($request->input('id'));
        $table->title = $request->input('title');
        $table->image = $imageName;
        $table->link = $request->input('link');
        $table->description = $request->input('description');
        $table->type = $request->input('type');
        $table->save();

        $detail = Dasar_pemikiran::where('id',$request->input('id'))->first();
        if ($request->hasFile('image')) {
            if ($request->input('old_image') != null) {
                $oldimage = base_path() . '/public/upload/' . $request->input('old_image');
                if (file_exists($oldimage)) {
                    unlink($oldimage);
                }
            }

            $imageTempName = $request->file('image')->getPathname();
            $imageName = $detail->slug .rand().'.' . $request->file('image')->getClientOriginalExtension();
            $path = base_path() . '/public/upload';
            $request->file('image')->move($path, $imageName);
            
        } else {
            $imageName = $request->input('old_image');
        }


        $detail->image = $imageName;
        $detail->save();
        $request->session()->flash('update', 'Success');
        return redirect()->route('dasar_pemikiran_view');
    }
    public function delete($id, Request $request)
    {
        $table = Dasar_pemikiran::find($id);
        $table->delete();

        $request->session()->flash('delete', 'Success');
        return redirect()->route('dasar_pemikiran_view');
    }

    public function status($id,$status){
        $table = Dasar_pemikiran::find($id);
        $table->status = $status;
        $table->Save();

        return redirect()->route('dasar_pemikiran_view');
    }

    public function update_sort(Request $request){
        $maincontent_id = $request->input('maincontent_id');
        $oldsort = $request->input('oldsort');
        $newsort = $request->input('newsort');

        $getTable = Dasar_pemikiran::where('id',$maincontent_id)->where('sort',$oldsort)->first();
        $getTable->sort = $newsort;
        $getTable->save();

        $status=array('status'=>'1','message'=>'Success');
        return response()->json($status);
    }

     function updateImage(Request $request){
            
        $our = Dasar_pemikiran_image::first();
        if (empty($our)) {
            $our = new Dasar_pemikiran_image;
        }

        if ($request->hasFile('image')) {
            if ($request->input('old_image') != null) {
                $oldimage = base_path() . '/public/upload/' . $request->input('old_image');
                if (file_exists($oldimage)) {
                    unlink($oldimage);
                }
            }
            $imageName = 'dasar_pemikiran_'.rand().'.'.$request->file('image')->getClientOriginalExtension();
            $path = base_path() . '/public/upload';
            $request->file('image')->move($path, $imageName);
            
        } else {
            $imageName = $request->input('old_image');
        }

        $our->image =$imageName;
        $our->save();

        $request->session()->flash('update', 'Success');
        return redirect()->route('dasar_pemikiran_view');
    }

}
