<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Galeri;
use App\Models\Galeri_title;
use Illuminate\Http\Request;

class GaleriController extends Controller
{
    public function view()
    {
        $data = Galeri::orderby('sort','asc')->get();
        return view('vendor.backpack.base.galeri.list', ['data' => $data]);
    }
    public function create()
    {
        return view('vendor.backpack.base.galeri.create');
    }
    public function edit($id)
    {
        $data = Galeri::find($id);
        return view('vendor.backpack.base.galeri.edit', ['data' => $data]);
    }
    public function insert(Request $request)
    {

        $imageName = "";
        $imageNameMobile = "";
        $last_sort = Galeri::orderby('sort', 'desc')->first();
            $sort = 1;
            if($last_sort){
                $sort = $last_sort->sort + 1;
            }

        $table = new Galeri;
        $table->title = $request->input('title');
        $table->image = $imageName;
        $table->link = $request->input('link');
        $table->description = $request->input('description');
        $table->type = $request->input('type');
        $table->sort = $sort;
        $table->save();

        $detail = Galeri::where('id',$table->id)->first();
        if ($request->hasFile('image')) {
            $imageTempName = $request->file('image')->getPathname();
            $imageName = $detail->slug . '.' . $request->file('image')->getClientOriginalExtension();
            $path = base_path() . '/public/upload/galeri';
            $request->file('image')->move($path, $imageName);

            $detail->image = $imageName;
            $detail->save();
        }

        $request->session()->flash('insert', 'Success');
        return redirect()->route('galeri_view');
    }
    public function update(Request $request)
    {
        $imageName = "";
        $imageNameMobile = "";

        $table = Galeri::find($request->input('id'));
        $table->title = $request->input('title');
        $table->image = $imageName;
        $table->link = $request->input('link');
        $table->description = $request->input('description');
        $table->type = $request->input('type');
        $table->save();

        $detail = Galeri::where('id',$request->input('id'))->first();
        if ($request->hasFile('image')) {
            if ($request->input('old_image') != null) {
                $oldimage = base_path() . '/public/upload/galeri/' . $request->input('old_image');
                if (file_exists($oldimage)) {
                    unlink($oldimage);
                }
            }

            $imageTempName = $request->file('image')->getPathname();
            $imageName = $detail->slug . '.' . $request->file('image')->getClientOriginalExtension();
            $path = base_path() . '/public/upload/galeri';
            $request->file('image')->move($path, $imageName);
            
        } else {
            $imageName = $request->input('old_image');
        }


        $detail->image = $imageName;
        $detail->save();
        $request->session()->flash('update', 'Success');
        return redirect()->route('galeri_view');
    }
    public function delete($id, Request $request)
    {
        $table = Galeri::find($id);
        $table->delete();

        $request->session()->flash('delete', 'Success');
        return redirect()->route('galeri_view');
    }

    public function status($id,$status){
        $table = Galeri::find($id);
        $table->status = $status;
        $table->Save();

        return redirect()->route('galeri_view');
    }

    public function update_sort(Request $request){
        $maincontent_id = $request->input('maincontent_id');
        $oldsort = $request->input('oldsort');
        $newsort = $request->input('newsort');

        $getTable = Galeri::where('id',$maincontent_id)->where('sort',$oldsort)->first();
        $getTable->sort = $newsort;
        $getTable->save();

        $status=array('status'=>'1','message'=>'Success');
        return response()->json($status);
    }

    public function title(){
        $data = Galeri_title::first();
        return view('vendor.backpack.base.galeri.title', ['data'=>$data]);
    }

    public function update_title(Request $request)
    {
        $company_profile = Galeri_title::first();
        if (empty($company_profile)) {
            $company_profile = new Galeri_title;
        }

        // if ($request->hasFile('image')) {
        //     if ($request->input('old_image') != null) {
        //         $oldimage = base_path() . '/public/upload/' . $request->input('old_image');
        //         if (file_exists($oldimage)) {
        //             unlink($oldimage);
        //         }
        //     }
        //     $imageName = 'mengapa_'.rand().'.'.$request->file('image')->getClientOriginalExtension();
        //     $path = base_path() . '/public/upload';
        //     $request->file('image')->move($path, $imageName);
            
        // } else {
        //     $imageName = $request->input('old_image');
        // }

//        $company_profile->image =$imageName;

        $company_profile->title =$request->input('title');
        $company_profile->description =$request->input('description');
        $company_profile->link =$request->input('link');
        $company_profile->link_name = $request->input('link_name');
        $company_profile->save();

        $request->session()->flash('update', 'Success');
        return redirect()->route('galeri_title_view');
    }
}
