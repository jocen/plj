<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Artikel;
use App\Models\Kategori;
use Illuminate\Http\Request;
use DB;
use Image;

class ArtikelController extends Controller
{
    public function view()
    {
        $data = Artikel::where('featured',0)->orderby('id','desc')->get();
        $featured = Artikel::where('featured',1)->orderby('sort','asc')->get();
        return view('vendor.backpack.base.artikel.list', ['data' => $data, 'featured' => $featured]);
    }
    public function create()
    {
        $kategori = Kategori::where('status',1)->get();
        return view('vendor.backpack.base.artikel.create', ['kategori' => $kategori]);
    }
    public function edit($id)
    {
        $data = Artikel::find($id);
        $kategori = Kategori::where('status',1)->get();
        return view('vendor.backpack.base.artikel.edit', ['data' => $data,'kategori' => $kategori]);
    }
    public function insert(Request $request)
    {
        DB::transaction(function () use($request) {
            $kategori = (null !== $request->input('kategori')) ? $request->input('kategori') : [];
            $imageName = "";

            $last_sort = Artikel::orderby('sort', 'desc')->first();
            $sort = 1;
            if($last_sort){
                $sort = $last_sort->sort + 1;
            }

            $artikel = new Artikel;
            $artikel->title = $request->input('title');
            $artikel->description = $request->input('description');
            $artikel->date = $request->input('date');
            $artikel->sumber = $request->input('sumber');
            $artikel->sort = $sort;
            $artikel->save();

            $artikel->kategori()->sync($kategori);

            $detail = Artikel::where('id',$artikel->id)->first();
            if ($request->hasFile('image')) {
                $imageTempName = $request->file('image')->getPathname();
                $imageName = $detail->slug . '.' . $request->file('image')->getClientOriginalExtension();
                $path = base_path() . '/public/upload/artikel';
                $request->file('image')->move($path, $imageName);

                $detail->image = $imageName;
                $detail->save();
            }


            });
        $request->session()->flash('insert', 'Success');
        return redirect()->route('artikel_view');
    }
    public function update(Request $request)
    {
        DB::transaction(function () use($request) {
        $kategori = (null !== $request->input('kategori')) ? $request->input('kategori') : [];

        $imageName = "";

        $artikel = Artikel::find($request->input('id'));
        $artikel->title = $request->input('title');
        $artikel->description = $request->input('description');
        $artikel->date = $request->input('date');
        $artikel->sumber = $request->input('sumber');
        $artikel->save();

        $artikel->kategori()->sync($kategori);

        $detail = Artikel::where('id',$request->input('id'))->first();
        if ($request->hasFile('image')) {
            if ($request->input('old_image') != null) {
                $oldimage = base_path() . '/public/upload/artikel/' . $request->input('old_image');
                if (file_exists($oldimage)) {
                    unlink($oldimage);
                }
            }

            $imageTempName = $request->file('image')->getPathname();
            $imageName = $detail->slug . '.' . $request->file('image')->getClientOriginalExtension();
            $path = base_path() . '/public/upload/artikel';
            $request->file('image')->move($path, $imageName);
            
        } else {
            $imageName = $request->input('old_image');
        }

        $detail->image = $imageName;
        $detail->save();

        if($detail->featured == 1){
                if ($request->hasFile('banner')) {
                    if ($request->input('old_banner') != null) {
                        $oldimagebanner = base_path() . '/public/upload/artikel/' . $request->input('old_banner');
                        if (file_exists($oldimagebanner)) {
                            unlink($oldimagebanner);
                        }
                    }

                    $imageNameBanner = $detail->slug . '_banner.' . $request->file('banner')->getClientOriginalExtension();
                    $path = base_path() . '/public/upload/artikel';
                    $request->file('banner')->move($path, $imageNameBanner);
                    
                } else {
                    $imageNameBanner = $request->input('old_banner');
                }
                $detail->banner = $imageNameBanner;
                $detail->save();
            }
            
        });

        $request->session()->flash('update', 'Success');
        return redirect()->route('artikel_view');
    }
    public function delete($id, Request $request)
    {
        $table = Artikel::find($id);
        $table->delete();

        $request->session()->flash('delete', 'Success');
        return redirect()->route('artikel_view');
    }

    public function status($id,$status){
        $table = Artikel::find($id);
        $table->status = $status;
        $table->Save();

        return redirect()->route('artikel_view');
    }

    public function update_sort(Request $request){
        $maincontent_id = $request->input('maincontent_id');
        $oldsort = $request->input('oldsort');
        $newsort = $request->input('newsort');

        $getTable = Artikel::where('id',$maincontent_id)->where('sort',$oldsort)->first();
        $getTable->sort = $newsort;
        $getTable->save();

        $status=array('status'=>'1','message'=>'Success');
        return response()->json($status);
    }  

    public function featured($id,$status){
        $table = Artikel::find($id);

        $last_sort = Artikel::where('featured',1)->orderby('sort', 'desc')->first();
            $sort = 1;
            if($last_sort){
                $sort = $last_sort->sort + 1;
        }

        $table->featured = $status;
        $table->sort = $sort;
        $table->Save();

        return redirect()->route('artikel_view');
    }
}
