<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Pengurus;
use App\Models\Cabang;
use App\Models\Pengurus_title;
use Illuminate\Http\Request;

class PengurusController extends Controller
{
    public function view()
    {
        $data = Pengurus::orderby('sort','asc')->get();
        return view('vendor.backpack.base.pengurus.list', ['data' => $data]);
    }
    public function create()
    {
        $cabang = Cabang::where('status',1)->orderby('sort','asc')->get();
        return view('vendor.backpack.base.pengurus.create',['cabang'=>$cabang]);
    }
    public function edit($id)
    {
        $cabang = Cabang::where('status',1)->orderby('sort','asc')->get();
        $data = Pengurus::find($id);
        return view('vendor.backpack.base.pengurus.edit', ['data' => $data,'cabang'=>$cabang]);
    }
    public function insert(Request $request)
    {
        // $validatedData = $request->validate([
        //     'name' => 'required|max:255|unique:pengurus',
        // ]);

        $imageName = "";
        $imageNameMobile = "";
        $last_sort = Pengurus::orderby('sort', 'desc')->first();
            $sort = 1;
            if($last_sort){
                $sort = $last_sort->sort + 1;
            }

        $table = new Pengurus;
        $table->name = $request->input('name');
        //$table->image = $imageName;
        $table->title = $request->input('title');
        $table->cabang_id = $request->input('cabang_id');
        $table->sort = $sort;
        $table->save();

        $detail = Pengurus::where('id',$table->id)->first();
        if ($request->hasFile('image')) {
            $imageTempName = $request->file('image')->getPathname();
            $imageName = $detail->slug . '.' . $request->file('image')->getClientOriginalExtension();
            $path = base_path() . '/public/upload/pengurus';
            $request->file('image')->move($path, $imageName);

            $detail->image = $imageName;
            $detail->save();
        }

        $request->session()->flash('insert', 'Success');
        return redirect()->route('pengurus_view');
    }
    public function update(Request $request)
    {
        // $validatedData = $request->validate([
        //     'name' => 'required|max:255|unique:pengurus,name,'.$request->input('id'),
        // ]);

        $imageName = "";
        $imageNameMobile = "";

        $table = Pengurus::find($request->input('id'));
        $table->name = $request->input('name');
        $table->image = $imageName;
        $table->title = $request->input('title');
        $table->cabang_id = $request->input('cabang_id');
        $table->save();

        $detail = Pengurus::where('id',$request->input('id'))->first();
        if ($request->hasFile('image')) {
            if ($request->input('old_image') != null) {
                $oldimage = base_path() . '/public/upload/pengurus/' . $request->input('old_image');
                if (file_exists($oldimage)) {
                    unlink($oldimage);
                }
            }

            $imageTempName = $request->file('image')->getPathname();
            $imageName = $detail->slug . '.' . $request->file('image')->getClientOriginalExtension();
            $path = base_path() . '/public/upload/pengurus';
            $request->file('image')->move($path, $imageName);
            
        } else {
            $imageName = $request->input('old_image');
        }


        $detail->image = $imageName;
        $detail->save();
        $request->session()->flash('update', 'Success');
        return redirect()->route('pengurus_view');
    }
    public function delete($id, Request $request)
    {
        $table = Pengurus::find($id);
        $table->delete();

        $request->session()->flash('delete', 'Success');
        return redirect()->route('pengurus_view');
    }

    public function status($id,$status){
        $table = Pengurus::find($id);
        $table->status = $status;
        $table->Save();

        return redirect()->route('pengurus_view');
    }

    public function update_sort(Request $request){
        $maincontent_id = $request->input('maincontent_id');
        $oldsort = $request->input('oldsort');
        $newsort = $request->input('newsort');

        $getTable = Pengurus::where('id',$maincontent_id)->where('sort',$oldsort)->first();
        $getTable->sort = $newsort;
        $getTable->save();

        $status=array('status'=>'1','message'=>'Success');
        return response()->json($status);
    }

    public function title(){
        $data = Pengurus_title::first();
        return view('vendor.backpack.base.pengurus.title', ['data'=>$data]);
    }

    public function update_title(Request $request)
    {
        $company_profile = Pengurus_title::first();
        if (empty($company_profile)) {
            $company_profile = new Pengurus_title;
        }

        // if ($request->hasFile('image')) {
        //     if ($request->input('old_image') != null) {
        //         $oldimage = base_path() . '/public/upload/' . $request->input('old_image');
        //         if (file_exists($oldimage)) {
        //             unlink($oldimage);
        //         }
        //     }
        //     $imageName = 'pengurus_'.rand().'.'.$request->file('image')->getClientOriginalExtension();
        //     $path = base_path() . '/public/upload';
        //     $request->file('image')->move($path, $imageName);
            
        // } else {
        //     $imageName = $request->input('old_image');
        // }

//        $company_profile->image =$imageName;

        $company_profile->title =$request->input('title');
        $company_profile->description =$request->input('description');
        //$company_profile->image =$imageName;
        $company_profile->save();

        $request->session()->flash('update', 'Success');
        return redirect()->route('pengurus_title_view');
    }
}
