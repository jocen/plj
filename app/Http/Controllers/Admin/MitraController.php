<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Mitra;
use App\Models\Mitra_title;
use Illuminate\Http\Request;

class MitraController extends Controller
{
    public function view()
    {
        $data = Mitra::orderby('sort','asc')->get();
        return view('vendor.backpack.base.mitra.list', ['data' => $data]);
    }
    public function create()
    {
        return view('vendor.backpack.base.mitra.create');
    }
    public function edit($id)
    {
        $data = Mitra::find($id);
        return view('vendor.backpack.base.mitra.edit', ['data' => $data]);
    }
    public function insert(Request $request)
    {

        $imageName = "";
        $imageNameMobile = "";
        $last_sort = Mitra::orderby('sort', 'desc')->first();
            $sort = 1;
            if($last_sort){
                $sort = $last_sort->sort + 1;
            }

        $table = new Mitra;
        $table->title = $request->input('title');
        $table->image = $imageName;
        $table->video = $request->input('video');
        // $table->description = $request->input('description');
        // $table->type = $request->input('type');
        $table->sort = $sort;
        $table->save();

        // $detail = Mitra::where('id',$table->id)->first();
        // if ($request->hasFile('image')) {
        //     $imageTempName = $request->file('image')->getPathname();
        //     $imageName = $detail->slug . '.' . $request->file('image')->getClientOriginalExtension();
        //     $path = base_path() . '/public/upload/mitra';
        //     $request->file('image')->move($path, $imageName);

        //     $detail->image = $imageName;
        //     $detail->save();
        // }

        $request->session()->flash('insert', 'Success');
        return redirect()->route('mitra_view');
    }
    public function update(Request $request)
    {
        $imageName = "";
        $imageNameMobile = "";

        $table = Mitra::find($request->input('id'));
        $table->title = $request->input('title');
        $table->image = $imageName;
        $table->video = $request->input('video');
        // $table->description = $request->input('description');
        // $table->type = $request->input('type');
        $table->save();

        // $detail = mitra::where('id',$request->input('id'))->first();
        // if ($request->hasFile('image')) {
        //     if ($request->input('old_image') != null) {
        //         $oldimage = base_path() . '/public/upload/mitra/' . $request->input('old_image');
        //         if (file_exists($oldimage)) {
        //             unlink($oldimage);
        //         }
        //     }

        //     $imageTempName = $request->file('image')->getPathname();
        //     $imageName = $detail->slug . '.' . $request->file('image')->getClientOriginalExtension();
        //     $path = base_path() . '/public/upload/mitra';
        //     $request->file('image')->move($path, $imageName);
            
        // } else {
        //     $imageName = $request->input('old_image');
        // }


        // $detail->image = $imageName;
        //$detail->save();
        $request->session()->flash('update', 'Success');
        return redirect()->route('mitra_view');
    }
    public function delete($id, Request $request)
    {
        $table = Mitra::find($id);
        $table->delete();

        $request->session()->flash('delete', 'Success');
        return redirect()->route('mitra_view');
    }

    public function status($id,$status){
        $table = Mitra::find($id);
        $table->status = $status;
        $table->Save();

        return redirect()->route('mitra_view');
    }

    public function update_sort(Request $request){
        $maincontent_id = $request->input('maincontent_id');
        $oldsort = $request->input('oldsort');
        $newsort = $request->input('newsort');

        $getTable = Mitra::where('id',$maincontent_id)->where('sort',$oldsort)->first();
        $getTable->sort = $newsort;
        $getTable->save();

        $status=array('status'=>'1','message'=>'Success');
        return response()->json($status);
    }

    public function title(){
        $data = Mitra_title::first();
        return view('vendor.backpack.base.mitra.title', ['data'=>$data]);
    }

    public function update_title(Request $request)
    {
        $company_profile = Mitra_title::first();
        if (empty($company_profile)) {
            $company_profile = new Mitra_title;
        }

        // if ($request->hasFile('image')) {
        //     if ($request->input('old_image') != null) {
        //         $oldimage = base_path() . '/public/upload/' . $request->input('old_image');
        //         if (file_exists($oldimage)) {
        //             unlink($oldimage);
        //         }
        //     }
        //     $imageName = 'mengapa_'.rand().'.'.$request->file('image')->getClientOriginalExtension();
        //     $path = base_path() . '/public/upload';
        //     $request->file('image')->move($path, $imageName);
            
        // } else {
        //     $imageName = $request->input('old_image');
        // }

//        $company_profile->image =$imageName;

        $company_profile->title =$request->input('title');
        $company_profile->description =$request->input('description');
        $company_profile->youtube_link1 = $request->input('youtube_link1');
        $company_profile->youtube_link2 = $request->input('youtube_link2');
        $company_profile->youtube_link_name1 = $request->input('youtube_link_name1');
        $company_profile->youtube_link_name2 = $request->input('youtube_link_name2');
        $company_profile->save();

        $request->session()->flash('update', 'Success');
        return redirect()->route('mitra_title_view');
    }
}
