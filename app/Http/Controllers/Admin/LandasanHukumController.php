<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Landasan_hukum;
use App\Models\Landasan_hukum_title;
use Illuminate\Http\Request;

class LandasanHukumController extends Controller
{
    public function view()
    {
        $data = Landasan_hukum::orderby('sort','asc')->get();
        return view('vendor.backpack.base.landasan_hukum.list', ['data' => $data]);
    }
    public function create()
    {
        return view('vendor.backpack.base.landasan_hukum.create');
    }
    public function edit($id)
    {
        $data = Landasan_hukum::find($id);
        return view('vendor.backpack.base.landasan_hukum.edit', ['data' => $data]);
    }
    public function insert(Request $request)
    {

        $imageName = "";
        $imageNameMobile = "";
        $last_sort = Landasan_hukum::orderby('sort', 'desc')->first();
            $sort = 1;
            if($last_sort){
                $sort = $last_sort->sort + 1;
            }

        $table = new Landasan_hukum;
        $table->name = $request->input('name');
        //$table->image = $imageName;
        $table->whatsapp = $request->input('whatsapp');
        $table->description = $request->input('description');
        $table->sort = $sort;
        $table->save();

        $detail = Landasan_hukum::where('id',$table->id)->first();
        if ($request->hasFile('image')) {
            $imageTempName = $request->file('image')->getPathname();
            $imageName = $detail->slug . '_landasan_hukum.' . $request->file('image')->getClientOriginalExtension();
            $path = base_path() . '/public/upload';
            $request->file('image')->move($path, $imageName);

            $detail->image = $imageName;
            $detail->save();
        }

        $request->session()->flash('insert', 'Success');
        return redirect()->route('landasan_hukum_view');
    }
    public function update(Request $request)
    {

        $imageName = "";
        $imageNameMobile = "";

        $table = Landasan_hukum::find($request->input('id'));
        $table->name = $request->input('name');
        $table->image = $imageName;
        $table->whatsapp = $request->input('whatsapp');
        $table->description = $request->input('description');
        $table->save();

        $detail = Landasan_hukum::where('id',$request->input('id'))->first();
        if ($request->hasFile('image')) {
            if ($request->input('old_image') != null) {
                $oldimage = base_path() . '/public/upload/' . $request->input('old_image');
                if (file_exists($oldimage)) {
                    unlink($oldimage);
                }
            }

            $imageTempName = $request->file('image')->getPathname();
            $imageName = $detail->slug . '_landasan_hukum.' . $request->file('image')->getClientOriginalExtension();
            $path = base_path() . '/public/upload';
            $request->file('image')->move($path, $imageName);
            
        } else {
            $imageName = $request->input('old_image');
        }


        $detail->image = $imageName;
        $detail->save();
        $request->session()->flash('update', 'Success');
        return redirect()->route('landasan_hukum_view');
    }
    public function delete($id, Request $request)
    {
        $table = Landasan_hukum::find($id);
        $table->delete();

        $request->session()->flash('delete', 'Success');
        return redirect()->route('landasan_hukum_view');
    }

    public function status($id,$status){
        $table = Landasan_hukum::find($id);
        $table->status = $status;
        $table->Save();

        return redirect()->route('landasan_hukum_view');
    }

    public function update_sort(Request $request){
        $maincontent_id = $request->input('maincontent_id');
        $oldsort = $request->input('oldsort');
        $newsort = $request->input('newsort');

        $getTable = Landasan_hukum::where('id',$maincontent_id)->where('sort',$oldsort)->first();
        $getTable->sort = $newsort;
        $getTable->save();

        $status=array('status'=>'1','message'=>'Success');
        return response()->json($status);
    }

    public function title(){
        $data = Landasan_hukum_title::first();
        return view('vendor.backpack.base.landasan_hukum.title', ['data'=>$data]);
    }

    public function update_title(Request $request)
    {
        $company_profile = Landasan_hukum_title::first();
        if (empty($company_profile)) {
            $company_profile = new Landasan_hukum_title;
        }

        if ($request->hasFile('image')) {
            if ($request->input('old_image') != null) {
                $oldimage = base_path() . '/public/upload/' . $request->input('old_image');
                if (file_exists($oldimage)) {
                    unlink($oldimage);
                }
            }
            $imageName = 'landasan_hukum_'.rand().'.'.$request->file('image')->getClientOriginalExtension();
            $path = base_path() . '/public/upload';
            $request->file('image')->move($path, $imageName);
            
        } else {
            $imageName = $request->input('old_image');
        }

//        $company_profile->image =$imageName;

        $company_profile->title =$request->input('title');
        $company_profile->description =$request->input('description');
        $company_profile->image =$imageName;
        $company_profile->save();

        $request->session()->flash('update', 'Success');
        return redirect()->route('landasan_hukum_title_view');
    }
}
