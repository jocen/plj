<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Cabang;
use App\Models\Cabang_title;
use Illuminate\Http\Request;

class CabangController extends Controller
{
    public function view()
    {
        $data = Cabang::orderby('sort','asc')->get();
        return view('vendor.backpack.base.cabang.list', ['data' => $data]);
    }
    public function create()
    {
        return view('vendor.backpack.base.cabang.create');
    }
    public function edit($id)
    {
        $data = Cabang::find($id);
        return view('vendor.backpack.base.cabang.edit', ['data' => $data]);
    }
    public function insert(Request $request)
    {
        // $validatedData = $request->validate([
        //     'name' => 'required|max:255|unique:cabang',
        // ]);

        $imageName = "";
        $imageNameMobile = "";
        $last_sort = Cabang::orderby('sort', 'desc')->first();
            $sort = 1;
            if($last_sort){
                $sort = $last_sort->sort + 1;
            }

        $table = new Cabang;
        $table->name = $request->input('name');
        //$table->image = $imageName;
        $table->whatsapp = $request->input('whatsapp');
        $table->description = $request->input('description');
        $table->link = $request->input('link');
        $table->link_name = $request->input('link_name');
        $table->sort = $sort;
        $table->save();

        $detail = cabang::where('id',$table->id)->first();
        if ($request->hasFile('image')) {
            $imageTempName = $request->file('image')->getPathname();
            $imageName = $detail->slug . '_cabang.' . $request->file('image')->getClientOriginalExtension();
            $path = base_path() . '/public/upload';
            $request->file('image')->move($path, $imageName);

            $detail->image = $imageName;
            $detail->save();
        }

        $request->session()->flash('insert', 'Success');
        return redirect()->route('cabang_view');
    }
    public function update(Request $request)
    {
        // $validatedData = $request->validate([
        //     'name' => 'required|max:255|unique:cabang,name,'.$request->input('id'),
        // ]);

        $imageName = "";
        $imageNameMobile = "";

        $table = Cabang::find($request->input('id'));
        $table->name = $request->input('name');
        $table->image = $imageName;
        $table->whatsapp = $request->input('whatsapp');
        //$table->description = $request->input('description');
        $table->link = $request->input('link');
        $table->link_name = $request->input('link_name');
        $table->save();

        $detail = Cabang::where('id',$request->input('id'))->first();
        if ($request->hasFile('image')) {
            if ($request->input('old_image') != null) {
                $oldimage = base_path() . '/public/upload/' . $request->input('old_image');
                if (file_exists($oldimage)) {
                    unlink($oldimage);
                }
            }

            $imageTempName = $request->file('image')->getPathname();
            $imageName = $detail->slug . '_cabang.' . $request->file('image')->getClientOriginalExtension();
            $path = base_path() . '/public/upload';
            $request->file('image')->move($path, $imageName);
            
        } else {
            $imageName = $request->input('old_image');
        }


        $detail->image = $imageName;
        $detail->save();
        $request->session()->flash('update', 'Success');
        return redirect()->route('cabang_view');
    }
    public function delete($id, Request $request)
    {
        $table = Cabang::find($id);
        $table->delete();

        $request->session()->flash('delete', 'Success');
        return redirect()->route('cabang_view');
    }

    public function status($id,$status){
        $table = Cabang::find($id);
        $table->status = $status;
        $table->Save();

        return redirect()->route('cabang_view');
    }

    public function update_sort(Request $request){
        $maincontent_id = $request->input('maincontent_id');
        $oldsort = $request->input('oldsort');
        $newsort = $request->input('newsort');

        $getTable = Cabang::where('id',$maincontent_id)->where('sort',$oldsort)->first();
        $getTable->sort = $newsort;
        $getTable->save();

        $status=array('status'=>'1','message'=>'Success');
        return response()->json($status);
    }

    public function title(){
        $data = Cabang_title::first();
        return view('vendor.backpack.base.cabang.title', ['data'=>$data]);
    }

    public function update_title(Request $request)
    {
        $company_profile = Cabang_title::first();
        if (empty($company_profile)) {
            $company_profile = new Cabang_title;
        }

        if ($request->hasFile('image')) {
            if ($request->input('old_image') != null) {
                $oldimage = base_path() . '/public/upload/' . $request->input('old_image');
                if (file_exists($oldimage)) {
                    unlink($oldimage);
                }
            }
            $imageName = 'cabang_'.rand().'.'.$request->file('image')->getClientOriginalExtension();
            $path = base_path() . '/public/upload';
            $request->file('image')->move($path, $imageName);
            
        } else {
            $imageName = $request->input('old_image');
        }

//        $company_profile->image =$imageName;

        $company_profile->title =$request->input('title');
        $company_profile->description =$request->input('description');
        $company_profile->image =$imageName;
        $company_profile->save();

        $request->session()->flash('update', 'Success');
        return redirect()->route('cabang_title_view');
    }
}
