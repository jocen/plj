<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Latar_belakang;
use Illuminate\Http\Request;

class LatarBelakangController extends Controller
{
    public function view($id)
    {
        $data = Latar_belakang::find($id);
        return view('vendor.backpack.base.latar_belakang.list', ['data' => $data]);
    }
    public function create()
    {
        return view('vendor.backpack.base.latar_belakang.create');
    }
    public function edit($id)
    {
        $data = latar_belakang::find($id);
        return view('vendor.backpack.base.latar_belakang.edit', ['data' => $data]);
    }
    public function insert(Request $request)
    {
        // $validatedData = $request->validate([
        //     'title' => 'required|max:255|unique:latar_belakang',
        // ]);

        $imageName = "";
        $imageNameMobile = "";
        $last_sort = latar_belakang::orderby('sort', 'desc')->first();
            $sort = 1;
            if($last_sort){
                $sort = $last_sort->sort + 1;
            }

        $table = new latar_belakang;
        $table->title = $request->input('title');
        $table->image = $imageName;
        $table->link = $request->input('link');
        $table->description = $request->input('description');
        $table->sort = $sort;
        $table->save();

        $detail = latar_belakang::where('id',$table->id)->first();
        if ($request->hasFile('image')) {
            $imageTempName = $request->file('image')->getPathname();
            $imageName = $detail->slug . '.' . $request->file('image')->getClientOriginalExtension();
            $path = base_path() . '/public/upload';
            $request->file('image')->move($path, $imageName);

            $detail->image = $imageName;
            $detail->save();
        }

        $request->session()->flash('insert', 'Success');
        return redirect()->route('latar_belakang_view');
    }
    public function update(Request $request)
    {
        $imageName = "";

        $table = Latar_belakang::find($request->input('id'));
        $table->title = $request->input('title');
        $table->image = $imageName;
        $table->description = $request->input('description');
        $table->save();

        $detail = Latar_belakang::where('id',$request->input('id'))->first();
        if ($request->hasFile('image')) {
            if ($request->input('old_image') != null) {
                $oldimage = base_path() . '/public/upload/' . $request->input('old_image');
                if (file_exists($oldimage)) {
                    unlink($oldimage);
                }
            }

            $imageTempName = $request->file('image')->getPathname();
            $imageName = 'Latar_belakang_' .rand(). '.' . $request->file('image')->getClientOriginalExtension();
            $path = base_path() . '/public/upload';
            $request->file('image')->move($path, $imageName);
            
        } else {
            $imageName = $request->input('old_image');
        }


        $detail->image = $imageName;
        $detail->save();
        $request->session()->flash('update', 'Success');
        return back();
    }
    public function delete($id, Request $request)
    {
        $table = latar_belakang::find($id);
        $table->delete();

        $request->session()->flash('delete', 'Success');
        return redirect()->route('latar_belakang_view');
    }

    public function status($id,$status){
        $table = latar_belakang::find($id);
        $table->status = $status;
        $table->Save();

        return redirect()->route('latar_belakang_view');
    }

    public function update_sort(Request $request){
        $maincontent_id = $request->input('maincontent_id');
        $oldsort = $request->input('oldsort');
        $newsort = $request->input('newsort');

        $getTable = latar_belakang::where('id',$maincontent_id)->where('sort',$oldsort)->first();
        $getTable->sort = $newsort;
        $getTable->save();

        $status=array('status'=>'1','message'=>'Success');
        return response()->json($status);
    }

    public function title(){
        $data = latar_belakang_title::first();
        return view('vendor.backpack.base.latar_belakang.title', ['data'=>$data]);
    }

    public function update_title(Request $request)
    {
        $company_profile = latar_belakang_title::first();
        if (empty($company_profile)) {
            $company_profile = new latar_belakang_title;
        }

        // if ($request->hasFile('image')) {
        //     if ($request->input('old_image') != null) {
        //         $oldimage = base_path() . '/public/upload/' . $request->input('old_image');
        //         if (file_exists($oldimage)) {
        //             unlink($oldimage);
        //         }
        //     }
        //     $imageName = 'mengapa_'.rand().'.'.$request->file('image')->getClientOriginalExtension();
        //     $path = base_path() . '/public/upload';
        //     $request->file('image')->move($path, $imageName);
            
        // } else {
        //     $imageName = $request->input('old_image');
        // }

//        $company_profile->image =$imageName;

        $company_profile->title =$request->input('title');
        $company_profile->description =$request->input('description');
        $company_profile->link =$request->input('link');
        $company_profile->save();

        $request->session()->flash('update', 'Success');
        return redirect()->route('latar_belakang_title_view');
    }
}
