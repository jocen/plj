<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Pertanyaan;
use App\Models\Pertanyaan_image;
use Illuminate\Http\Request;

class PertanyaanController extends Controller
{
    public function view()
    {
        $data = Pertanyaan::orderby('sort','asc')->get();
        $image = Pertanyaan_image::first();
        return view('vendor.backpack.base.pertanyaan.list', ['data' => $data, 'image'=>$image]);
    }
    public function create()
    {
        return view('vendor.backpack.base.pertanyaan.create');
    }
    public function edit($id)
    {
        $data = Pertanyaan::find($id);
        return view('vendor.backpack.base.pertanyaan.edit', ['data' => $data]);
    }
    public function insert(Request $request)
    {
        // $validatedData = $request->validate([
        //     'title' => 'required|max:255|unique:pertanyaan',
        // ]);

        $imageName = "";
        $imageNameMobile = "";
        $last_sort = Pertanyaan::orderby('sort', 'desc')->first();
            $sort = 1;
            if($last_sort){
                $sort = $last_sort->sort + 1;
            }

        $table = new Pertanyaan;
        $table->title = $request->input('title');
        $table->image = $imageName;
        $table->link = $request->input('link');
        $table->description = $request->input('description');
        $table->link_name = $request->input('link_name');
        $table->judul = $request->input('judul');
        $table->sort = $sort;
        $table->save();

        $detail = Pertanyaan::where('id',$table->id)->first();
        if ($request->hasFile('image')) {
            $imageTempName = $request->file('image')->getPathname();
            $imageName = $detail->slug . '.' . $request->file('image')->getClientOriginalExtension();
            $path = base_path() . '/public/upload';
            $request->file('image')->move($path, $imageName);

            $detail->image = $imageName;
            $detail->save();
        }

        $request->session()->flash('insert', 'Success');
        return redirect()->route('pertanyaan_view');
    }
    public function update(Request $request)
    {
        // $validatedData = $request->validate([
        //     'title' => 'required|max:255|unique:pertanyaan,title,'.$request->input('id'),
        // ]);

        $imageName = "";
        $imageNameMobile = "";

        $table = Pertanyaan::find($request->input('id'));
        $table->title = $request->input('title');
        $table->image = $imageName;
        $table->link = $request->input('link');
        $table->link_name = $request->input('link_name');
        $table->judul = $request->input('judul');
        $table->description = $request->input('description');
        $table->save();

        $detail = Pertanyaan::where('id',$request->input('id'))->first();
        if ($request->hasFile('image')) {
            if ($request->input('old_image') != null) {
                $oldimage = base_path() . '/public/upload/' . $request->input('old_image');
                if (file_exists($oldimage)) {
                    unlink($oldimage);
                }
            }

            $imageTempName = $request->file('image')->getPathname();
            $imageName = $detail->slug . '.' . $request->file('image')->getClientOriginalExtension();
            $path = base_path() . '/public/upload';
            $request->file('image')->move($path, $imageName);
            
        } else {
            $imageName = $request->input('old_image');
        }


        $detail->image = $imageName;
        $detail->save();
        $request->session()->flash('update', 'Success');
        return redirect()->route('pertanyaan_view');
    }
    public function delete($id, Request $request)
    {
        $table = Pertanyaan::find($id);
        $table->delete();

        $request->session()->flash('delete', 'Success');
        return redirect()->route('pertanyaan_view');
    }

    public function status($id,$status){
        $table = Pertanyaan::find($id);
        $table->status = $status;
        $table->Save();

        return redirect()->route('pertanyaan_view');
    }

    public function update_sort(Request $request){
        $maincontent_id = $request->input('maincontent_id');
        $oldsort = $request->input('oldsort');
        $newsort = $request->input('newsort');

        $getTable = Pertanyaan::where('id',$maincontent_id)->where('sort',$oldsort)->first();
        $getTable->sort = $newsort;
        $getTable->save();

        $status=array('status'=>'1','message'=>'Success');
        return response()->json($status);
    }

     function updateImage(Request $request){
            
        $our = Pertanyaan_image::first();
        if (empty($our)) {
            $our = new Pertanyaan_image;
        }

        if ($request->hasFile('image')) {
            if ($request->input('old_image') != null) {
                $oldimage = base_path() . '/public/upload/' . $request->input('old_image');
                if (file_exists($oldimage)) {
                    unlink($oldimage);
                }
            }
            $imageName = 'pertanyaan_yang_sering_'.rand().'.'.$request->file('image')->getClientOriginalExtension();
            $path = base_path() . '/public/upload';
            $request->file('image')->move($path, $imageName);
            
        } else {
            $imageName = $request->input('old_image');
        }

        $our->image =$imageName;
        $our->save();

        $request->session()->flash('update', 'Success');
        return redirect()->route('pertanyaan_view');
    }
}
