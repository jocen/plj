<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Jadwal;
use Illuminate\Http\Request;

class JadwalController extends Controller
{

    public function title(){
        $data = Jadwal::first();
        return view('vendor.backpack.base.jadwal', ['data'=>$data]);
    }

    public function update_title(Request $request)
    {
        $company_profile = Jadwal::first();
        if (empty($company_profile)) {
            $company_profile = new Jadwal;
        }

        if ($request->hasFile('image')) {
            if ($request->input('old_image') != null) {
                $oldimage = base_path() . '/public/upload/' . $request->input('old_image');
                if (file_exists($oldimage)) {
                    unlink($oldimage);
                }
            }
            $imageName = 'jadwal_'.rand().'.'.$request->file('image')->getClientOriginalExtension();
            $path = base_path() . '/public/upload';
            $request->file('image')->move($path, $imageName);
            
        } else {
            $imageName = $request->input('old_image');
        }

        $company_profile->image =$imageName;

        $company_profile->title =$request->input('title');
        $company_profile->description =$request->input('description');
        $company_profile->save();

        $request->session()->flash('update', 'Success');
        return redirect()->route('jadwal_view');
    }
}
