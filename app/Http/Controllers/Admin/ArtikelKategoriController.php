<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Kategori;
use Illuminate\Http\Request;

class ArtikelKategoriController extends Controller
{
    public function view()
    {
        $data = Kategori::orderby('id','asc')->get();
        return view('vendor.backpack.base.kategori.list', ['data' => $data]);
    }
    public function create()
    {
        return view('vendor.backpack.base.kategori.create');
    }
    public function edit($id)
    {
        $data = Kategori::find($id);
        return view('vendor.backpack.base.kategori.edit', ['data' => $data]);
    }
    public function insert(Request $request)
    {
        $last_sort = Kategori::orderby('sort', 'desc')->first();
            $sort = 1;
            if($last_sort){
                $sort = $last_sort->sort + 1;
            }

        $table = new Kategori;
        $table->name = $request->input('name');
        $table->sort = $sort;
        $table->save();

        $request->session()->flash('insert', 'Success');
        return redirect()->route('artikel_kategori_view');
    }
    public function update(Request $request)
    {

        $table = Kategori::find($request->input('id'));
        $table->name = $request->input('name');
        $table->save();

        $request->session()->flash('update', 'Success');
        return redirect()->route('artikel_kategori_view');
    }
    public function delete($id, Request $request)
    {
        $table = Kategori::find($id);
        $table->delete();

        $request->session()->flash('delete', 'Success');
        return redirect()->route('artikel_kategori_view');
    }

    public function status($id,$status){
        $table = Kategori::find($id);
        $table->status = $status;
        $table->Save();

        return redirect()->route('artikel_kategori_view');
    }

    public function update_sort(Request $request){
        $maincontent_id = $request->input('maincontent_id');
        $oldsort = $request->input('oldsort');
        $newsort = $request->input('newsort');

        $getTable = Kategori::where('id',$maincontent_id)->where('sort',$oldsort)->first();
        $getTable->sort = $newsort;
        $getTable->save();

        $status=array('status'=>'1','message'=>'Success');
        return response()->json($status);
    }
}
