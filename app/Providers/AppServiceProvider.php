<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use App\Models\Category;
use App\Models\Metadata;
use App\Models\Admin_color;
use App\Models\Menu_admin;
use App\Models\Logo;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        $metadata = Metadata::where('link',url()->current())->first();
        $color = Admin_color::first();
        $menu_admin = Menu_admin::get();
        $logo_admin = Logo::first();
        $menu_admin_home = Menu_admin::get();

        view()->share('metadata', $metadata);
        view()->share('color', $color);
        view()->share('menu_admin', $menu_admin);
        view()->share('menu_admin_home', $menu_admin_home);
        view()->share('logo_admin', $logo_admin);
    }
}
