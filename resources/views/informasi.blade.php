@extends('layout')

@section('content')

	<div class="full-banner" style="background: url('{{ asset('upload/'.$informasi_title->image) }}') no-repeat center bottom;">
		<div class="abs">
			<div class="container">
				<div class="t-banner">{{ $informasi_title->title }}</div>
			</div>
		</div>
	</div>
	
	<div class="bg-informasi" id="informasi">
		<div class="container">
			<div class="row">
				<div class="col-md-4 col-lg-3">
					<div class="accordion" id="accordionInformasi">
						<?php $c=1; ?>
                        @foreach($informasi as $informasi_category)
						<div class="accordion-item">
							<div class="accordion-header" id="heading{{ $informasi_category->id }}">
								<a class="accordion-button @if($c > 1) collapsed @endif" data-bs-toggle="collapse" data-bs-target="#collapse{{ $informasi_category->id }}" aria-expanded="@if($c > 1) false @else true @endif" aria-controls="collapse{{ $informasi_category->id }}"><i class="fas fa-chevron-right"></i> {{ $informasi_category->name }}</a>
							</div>
							<div id="collapse{{ $informasi_category->id }}" class="accordion-collapse collapse @if($c == 1) show @endif" aria-labelledby="heading{{ $informasi_category->id }}" data-bs-parent="#accordionInformasi">
								<div class="accordion-body">
									<ul class="nav nav-tabs">
										@foreach($informasi_category->informasi as $inform)
										<li class="nav-item"> 
											<a class="nav-link  @if($c == 1) active @endif" id="{{ $informasi_category->slug }}-{{ $inform->id }}-tab" data-bs-toggle="tab" data-bs-target="#{{ $informasi_category->slug }}-{{ $inform->id }}" role="tab" aria-controls="{{ $informasi_category->slug }}-{{ $inform->id }}" aria-selected="true">{!! $inform->question !!}</a>
										</li>
										@endforeach
									</ul>
								</div>
							</div>
						</div>
						<?php $c++; ?>
						@endforeach
					</div>
				</div>
				<div class="col-md-8 col-lg-9">
					<div class="tab-content">
						<?php $d=1; ?>
						@foreach($informasi as $informasi_category)
						@foreach($informasi_category->informasi as $inform)
						<div class="tab-pane fade @if($d == 1) show active @endif" id="{{ $informasi_category->slug }}-{{ $inform->id }}" role="tabpanel" aria-labelledby="{{ $informasi_category->slug }}-{{ $inform->id }}-tab">
							<div class="row">
								<div class="col-lg-10">
									<div class="bdy">
										<h1>{{ $inform->question }}</h1>
										{!! $inform->answer !!}
									</div>
								</div>
							</div>
						</div>
						@endforeach
						<?php $d++; ?>
						@endforeach
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="bg-kontak" style="background: url('{{ asset('images/bg-kontak.jpg') }}') no-repeat center;">
		<div class="container">
			<div class="t">
				<div>Juru Bahasa Isyarat Kami</div>
				<div>Siap Bantu Anda Kapan Saja!</div>
			</div>
			<div class="link">
				<a href="#">
					<button type="button" class="hvr-button">Kontak Kami</button>
				</a>
			</div>
		</div>
	</div>

@endsection

@section('js')
<script type="text/javascript">
	$(document).ready(function() {
	    $('header').addClass('white');

	    $(".collapse.show").each(function(){
			$(this).prev('.accordion-header').find(".fas").removeClass("fa-chevron-right").addClass("fa-chevron-down");
	    });
	    $(".collapse").on('show.bs.collapse', function(){
	        $(this).prev('.accordion-header').find(".fas").removeClass("fa-chevron-right").addClass("fa-chevron-down");
	    }).on('hide.bs.collapse', function(){
	        $(this).prev('.accordion-header').find(".fas").removeClass("fa-chevron-down").addClass("fa-chevron-right");
	    });
	    $('.nav-tabs li a').click(function(event) {
	    	$('.nav-tabs li a').removeClass('active');
	    	$(this).addClass('active');
	    });
	});
</script>
@endsection