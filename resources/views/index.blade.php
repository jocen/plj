@extends('layout')

@section('content')
	@if($menu_admin_home[0]->status == 1)
	<section id="beranda" data-anchor="beranda">
		<div class="banner">
			@foreach($banner as $ban)
			<div class="bg"><img src="{{asset('upload/'.$ban->image)}}"/></div>
			<div class="container pos-rel">
				<div class="w-text">
					<div class="t1 sr-left-td1">{!! $ban->title !!}</div>
					<div class="bdy sr-left-td2">
						{!! $ban->description !!}
					</div>
					<div class="link sr-left-td3">
						<a @if($ban->link) href="{{ $ban->link }}" @else class="click-kontak-kami" @endif>
							<button type="button" class="hvr-button">{{ $ban->link_name }}</button>
						</a>
					</div>
				</div>
			</div>
			@endforeach
		</div>
	</section>
	@endif

	@if($menu_admin_home[1]->status == 1)
	<div class="pad150 pb0">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-md-10 col-lg-9">
					<div class="title-global sr-down-td1">{{ $apakah->title }}</div>
					<div class="bdy-global sr-up-td1">
						{!! $apakah->description !!}
					</div>
				</div>
			</div>
		</div>
	</div>
	@endif

	@if($menu_admin_home[2]->status == 1)
	<div class="bg-tab">
		<ul class="nav nav-tabs" role="tablist">
			<?php $g=1; ?>
			@foreach($sejarah as $sej)
			<li class="nav-item" role="presentation">
				<a class="nav-link @if($g==2) active @endif" id="{{ $sej->slug }}-tab" data-bs-toggle="tab" data-bs-target="#{{ $sej->slug }}" role="tab" aria-controls="{{ $sej->slug }}" aria-selected="false">{{ $sej->title }}</a>
			</li>
			<?php $g++; ?>
			@endforeach
		</ul>
		<div class="shadow-tab">
			<div class="container">
				<div class="tab-content">
					<?php $h=1; ?>
					@foreach($sejarah as $sej)
					<div class="tab-pane fade @if($h==2) show active @endif" id="{{ $sej->slug }}" role="tabpanel" aria-labelledby="{{ $sej->slug }}-tab">
						<div class="row justify-content-center">
							@if($sej->image == null)
							<div class="col-md-10 col-lg-9 sr-down-td1">
								<div class="bdy-global">
									{!! $sej->description !!}
								</div>
							</div>
							@else
								@if($sej->type == 1)
								<div class="col-md-10">
									<div class="row">
										<div class="col-md-5">
											<div class="img"><img src="{{ asset('upload/'.$sej->image) }}"/></div>
										</div>
										<div class="col-md-7">
											<div class="bdy">
												{!! $sej->description !!}
											</div>
										</div>
									</div>
								</div>
								@else
								<div class="col-md-10">
									<div class="row">
										<div class="col-md-7">
											<div class="bdy">
												{!! $sej->description !!}
											</div>
										</div>
										<div class="col-md-5">
											<div class="img"><img src="{{ asset('upload/'.$sej->image) }}"/></div>
										</div>
									</div>
								</div>
								@endif
							@endif
						</div>
					</div>
					<?php $h++; ?>
					@endforeach
				</div>
				<div class="link sr-down-td2">
					<a href="{{ URL::to('/tentang-kami') }}#sejarah-visi">
						<button type="button" class="hvr-button">Baca lebih lanjut</button>
					</a>
				</div>
			</div>
		</div>
	</div>
	@endif

	@if($menu_admin_home[3]->status == 1)
	<div class="bg-keunggulan" style="background: url('{{ asset('images/bg-keunggulan.jpg') }}') no-repeat center;">
		<div class="container">
			<div class="t1">{{ $keunggulan_title->title }}</div>
			<div class="row">
				@foreach($keunggulan as $unggul)
				<div class="col-md-6 col-lg-3">
					<div class="item">
						<div class="img"><img src="{{ asset('upload/'.$unggul->image) }}"/></div>
						<div class="nm">{{ $unggul->title }}</div>
						<div class="bdy">
							{!! $unggul->description !!}
						</div>
					</div>
				</div>
				@endforeach
			</div>
		</div>
	</div>
	@endif
	@if($menu_admin_home[4]->status == 1)
	<section id="layanan" data-anchor="layanan">
		<div class="bg-layanan">
			<div class="container">
				<div class="tbl">
					<div class="cell w400 sr-left-td1">
						<div class="t">{{ $butuhkan_title->title }}</div>
						<div class="bdy">
							{!! $butuhkan_title->description !!}
						</div>
					</div>
					<div class="cell sr-right-td1">
						<div class="row">
							@foreach($butuhkan as $butuh)
							<div class="col-sm-4 xs30">
								<div class="item">
									<div class="img"><img src="{{ asset('upload/'.$butuh->image) }}"/></div>
									<div class="link"><a href="{{ $butuh->link }}">{{ $butuh->name }}</a></div>
								</div>
							</div>
							@endforeach
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	@endif
	@if($menu_admin_home[5]->status == 1)
	<div class="bg-klien">
		<div class="container">
			<div class="title">{{ $klien_title->title }}</div>
			<div class="row justify-content-center">
				<div class="col-md-9 col-lg-7">
					<div class="bdy">
						{!! $klien_title->description !!}
					</div>
					<div class="bdr"></div>
				</div>
			</div>

			{{--
			<div class="slider-klien">
				@foreach($klien as $klie)
				<div class="item">
					<div class="img"><img src="{{ asset('upload/'.$klie->image) }}"/></div>
				</div>
				@endforeach
			</div>
			--}}

			<!-- STATIC -->
			<div class="slider-klien">
				<div class="item">
					<div class="row">
						<div class="col-6 col-md-3">
							<div class="img"><img src="https://pljindonesia.org/upload/kompas-tv-2.jpg"/></div>
						</div>
						<div class="col-6 col-md-3">
							<div class="img"><img src="https://pljindonesia.org/upload/kompas-tv-2.jpg"/></div>
						</div>
						<div class="col-6 col-md-3">
							<div class="img"><img src="https://pljindonesia.org/upload/kompas-tv-2.jpg"/></div>
						</div>
						<div class="col-6 col-md-3">
							<div class="img"><img src="https://pljindonesia.org/upload/kompas-tv-2.jpg"/></div>
						</div>
						<div class="col-6 col-md-3">
							<div class="img"><img src="https://pljindonesia.org/upload/kompas-tv-2.jpg"/></div>
						</div>
						<div class="col-6 col-md-3">
							<div class="img"><img src="https://pljindonesia.org/upload/kompas-tv-2.jpg"/></div>
						</div>
						<div class="col-6 col-md-3">
							<div class="img"><img src="https://pljindonesia.org/upload/kompas-tv-2.jpg"/></div>
						</div>
						<div class="col-6 col-md-3">
							<div class="img"><img src="https://pljindonesia.org/upload/kompas-tv-2.jpg"/></div>
						</div>
					</div>
				</div>
				<div class="item">
					<div class="row">
						<div class="col-6 col-md-3">
							<div class="img"><img src="https://pljindonesia.org/upload/kompas-tv-2.jpg"/></div>
						</div>
						<div class="col-6 col-md-3">
							<div class="img"><img src="https://pljindonesia.org/upload/kompas-tv-2.jpg"/></div>
						</div>
						<div class="col-6 col-md-3">
							<div class="img"><img src="https://pljindonesia.org/upload/kompas-tv-2.jpg"/></div>
						</div>
						<div class="col-6 col-md-3">
							<div class="img"><img src="https://pljindonesia.org/upload/kompas-tv-2.jpg"/></div>
						</div>
						<div class="col-6 col-md-3">
							<div class="img"><img src="https://pljindonesia.org/upload/kompas-tv-2.jpg"/></div>
						</div>
						<div class="col-6 col-md-3">
							<div class="img"><img src="https://pljindonesia.org/upload/kompas-tv-2.jpg"/></div>
						</div>
						<div class="col-6 col-md-3">
							<div class="img"><img src="https://pljindonesia.org/upload/kompas-tv-2.jpg"/></div>
						</div>
						<div class="col-6 col-md-3">
							<div class="img"><img src="https://pljindonesia.org/upload/kompas-tv-2.jpg"/></div>
						</div>
					</div>
				</div>
				<div class="item">
					<div class="row">
						<div class="col-6 col-md-3">
							<div class="img"><img src="https://pljindonesia.org/upload/kompas-tv-2.jpg"/></div>
						</div>
						<div class="col-6 col-md-3">
							<div class="img"><img src="https://pljindonesia.org/upload/kompas-tv-2.jpg"/></div>
						</div>
						<div class="col-6 col-md-3">
							<div class="img"><img src="https://pljindonesia.org/upload/kompas-tv-2.jpg"/></div>
						</div>
						<div class="col-6 col-md-3">
							<div class="img"><img src="https://pljindonesia.org/upload/kompas-tv-2.jpg"/></div>
						</div>
						<div class="col-6 col-md-3">
							<div class="img"><img src="https://pljindonesia.org/upload/kompas-tv-2.jpg"/></div>
						</div>
						<div class="col-6 col-md-3">
							<div class="img"><img src="https://pljindonesia.org/upload/kompas-tv-2.jpg"/></div>
						</div>
						<div class="col-6 col-md-3">
							<div class="img"><img src="https://pljindonesia.org/upload/kompas-tv-2.jpg"/></div>
						</div>
						<div class="col-6 col-md-3">
							<div class="img"><img src="https://pljindonesia.org/upload/kompas-tv-2.jpg"/></div>
						</div>
					</div>
				</div>
			</div>

			<div class="slider-klien-mobile">
				<div class="item">
					<div class="row">
						<div class="col-6 col-md-3">
							<div class="img"><img src="https://pljindonesia.org/upload/kompas-tv-2.jpg"/></div>
						</div>
						<div class="col-6 col-md-3">
							<div class="img"><img src="https://pljindonesia.org/upload/kompas-tv-2.jpg"/></div>
						</div>
						<div class="col-6 col-md-3">
							<div class="img"><img src="https://pljindonesia.org/upload/kompas-tv-2.jpg"/></div>
						</div>
						<div class="col-6 col-md-3">
							<div class="img"><img src="https://pljindonesia.org/upload/kompas-tv-2.jpg"/></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	@endif
	@if($menu_admin_home[6]->status == 1)
	<div class="bg-gallery">
		<div class="container">
			<div class="title">{{ $galeri_title->title }}</div>
			<div class="row">
				@foreach($galeri as $gal)
				<div class="col-md-4">
					<div class="item">
						<a href="{{ URL::to('/kegiatan') }}">
							<div class="img">
								<img src="{{ asset('upload/galeri/'.$gal->image) }}"/>
							</div>
							<div class="abs">
								<div class="bg"></div>
								<div class="t">{{ $gal->title }}</div>
								<div class="bdy">
									{!! $gal->description !!}
								</div>
							</div>
						</a>
					</div>
				</div>
				@endforeach
			</div>
			<div class="link">
				<a href="{{ URL::to('/kegiatan') }}#gallery">
					<button type="button" class="hvr-button">Lihat Semua</button>
				</a>
			</div>
		</div>
	</div>
	@endif

	<br/><br/>
	<div class="container">
		<script src="https://apps.elfsight.com/p/platform.js" defer></script>
		<div class="elfsight-app-0737cee0-d8cf-4108-afde-663f131c98f3"></div>
	</div>

	@if($menu_admin_home[7]->status == 1)
	<section id="kontak-kami" data-anchor="kontak-kami">
		<div class="bg-hubungi">
			<div class="container">
				<div class="row">
					<div class="col-md-6 col-lg-5">
						<div class="t1">{{ $kontak_kami->title }}</div>
						<div class="t2">Alamat Surat</div>
						<div class="bdy mb20">
							{!! $kontak_kami->address !!}
						</div>
						<div class="bdy">
							<p><span class="bold">Catatan:</span> {!! $kontak_kami->catatan !!}</p>
						</div>
						<div class="t3">Informasi Kontak</div>
						<ul class="l-kami">
    						<li class="img"><img src="{{ asset('images/fax.png') }}" alt="" title=""></li>
    						<li>{{ $kontak_kami->fax }}</li>
    					</ul>
    					<ul class="l-kami">
    						<li class="img"><img src="{{ asset('images/mail.png') }}" alt="" title=""></li>
    						<li>{{ $kontak_kami->email }}</li>
    					</ul>
    					<ul class="l-kami">
    						<li class="img"><img src="{{ asset('images/website.png') }}" alt="" title=""></li>
    						<li>{{ $kontak_kami->website }}</li>
    					</ul>
    					<ul class="l-kami">
    						<li class="img"><img src="{{ asset('images/wa.png') }}" alt="" title=""></li>
    						<li>{{ $kontak_kami->whatsapp }}</li>
    					</ul>
					</div>
					<div class="col-md-6 col-lg-5 offset-lg-1">
						<div class="img mt15"><img src="{{ asset('images/bg-kami.jpg') }}" alt="" title=""></div>
						<div class="bdy-kami">
							{!! $kontak_kami->description !!}
						</div>
						<ul class="l-pay">
							<li>
								<a href="{{ $kontak_kami->donasi }}">
									<button type="button" class="hvr-button">Donasi</button>
								</a>
							</li>
							<li><div class="img-pay"><img src="{{ asset('images/mandiri.png') }}" alt="" title=""></div></li>
							<li><div class="img-pay"><img src="{{ asset('images/paypal.png') }}" alt="" title=""></div></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</section>
	@endif
	<div class="bg-kontak">
		<div class="bg"><img src="{{ asset('images/bg-kontak.jpg') }}"/></div>
		<div class="container pos-rel">
			<div class="t">
				<div>Juru Bahasa Isyarat Kami</div>
				<div>Siap Bantu Anda Kapan Saja!</div>
			</div>
			<div class="link">
				<a href="#">
					<button type="button" class="hvr-button">Kontak Kami</button>
				</a>
			</div>
		</div>
	</div>

@endsection

@section('js')
<script type="text/javascript">
	$(document).ready(function() {
		$('.slider-klien').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            dots: false,
            centerMode: false,
            arrows: false,
            infinite: true,
            autoplay: true,
            autoplaySpeed: 1000,
            speed: 1000	
        });

        $('.slider-klien-mobile').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            dots: false,
            centerMode: false,
            arrows: false,
            infinite: true,
            autoplay: true,
            autoplaySpeed: 1000,
            speed: 1000	
        });
	});
</script>
@endsection