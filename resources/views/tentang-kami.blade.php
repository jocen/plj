@extends('layout')

@section('content')
	
	<div class="full-banner mb0" style="background: url('{{ asset('upload/'.$tentang_kami_title->image) }}') no-repeat center bottom;">
		<div class="abs">
			<div class="container">
				<div class="t-banner">{{ $tentang_kami_title->title }}</div>
			</div>
		</div>
	</div>

	@if($menu_admin_home[1]->status == 1)
	<div class="pad150 pb0" id="sejarah">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-md-10 col-lg-9">
					<div class="title-global sr-down-td1">{{ $apakah->title }}</div>
					<div class="bdy-global sr-up-td1">
						{!! $apakah->description !!}
					</div>
				</div>
			</div>
		</div>
	</div>
	@endif

	@if($menu_admin_home[2]->status == 1)
	<div class="bg-tab" id="sejarah-visi">
		<ul class="nav nav-tabs" role="tablist">
			<?php $g=1; ?>
			@foreach($sejarah as $sej)
			<li class="nav-item" role="presentation">
				<a class="nav-link @if($g==2) active @endif" id="{{ $sej->slug }}-tab" data-bs-toggle="tab" data-bs-target="#{{ $sej->slug }}" role="tab" aria-controls="{{ $sej->slug }}" aria-selected="false">{{ $sej->title }}</a>
			</li>
			<?php $g++; ?>
			@endforeach
		</ul>
		<div class="shadow-tab">
			<div class="container">
				<div class="tab-content">
					<?php $h=1; ?>
					@foreach($sejarah as $sej)
					<div class="tab-pane fade @if($h==2) show active @endif" id="{{ $sej->slug }}" role="tabpanel" aria-labelledby="{{ $sej->slug }}-tab">
						<div class="row justify-content-center">
							@if($sej->image == null)
							<div class="col-md-10 col-lg-9 sr-down-td1">
								<div class="bdy-global">
									{!! $sej->description !!}
								</div>
							</div>
							@else
								@if($sej->type == 1)
								<div class="col-md-10">
									<div class="row">
										<div class="col-md-5">
											<div class="img"><img src="{{ asset('upload/'.$sej->image) }}"/></div>
										</div>
										<div class="col-md-7">
											<div class="bdy">
												{!! $sej->description !!}
											</div>
										</div>
									</div>
								</div>
								@else
								<div class="col-md-10">
									<div class="row">
										<div class="col-md-7">
											<div class="bdy">
												{!! $sej->description !!}
											</div>
										</div>
										<div class="col-md-5">
											<div class="img"><img src="{{ asset('upload/'.$sej->image) }}"/></div>
										</div>
									</div>
								</div>
								@endif
							@endif
						</div>
					</div>
					<?php $h++; ?>
					@endforeach
				</div>
				<div class="link sr-down-td2">
					<a href="{{ URL::to('/tentang-kami') }}#sejarah-visi">
						<button type="button" class="hvr-button">Baca lebih lanjut</button>
					</a>
				</div>
			</div>
		</div>
	</div>
	@endif

	@if($menu_admin_home[8]->status == 1)
	<div class="bg-dasar" id="dasar-pemikiran">
		<div class="container">
			<div class="row">
				<div class="col-md-5 offset-md-1 col-lg-4 offset-lg-2 my-auto sr-left-td1">
					<div class="t">
						<div class="img"><img src="{{ asset('upload/'.$dasar_pemikiran_image->image) }}"/></div>
						<div>Dasar</div>
						<div>Pemikiran</div>
					</div>
				</div>
				<div class="col-md-6 my-auto sr-right-td1">
					<div class="slider-dasar">
						@foreach($dasar_pemikiran as $dasar)
						<div class="item">
							<div class="bdy">
								{!! $dasar->description !!}
							</div>
						</div>
						@endforeach
					</div>
				</div>
			</div>
		</div>
	</div>
	@endif
	@if($menu_admin_home[9]->status == 1)
	<div class="bg-tab-tentang">
		<div class="container">
			<ul class="nav nav-tabs" role="tablist">
				<?php $i=1; ?>
				@foreach($layanan as $lay)
				<li class="nav-item" role="presentation">
					<a class="nav-link @if($i==1) active @endif" id="{{ $lay->slug }}-tab" data-bs-toggle="tab" data-bs-target="#{{ $lay->slug }}" role="tab" aria-controls="{{ $lay->slug }}" aria-selected="true">{{ $lay->title }}</a>
				</li>
				<?php $i++; ?>
				@endforeach
			</ul>
		</div>
		<div class="blue">
			<div class="container">
				<div class="tab-content">
					<?php $j=1; ?>
					@foreach($layanan as $laya)
					<div class="tab-pane fade  @if($j==1) show active @endif" id="{{ $laya->slug }}" role="tabpanel" aria-labelledby="{{ $laya->slug }}-tab">
						<div class="bdy">
							<div class="row justify-content-center">
								<div class="col-md-10">
									{!! $laya->description !!}
								</div>
							</div>
						</div>
					</div>
					<?php $j++; ?>
					@endforeach
				</div>
			</div>
		</div>
	</div>
	@endif
	@if($menu_admin_home[10]->status == 1)
	<div class="bg-pasal" id="landasan-hukum">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-md-9 col-lg-7">
					<div class="t">{{ $landasan_hukum_title->title }}</div>
					<div class="bdy">
						{!! $landasan_hukum_title->descripiton !!}
					</div>
				</div>
			</div>
			<div class="row justify-content-center">
				<div class="col-md-9 col-lg-6">
					<div class="accordion" id="accordionPasal">
						@foreach($landasan_hukum as $landasan)
						<div class="accordion-item">
							<div class="accordion-header" id="heading{{ $landasan->id }}">
								<a class="accordion-button collapsed" data-bs-toggle="collapse" data-bs-target="#collapse{{ $landasan->id }}" aria-expanded="false" aria-controls="collapse{{ $landasan->id }}"><i class="fas fa-chevron-right"></i> {{ $landasan->name }}</a>
							</div>
							<div id="collapse{{ $landasan->id }}" class="accordion-collapse collapse" aria-labelledby="heading{{ $landasan->id }}" data-bs-parent="#accordionPasal">
								<div class="accordion-body">
									{!! $landasan->description !!}
								</div>
							</div>
						</div>
						@endforeach
						
					</div>
				</div>
			</div>
		</div>
	</div>
	@endif
	@if($menu_admin_home[11]->status == 1)
	<div class="bg-klien" id="kerja-sama">
		<div class="container">
			<div class="title">{{ $kerjasama_title->title }}</div>
			<div class="row justify-content-center">
				<div class="col-md-9 col-lg-7">
					<div class="bdy">
						{!! $kerjasama_title->description !!}
					</div>
					<div class="bdr"></div>
				</div>
			</div>
			{{--
			<div class="slider-kerja">
				@foreach($kerjasama as $kerja)
				<div class="item">
					<div class="img"><img src="{{ asset('upload/'.$kerja->image) }}"/></div>
				</div>
				@endforeach
			</div>
			--}}
			<div class="slider-kerja">
				<div class="item">
					<div class="row">
						<div class="col-6 col-md-3">
							<div class="img"><img src="https://pljindonesia.org/upload/gerkatin.jpg"/></div>
						</div>
						<div class="col-6 col-md-3">
							<div class="img"><img src="https://pljindonesia.org/upload/gerkatin.jpg"/></div>
						</div>
						<div class="col-6 col-md-3">
							<div class="img"><img src="https://pljindonesia.org/upload/gerkatin.jpg"/></div>
						</div>
						<div class="col-6 col-md-3">
							<div class="img"><img src="https://pljindonesia.org/upload/gerkatin.jpg"/></div>
						</div>
						<div class="col-6 col-md-3">
							<div class="img"><img src="https://pljindonesia.org/upload/gerkatin.jpg"/></div>
						</div>
						<div class="col-6 col-md-3">
							<div class="img"><img src="https://pljindonesia.org/upload/gerkatin.jpg"/></div>
						</div>
						<div class="col-6 col-md-3">
							<div class="img"><img src="https://pljindonesia.org/upload/gerkatin.jpg"/></div>
						</div>
						<div class="col-6 col-md-3">
							<div class="img"><img src="https://pljindonesia.org/upload/gerkatin.jpg"/></div>
						</div>
					</div>
				</div>
				<div class="item">
					<div class="row">
						<div class="col-6 col-md-3">
							<div class="img"><img src="https://pljindonesia.org/upload/gerkatin.jpg"/></div>
						</div>
						<div class="col-6 col-md-3">
							<div class="img"><img src="https://pljindonesia.org/upload/gerkatin.jpg"/></div>
						</div>
						<div class="col-6 col-md-3">
							<div class="img"><img src="https://pljindonesia.org/upload/gerkatin.jpg"/></div>
						</div>
						<div class="col-6 col-md-3">
							<div class="img"><img src="https://pljindonesia.org/upload/gerkatin.jpg"/></div>
						</div>
						<div class="col-6 col-md-3">
							<div class="img"><img src="https://pljindonesia.org/upload/gerkatin.jpg"/></div>
						</div>
						<div class="col-6 col-md-3">
							<div class="img"><img src="https://pljindonesia.org/upload/gerkatin.jpg"/></div>
						</div>
						<div class="col-6 col-md-3">
							<div class="img"><img src="https://pljindonesia.org/upload/gerkatin.jpg"/></div>
						</div>
						<div class="col-6 col-md-3">
							<div class="img"><img src="https://pljindonesia.org/upload/gerkatin.jpg"/></div>
						</div>
					</div>
				</div>
				<div class="item">
					<div class="row">
						<div class="col-6 col-md-3">
							<div class="img"><img src="https://pljindonesia.org/upload/gerkatin.jpg"/></div>
						</div>
						<div class="col-6 col-md-3">
							<div class="img"><img src="https://pljindonesia.org/upload/gerkatin.jpg"/></div>
						</div>
						<div class="col-6 col-md-3">
							<div class="img"><img src="https://pljindonesia.org/upload/gerkatin.jpg"/></div>
						</div>
						<div class="col-6 col-md-3">
							<div class="img"><img src="https://pljindonesia.org/upload/gerkatin.jpg"/></div>
						</div>
						<div class="col-6 col-md-3">
							<div class="img"><img src="https://pljindonesia.org/upload/gerkatin.jpg"/></div>
						</div>
						<div class="col-6 col-md-3">
							<div class="img"><img src="https://pljindonesia.org/upload/gerkatin.jpg"/></div>
						</div>
						<div class="col-6 col-md-3">
							<div class="img"><img src="https://pljindonesia.org/upload/gerkatin.jpg"/></div>
						</div>
						<div class="col-6 col-md-3">
							<div class="img"><img src="https://pljindonesia.org/upload/gerkatin.jpg"/></div>
						</div>
					</div>
				</div>
			</div>
			<div class="slider-kerja-mobile">
				<div class="item">
					<div class="row">
						<div class="col-6 col-md-3">
							<div class="img"><img src="https://pljindonesia.org/upload/gerkatin.jpg"/></div>
						</div>
						<div class="col-6 col-md-3">
							<div class="img"><img src="https://pljindonesia.org/upload/gerkatin.jpg"/></div>
						</div>
						<div class="col-6 col-md-3">
							<div class="img"><img src="https://pljindonesia.org/upload/gerkatin.jpg"/></div>
						</div>
						<div class="col-6 col-md-3">
							<div class="img"><img src="https://pljindonesia.org/upload/gerkatin.jpg"/></div>
						</div>
					</div>
				</div>
				<div class="item">
					<div class="row">
						<div class="col-6 col-md-3">
							<div class="img"><img src="https://pljindonesia.org/upload/gerkatin.jpg"/></div>
						</div>
						<div class="col-6 col-md-3">
							<div class="img"><img src="https://pljindonesia.org/upload/gerkatin.jpg"/></div>
						</div>
						<div class="col-6 col-md-3">
							<div class="img"><img src="https://pljindonesia.org/upload/gerkatin.jpg"/></div>
						</div>
						<div class="col-6 col-md-3">
							<div class="img"><img src="https://pljindonesia.org/upload/gerkatin.jpg"/></div>
						</div>
					</div>
				</div>
				<div class="item">
					<div class="row">
						<div class="col-6 col-md-3">
							<div class="img"><img src="https://pljindonesia.org/upload/gerkatin.jpg"/></div>
						</div>
						<div class="col-6 col-md-3">
							<div class="img"><img src="https://pljindonesia.org/upload/gerkatin.jpg"/></div>
						</div>
						<div class="col-6 col-md-3">
							<div class="img"><img src="https://pljindonesia.org/upload/gerkatin.jpg"/></div>
						</div>
						<div class="col-6 col-md-3">
							<div class="img"><img src="https://pljindonesia.org/upload/gerkatin.jpg"/></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	@endif
	@if($menu_admin_home[12]->status == 1)
	<div class="bg-kepengurusan" id="kepngurusan-plj">
		<div class="container">
			<div class="title">{{ $struktur_kepengurusan_title->title }}</div>
			<div class="slider">
				@foreach($struktur_kepengurusan as $struktur)
				<div class="item">
					<div class="pos-rel">
						<div class="img"><img src="{{ asset('upload/'.$struktur->image) }}"/></div>
						<div class="abs">
							<img src="{{ asset('images/abs-kepengurusan.png') }}"/>
						</div>
					</div>
					<div class="nm">{{ $struktur->name }}</div>
					<div class="posisi">{{ $struktur->title }}</div>
				</div>
				@endforeach
			</div>
		</div>
	</div>
	@endif

	@if($menu_admin_home[17]->status == 1)
	<div class="bg-profil" id="profil-jbi">
		<div class="container">
			<div class="title">{{ $profil_title->title }} </div>
			<div class="row justify-content-center">
				<div class="col-md-9 col-lg-7">
					<div class="bdy">
						{!! $profil_title->description !!}
					</div>
				</div>
			</div>
			{{--
			<div class="slider-profil">
				@foreach($profil as $prof)
				<div class="item">
					<div class="pos-rel">
						<div class="img"><img src="{{ asset('upload/profil/'.$prof->image) }}"/></div>
						<div class="pad">
							<div class="bg"></div>
							<div class="txt">
								<div>Nama: {{ $prof->name }}</div>
								<div>Status: {{ $prof->status_name }}</div>
								<div>No. ID: {{ $prof->no_id }}</div>
							</div>
						</div>
					</div>
				</div>
				@endforeach
			</div>
			--}}

			<div class="slider-profil">
				<div class="item">
					<div class="row">
						<div class="col-profil">
							<div class="pos-rel">
								<div class="img"><img src="https://pljindonesia.org/upload/profil/rully-anjar.jpg"/></div>
								<div class="pad">
									<div class="bg"></div>
									<div class="txt">
										<div>Nama: aaaa</div>
										<div>Status: bbbb</div>
										<div>No. ID: ccccc</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-profil">
							<div class="pos-rel">
								<div class="img"><img src="https://pljindonesia.org/upload/profil/rully-anjar.jpg"/></div>
								<div class="pad">
									<div class="bg"></div>
									<div class="txt">
										<div>Nama: aaaa</div>
										<div>Status: bbbb</div>
										<div>No. ID: ccccc</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-profil">
							<div class="pos-rel">
								<div class="img"><img src="https://pljindonesia.org/upload/profil/rully-anjar.jpg"/></div>
								<div class="pad">
									<div class="bg"></div>
									<div class="txt">
										<div>Nama: aaaa</div>
										<div>Status: bbbb</div>
										<div>No. ID: ccccc</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-profil">
							<div class="pos-rel">
								<div class="img"><img src="https://pljindonesia.org/upload/profil/rully-anjar.jpg"/></div>
								<div class="pad">
									<div class="bg"></div>
									<div class="txt">
										<div>Nama: aaaa</div>
										<div>Status: bbbb</div>
										<div>No. ID: ccccc</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-profil">
							<div class="pos-rel">
								<div class="img"><img src="https://pljindonesia.org/upload/profil/rully-anjar.jpg"/></div>
								<div class="pad">
									<div class="bg"></div>
									<div class="txt">
										<div>Nama: aaaa</div>
										<div>Status: bbbb</div>
										<div>No. ID: ccccc</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-profil">
							<div class="pos-rel">
								<div class="img"><img src="https://pljindonesia.org/upload/profil/rully-anjar.jpg"/></div>
								<div class="pad">
									<div class="bg"></div>
									<div class="txt">
										<div>Nama: aaaa</div>
										<div>Status: bbbb</div>
										<div>No. ID: ccccc</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-profil">
							<div class="pos-rel">
								<div class="img"><img src="https://pljindonesia.org/upload/profil/rully-anjar.jpg"/></div>
								<div class="pad">
									<div class="bg"></div>
									<div class="txt">
										<div>Nama: aaaa</div>
										<div>Status: bbbb</div>
										<div>No. ID: ccccc</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-profil">
							<div class="pos-rel">
								<div class="img"><img src="https://pljindonesia.org/upload/profil/rully-anjar.jpg"/></div>
								<div class="pad">
									<div class="bg"></div>
									<div class="txt">
										<div>Nama: aaaa</div>
										<div>Status: bbbb</div>
										<div>No. ID: ccccc</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-profil">
							<div class="pos-rel">
								<div class="img"><img src="https://pljindonesia.org/upload/profil/rully-anjar.jpg"/></div>
								<div class="pad">
									<div class="bg"></div>
									<div class="txt">
										<div>Nama: aaaa</div>
										<div>Status: bbbb</div>
										<div>No. ID: ccccc</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-profil">
							<div class="pos-rel">
								<div class="img"><img src="https://pljindonesia.org/upload/profil/rully-anjar.jpg"/></div>
								<div class="pad">
									<div class="bg"></div>
									<div class="txt">
										<div>Nama: aaaa</div>
										<div>Status: bbbb</div>
										<div>No. ID: ccccc</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="item">
					<div class="row">
						<div class="col-profil">
							<div class="pos-rel">
								<div class="img"><img src="https://pljindonesia.org/upload/profil/rully-anjar.jpg"/></div>
								<div class="pad">
									<div class="bg"></div>
									<div class="txt">
										<div>Nama: aaaa</div>
										<div>Status: bbbb</div>
										<div>No. ID: ccccc</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-profil">
							<div class="pos-rel">
								<div class="img"><img src="https://pljindonesia.org/upload/profil/rully-anjar.jpg"/></div>
								<div class="pad">
									<div class="bg"></div>
									<div class="txt">
										<div>Nama: aaaa</div>
										<div>Status: bbbb</div>
										<div>No. ID: ccccc</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-profil">
							<div class="pos-rel">
								<div class="img"><img src="https://pljindonesia.org/upload/profil/rully-anjar.jpg"/></div>
								<div class="pad">
									<div class="bg"></div>
									<div class="txt">
										<div>Nama: aaaa</div>
										<div>Status: bbbb</div>
										<div>No. ID: ccccc</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-profil">
							<div class="pos-rel">
								<div class="img"><img src="https://pljindonesia.org/upload/profil/rully-anjar.jpg"/></div>
								<div class="pad">
									<div class="bg"></div>
									<div class="txt">
										<div>Nama: aaaa</div>
										<div>Status: bbbb</div>
										<div>No. ID: ccccc</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-profil">
							<div class="pos-rel">
								<div class="img"><img src="https://pljindonesia.org/upload/profil/rully-anjar.jpg"/></div>
								<div class="pad">
									<div class="bg"></div>
									<div class="txt">
										<div>Nama: aaaa</div>
										<div>Status: bbbb</div>
										<div>No. ID: ccccc</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-profil">
							<div class="pos-rel">
								<div class="img"><img src="https://pljindonesia.org/upload/profil/rully-anjar.jpg"/></div>
								<div class="pad">
									<div class="bg"></div>
									<div class="txt">
										<div>Nama: aaaa</div>
										<div>Status: bbbb</div>
										<div>No. ID: ccccc</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-profil">
							<div class="pos-rel">
								<div class="img"><img src="https://pljindonesia.org/upload/profil/rully-anjar.jpg"/></div>
								<div class="pad">
									<div class="bg"></div>
									<div class="txt">
										<div>Nama: aaaa</div>
										<div>Status: bbbb</div>
										<div>No. ID: ccccc</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-profil">
							<div class="pos-rel">
								<div class="img"><img src="https://pljindonesia.org/upload/profil/rully-anjar.jpg"/></div>
								<div class="pad">
									<div class="bg"></div>
									<div class="txt">
										<div>Nama: aaaa</div>
										<div>Status: bbbb</div>
										<div>No. ID: ccccc</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-profil">
							<div class="pos-rel">
								<div class="img"><img src="https://pljindonesia.org/upload/profil/rully-anjar.jpg"/></div>
								<div class="pad">
									<div class="bg"></div>
									<div class="txt">
										<div>Nama: aaaa</div>
										<div>Status: bbbb</div>
										<div>No. ID: ccccc</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-profil">
							<div class="pos-rel">
								<div class="img"><img src="https://pljindonesia.org/upload/profil/rully-anjar.jpg"/></div>
								<div class="pad">
									<div class="bg"></div>
									<div class="txt">
										<div>Nama: aaaa</div>
										<div>Status: bbbb</div>
										<div>No. ID: ccccc</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="slider-profil-mobile">
				<div class="item">
					<div class="row">
						<div class="col-profil">
							<div class="pos-rel">
								<div class="img"><img src="https://pljindonesia.org/upload/profil/rully-anjar.jpg"/></div>
								<div class="pad">
									<div class="bg"></div>
									<div class="txt">
										<div>Nama: aaaa</div>
										<div>Status: bbbb</div>
										<div>No. ID: ccccc</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-profil">
							<div class="pos-rel">
								<div class="img"><img src="https://pljindonesia.org/upload/profil/rully-anjar.jpg"/></div>
								<div class="pad">
									<div class="bg"></div>
									<div class="txt">
										<div>Nama: aaaa</div>
										<div>Status: bbbb</div>
										<div>No. ID: ccccc</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-profil">
							<div class="pos-rel">
								<div class="img"><img src="https://pljindonesia.org/upload/profil/rully-anjar.jpg"/></div>
								<div class="pad">
									<div class="bg"></div>
									<div class="txt">
										<div>Nama: aaaa</div>
										<div>Status: bbbb</div>
										<div>No. ID: ccccc</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-profil">
							<div class="pos-rel">
								<div class="img"><img src="https://pljindonesia.org/upload/profil/rully-anjar.jpg"/></div>
								<div class="pad">
									<div class="bg"></div>
									<div class="txt">
										<div>Nama: aaaa</div>
										<div>Status: bbbb</div>
										<div>No. ID: ccccc</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="item">
					<div class="row">
						<div class="col-profil">
							<div class="pos-rel">
								<div class="img"><img src="https://pljindonesia.org/upload/profil/rully-anjar.jpg"/></div>
								<div class="pad">
									<div class="bg"></div>
									<div class="txt">
										<div>Nama: aaaa</div>
										<div>Status: bbbb</div>
										<div>No. ID: ccccc</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-profil">
							<div class="pos-rel">
								<div class="img"><img src="https://pljindonesia.org/upload/profil/rully-anjar.jpg"/></div>
								<div class="pad">
									<div class="bg"></div>
									<div class="txt">
										<div>Nama: aaaa</div>
										<div>Status: bbbb</div>
										<div>No. ID: ccccc</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-profil">
							<div class="pos-rel">
								<div class="img"><img src="https://pljindonesia.org/upload/profil/rully-anjar.jpg"/></div>
								<div class="pad">
									<div class="bg"></div>
									<div class="txt">
										<div>Nama: aaaa</div>
										<div>Status: bbbb</div>
										<div>No. ID: ccccc</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-profil">
							<div class="pos-rel">
								<div class="img"><img src="https://pljindonesia.org/upload/profil/rully-anjar.jpg"/></div>
								<div class="pad">
									<div class="bg"></div>
									<div class="txt">
										<div>Nama: aaaa</div>
										<div>Status: bbbb</div>
										<div>No. ID: ccccc</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	@endif

	<div class="bg-kontak" style="background: url('{{ asset('images/bg-kontak.jpg') }}') no-repeat center;">
		<div class="container">
			<div class="t">
				<div>Juru Bahasa Isyarat Kami</div>
				<div>Siap Bantu Anda Kapan Saja!</div>
			</div>
			<div class="link">
				<a href="#">
					<button type="button" class="hvr-button">Kontak Kami</button>
				</a>
			</div>
		</div>
	</div>

@endsection

@section('js')
<script type="text/javascript">
	$(document).ready(function() {
	    $('header').addClass('white');

	    $('.slider-dasar').slick({
	        slidesToShow: 1,
	        slidesToScroll: 1,
	        dots: true,
	        centerMode: false,
	        arrows: false,
	        infinite: true,
	        autoplay: false,
	        autoplaySpeed: 7000,
	        speed: 1000,
	    });

	    $('.slider').slick({
			slidesToShow: 5,
			slidesToScroll: 1,
			dots: false,
			arrows: false,
			infinite: true,
			autoplay: true,
			autoplaySpeed: 500,
	        speed: 1500,
	        centerMode: true,
	        centerPadding: '0',
	        focusOnSelect: true,
	        swipe: false,
			responsive: [
			{
				breakpoint: 767,
				settings: {
					centerPadding: '70px',
					slidesToShow: 1,
				}
			}]
		});

		$('.slider-profil').slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			dots: true,
			arrows: true,
			infinite: true,
			autoplay: true,
			autoplaySpeed: 500,
	        speed: 1500,
	        centerMode: true,
	        centerPadding: '0',
	        focusOnSelect: true,
	        swipe: false,
		});

		$('.slider-profil-mobile').slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			dots: true,
			arrows: true,
			infinite: true,
			autoplay: true,
			autoplaySpeed: 500,
	        speed: 1500,
	        centerMode: true,
	        centerPadding: '0',
	        focusOnSelect: true,
	        swipe: false,
		});

	    $(".collapse").on('show.bs.collapse', function(){
	        $(this).prev('.accordion-header').find(".fas").removeClass("fa-chevron-right").addClass("fa-chevron-down");
	    }).on('hide.bs.collapse', function(){
	        $(this).prev('.accordion-header').find(".fas").removeClass("fa-chevron-down").addClass("fa-chevron-right");
	    });

	    $('.slider-kerja').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            dots: false,
            centerMode: false,
            arrows: false,
            infinite: true,
            autoplay: true,
            autoplaySpeed: 1000,
            speed: 1000
        });

        $('.slider-kerja-mobile').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            dots: false,
            centerMode: false,
            arrows: false,
            infinite: true,
            autoplay: true,
            autoplaySpeed: 1000,
            speed: 1000
        });
	});
</script>
@endsection