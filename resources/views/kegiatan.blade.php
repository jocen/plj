@extends('layout')

@section('content')

	<div class="full-banner" style="background: url('{{ asset('upload/'.$kegiatan_title->image) }}') no-repeat center bottom;">
		<div class="abs">
			<div class="container">
				<div class="t-banner">{!! $kegiatan_title->title !!}</div>
			</div>
		</div>
	</div>
	@if($menu_admin_home[15]->status == 1)
	<div class="css-kegiatan">
		<div class="container">
			<div id="mitra">
				<div class="row justify-content-center">
					<div class="col-md-10 col-lg-9">
						<div class="title">{!! $mitra_title->title !!}</div>
					</div>
				</div>
				<div class="row justify-content-center">
					<div class="col-md-8 col-lg-7">
						<div class="bdy">
							{!! $mitra_title->description !!}
						</div>
					</div>
				</div>
				<div class="slider-video">

					<!-- <div class="item">
						<iframe src="https://www.youtube.com/embed/JHNCzrxVn4A" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
					</div> -->
					@foreach($mitra as $mit)
					<div class="item">
						<iframe width="100%" height="315" src="{{ $mit->video }}" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
					</div>
					@endforeach
				</div>
				<div class="t-video">Nonton lebih lanjut youtube</div>
				<ul class="l-video">
					@if($mitra_title->youtube_link1)
					<li>
						<div class="link">
							<a href="{{ $mitra_title->youtube_link1 }}" target="_blank">
								<button type="button" class="hvr-button">{{ $mitra_title->youtube_link_name1 }}</button>
							</a>
						</div>
					</li>
					@endif
					@if($mitra_title->youtube_link2)
					<li>
						<div class="link">
							<a href="{{ $mitra_title->youtube_link2 }}" target="_blank">
								<button type="button" class="hvr-button">{{ $mitra_title->youtube_link_name2 }}</button>
							</a>
						</div>
					</li>
					@endif
				</ul>
			</div>
			<div id="jadwal-jbi">
				<div class="title">{{ $jadwal->title }}</div>
				<div class="row justify-content-center">
					<div class="col-md-8 col-lg-7">
						<div class="bdy">
							{!! $jadwal->description !!}
						</div>
					</div>
				</div>
				<div class="img"><img src="{{ asset('upload/'.$jadwal->image) }}"/></div>
			</div>
		</div>
	</div>
	@endif
	@if($menu_admin_home[6]->status == 1)
	<div class="bg-gallery pb150" id="gallery">
		<div class="container">
			<div class="title">{{ $galeri_title->title }}</div>
			<div class="row">
				@foreach($galeri as $gal)
				<div class="col-md-4">
					<div class="item">
						<a href="{{ URL::to('/kegiatan') }}">
							<div class="img">
								<img src="{{ asset('upload/galeri/'.$gal->image) }}"/>
							</div>
							<div class="abs">
								<div class="bg"></div>
								<div class="t">{{ $gal->title }}</div>
								<div class="bdy">
									{!! $gal->description !!}
								</div>
							</div>
						</a>
					</div>
				</div>
				@endforeach
			</div>
			@if ($galeri->lastPage() > 1)
			<ul class="pagination">
				@if($galeri->currentPage() != 1)
				<li class="page-item">
					<a class="page-link {{ ($galeri->currentPage() == 1) ? ' disabled' : '' }}" href="{{ $galeri->url($galeri->currentPage()-1) }}" aria-label="Previous">
						<span aria-hidden="true">&laquo;</span>
						<span class="sr-only">Previous</span>
					</a>
				</li>
				@endif
				@for ($i = 1; $i <= $galeri->lastPage(); $i++)
					<li class="page-item {{ ($galeri->currentPage() == $i) ? ' active' : '' }}"><a class="page-link" href="{{ $galeri->url($i) }}">{{ $i }}</a></li>
				@endfor
				@if($galeri->currentPage() != $galeri->lastPage())
					<li class="page-item {{ ($galeri->currentPage() == $galeri->lastPage()) ? ' disabled' : '' }}">
						<a class="page-link" href="{{ $galeri->url($galeri->currentPage()+1) }}" aria-label="Next">
							<span aria-hidden="true">&raquo;</span>
							<span class="sr-only">Next</span>
						</a>
					</li>
				@endif
			</ul>
			@endif
		</div>
	</div>
	@endif

	<div class="bg-kontak" style="background: url('{{ asset('images/bg-kontak.jpg') }}') no-repeat center;">
		<div class="container">
			<div class="t">
				<div>Juru Bahasa Isyarat Kami</div>
				<div>Siap Bantu Anda Kapan Saja!</div>
			</div>
			<div class="link">
				<a href="#">
					<button type="button" class="hvr-button">Kontak Kami</button>
				</a>
			</div>
		</div>
	</div>

@endsection

@section('js')
<script type="text/javascript">
	$(document).ready(function() {
	    $('header').addClass('white');

	    $('.slider-video').slick({
			slidesToShow: 3,
			slidesToScroll: 1,
            dots: false,
            centerMode: false,
            arrows: false,
            infinite: true,
            autoplay: true,
            autoplaySpeed: 1000,
            speed: 1000,
            responsive: [
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 3,
                }
            },
            {
                breakpoint: 580,
                settings: {
                    slidesToShow: 1,
                }
            }
            ]
		});
	});
</script>
@endsection