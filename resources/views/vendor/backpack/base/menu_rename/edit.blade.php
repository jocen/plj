@extends('vendor.backpack.base.layout')

@section('header')
    <section class="content-header">
      <h1>
        Rename Menu
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ url(config('backpack.base.route_prefix', 'admin')) }}">{{ config('backpack.base.project_name') }}</a></li>
        <li class="active">Rename Menu</li>
      </ol>
    </section>
@endsection


@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box box-default">
                <div class="box-header with-border">
                    Rename Menu
                </div>

                <div class="box-body">
                    <form role="form" method="POST" action="{{ url(config('backpack.base.route_prefix').'/menu_rename/update') }}" enctype="multipart/form-data">
                      {!! csrf_field() !!}
                      <div class="form-group @if($errors->has('menu')) has-error @endif">
                        <label for="exampleInputEmail1">Menu</label>
                        <input type="text" name="menu" value="{{ $data->menu }}" class="form-control">
                        @if($errors->has('menu')) <span class="help-block">{{ $errors->first('menu') }}</span>  @endif
                      </div>
                    

                      <input type="hidden" name="id" value="{{ $data->id }}">
                      <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
