
    <!-- Left side column. contains the sidebar -->
    <aside class="main-sidebar" style="background-color: {{ $color->sidebar }}">
      <!-- sidebar: style can be found in sidebar.less -->
      <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
          <div class="pull-left image">
          <br>
          </div>
          <div class="pull-left info">
            <p>Super Admin</p>
            <!-- <a href="#"><i class="fa fa-circle text-success"></i> Online</a> -->
          </div>
        </div>
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu tree" data-widget="tree">
          <li class="header"></li>


          <li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/dashboard') }}"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
          <li class="treeview">
                <a href="#">
                  <i class="fa fa-home"></i>
                  <span>Beranda</span>
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
                </a>
                <ul class="treeview-menu" style="background: {{ $color->sidebar }}">
                  
                  <li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/banner') }}"><i class="fa fa-image"></i> <span>{{ $menu_admin[0]->menu }}</span></a></li>
                  <li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/apakah') }}"><i class="fa fa-question-circle"></i> <span>{{ $menu_admin[1]->menu }}</span></a></li>
                  <li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/sejarah') }}"><i class="fa fa-question-circle"></i> <span>{{ $menu_admin[2]->menu }}</span></a></li>

                  <li class="treeview">
                    <a href="#">
                      <i class="fa fa-question-circle"></i>
                      <span>{{ $menu_admin[3]->menu }}</span>
                      <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                      </span>
                    </a>
                    <ul class="treeview-menu" style="background: {{ $color->sidebar }}">
                        <li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/keunggulan') }}"><i class="fa fa-question-circle"></i> <span>Paragraf</span></a></li>
                        <li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/keunggulan_kami') }}"><i class="fa fa-question-circle"></i> <span>Deskripsi</span></a></li>
                    </ul>
                  </li>

                  <li class="treeview">
                    <a href="#">
                      <i class="fa fa-map"></i>
                      <span>{{ $menu_admin[4]->menu }}</span>
                      <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                      </span>
                    </a>
                    <ul class="treeview-menu" style="background: {{ $color->sidebar }}">
                        <li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/butuhkan_title') }}"><i class="fa fa-header"></i> <span>Judul</span></a></li>
                        <li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/butuhkan') }}"><i class="fa fa-map"></i> <span>Deskripsi</span></a></li>
                    </ul>
                  </li>

                  <li class="treeview">
                    <a href="#">
                      <i class="fa fa-building"></i>
                      <span>{{ $menu_admin[5]->menu }}</span>
                      <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                      </span>
                    </a>
                    <ul class="treeview-menu" style="background: {{ $color->sidebar }}">
                        <li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/klien_title') }}"><i class="fa fa-header"></i> <span>Judul</span></a></li>
                        <li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/klien') }}"><i class="fa fa-building"></i> <span>Logo</span></a></li>
                    </ul>
                  </li>
                  <li class="treeview">
                    <a href="#">
                      <i class="fa fa-image"></i>
                      <span>{{ $menu_admin[6]->menu }}</span>
                      <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                      </span>
                    </a>
                    <ul class="treeview-menu" style="background: {{ $color->sidebar }}">
                        <li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/galeri_title') }}"><i class="fa fa-header"></i> <span>Judul</span></a></li>
                        <li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/galeri') }}"><i class="fa fa-image"></i> <span>Galeri</span></a></li>
                      
                    </ul>
                  </li>
                  <li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/kontak_kami') }}"><i class="fa fa-phone"></i> <span>{{ $menu_admin[7]->menu }}</span></a></li>
                  

                  

                  
                  
                  
                  
                <!--   <li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/pertanyaan') }}"><i class="fa fa-question"></i> <span>{{ $menu_admin[5]->menu }}</span></a></li>
                  <li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/latar_belakang/1') }}"><i class="fa fa-bank"></i> <span>{{ $menu_admin[6]->menu }}</span></a></li> -->

                  
                </ul>
              </li>

            <li class="treeview">
                <a href="#">
                  <i class="fa fa-building-o"></i>
                  <span>Tentang Kami</span>
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
                </a>
                <ul class="treeview-menu" style="background: {{ $color->sidebar }}">
                    <li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/tentang_kami_title') }}"><i class="fa fa-question-circle"></i> <span>Header</span></a></li>
                    <li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/dasar_pemikiran') }}"><i class="fa fa-question-circle"></i> <span>{{ $menu_admin[8]->menu }}</span></a></li>
                    <li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/layanan') }}"><i class="fa fa-question-circle"></i> <span>{{ $menu_admin[9]->menu }}</span></a></li>

                    <li class="treeview">
                      <a href="#">
                        <i class="fa fa-map"></i>
                        <span>{{ $menu_admin[10]->menu }}</span>
                        <span class="pull-right-container">
                          <i class="fa fa-angle-left pull-right"></i>
                        </span>
                      </a>
                      <ul class="treeview-menu" style="background: {{ $color->sidebar }}">
                          <li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/landasan_hukum_title') }}"><i class="fa fa-header"></i> <span>Judul</span></a></li>
                          <li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/landasan_hukum') }}"><i class="fa fa-map"></i> <span>{{ $menu_admin[10]->menu }}</span></a></li>
                      </ul>
                    </li>

                    <li class="treeview">
                      <a href="#">
                        <i class="fa fa-building"></i>
                        <span>{{ $menu_admin[11]->menu }}</span>
                        <span class="pull-right-container">
                          <i class="fa fa-angle-left pull-right"></i>
                        </span>
                      </a>
                      <ul class="treeview-menu" style="background: {{ $color->sidebar }}">
                          <li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/kerjasama_title') }}"><i class="fa fa-header"></i> <span>Judul</span></a></li>
                          <li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/kerjasama') }}"><i class="fa fa-building"></i> <span>Logo</span></a></li>
                      </ul>
                    </li>

                    <li class="treeview">
                      <a href="#">
                        <i class="fa fa-sitemap"></i>
                        <span>{{ $menu_admin[12]->menu }}</span>
                        <span class="pull-right-container">
                          <i class="fa fa-angle-left pull-right"></i>
                        </span>
                      </a>
                      <ul class="treeview-menu" style="background: {{ $color->sidebar }}">
                          <li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/struktur_kepengurusan_title') }}"><i class="fa fa-header"></i> <span>Judul</span></a></li>
                          <li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/struktur_kepengurusan') }}"><i class="fa fa-sitemap"></i> <span>Daftar</span></a></li>
                      </ul>
                    </li>

                    <li class="treeview">
                      <a href="#">
                        <i class="fa fa-users"></i>
                        <span>{{ $menu_admin[17]->menu }}</span>
                        <span class="pull-right-container">
                          <i class="fa fa-angle-left pull-right"></i>
                        </span>
                      </a>
                      <ul class="treeview-menu" style="background: {{ $color->sidebar }}">
                          <li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/profil_title') }}"><i class="fa fa-header"></i> <span>Judul</span></a></li>
                          <li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/profil') }}"><i class="fa fa-users"></i> <span>Daftar</span></a></li>
                      </ul>
                    </li>
                    
                    <!-- <li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/latar_belakang/2') }}"><i class="fa fa-building"></i> <span>{{ $menu_admin[8]->menu }}</span></a></li> -->

                   <!--  <li class="treeview">
                      <a href="#">
                        <i class="fa fa-calendar"></i>
                        <span>{{ $menu_admin[9]->menu }}</span>
                        <span class="pull-right-container">
                          <i class="fa fa-angle-left pull-right"></i>
                        </span>
                      </a>
                      <ul class="treeview-menu" style="background: {{ $color->sidebar }}">
                          <li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/timeline_title') }}"><i class="fa fa-header"></i> <span>Judul & Paragraf</span></a></li>
                          <li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/timeline') }}"><i class="fa fa-calendar"></i> <span>Deskripsi</span></a></li>
                      </ul>
                    </li>
                   
                    <li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/video') }}"><i class="fa fa-video-camera"></i> <span>{{ $menu_admin[10]->menu }}</span></a></li>

                    

                    <li class="treeview">
                      <a href="#">
                        <i class="fa fa-users"></i>
                        <span>{{ $menu_admin[12]->menu }}</span>
                        <span class="pull-right-container">
                          <i class="fa fa-angle-left pull-right"></i>
                        </span>
                      </a>
                      <ul class="treeview-menu" style="background: {{ $color->sidebar }}">
                          <li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/pengurus_title') }}"><i class="fa fa-header"></i> <span>Judul</span></a></li>
                          <li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/pengurus') }}"><i class="fa fa-users"></i> <span>Pengurus</span></a></li>
                      </ul>
                    </li>
                     -->
                    
                  
                </ul>
              </li>

             <!--   <li class="treeview">
                <a href="#">
                  <i class="fa fa-book"></i>
                  <span>{{ $menu_admin[13]->menu }}</span>
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
                </a>
                <ul class="treeview-menu" style="background: {{ $color->sidebar }}">
                  <li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/artikel_kategori') }}"><i class="fa fa-bars"></i> <span>Kategori</span></a></li>
                  <li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/artikel') }}"><i class="fa fa-book"></i> <span>Artikel</span></a></li>
                  
                </ul>
              </li> -->

              <li class="treeview">
                <a href="#">
                  <i class="fa fa-question-circle"></i>
                  <span>{{ $menu_admin[13]->menu }}</span>
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
                </a>
                <ul class="treeview-menu" style="background: {{ $color->sidebar }}">
                  <li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/informasi_title') }}"><i class="fa fa-image"></i> <span>Header</span></a></li>
                  <li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/informasi_category') }}"><i class="fa fa-bars"></i> <span>Kategori</span></a></li>
                  <li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/informasi') }}"><i class="fa fa-question-circle"></i> <span>Informasi</span></a></li>
                  
                </ul>
              </li>

              <li class="treeview">
                <a href="#">
                  <i class="fa fa-question-circle"></i>
                  <span>{{ $menu_admin[14]->menu }}</span>
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
                </a>
                <ul class="treeview-menu" style="background: {{ $color->sidebar }}">
                  <li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/kegiatan_title') }}"><i class="fa fa-image"></i> <span>Header</span></a></li>
                  <li class="treeview">
                      <a href="#">
                        <i class="fa fa-video-camera"></i>
                        <span>{{ $menu_admin[15]->menu }}</span>
                        <span class="pull-right-container">
                          <i class="fa fa-angle-left pull-right"></i>
                        </span>
                      </a>
                      <ul class="treeview-menu" style="background: {{ $color->sidebar }}">
                          <li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/mitra_title') }}"><i class="fa fa-header"></i> <span>Judul & Paragraf</span></a></li>
                          <li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/mitra') }}"><i class="fa fa-video-camera"></i> <span>Video</span></a></li>
                      </ul>
                  </li>

                  <li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/jadwal') }}"><i class="fa fa-calendar"></i> <span>{{ $menu_admin[16]->menu }}</span></a></li>
                  
                </ul>
              </li>

        <!--   <li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/tentang_kami') }}"><i class="fa fa-info-circle"></i> <span>Tentang Kami</span></a></li>
          
          <li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/testimonial') }}"><i class="fa fa-comments"></i> <span>Testimonial</span></a></li> -->
          <li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/social_media') }}"><i class="fa fa-instagram"></i> <span>Sosial Media</span></a></li>
          
          <li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/hubungi_kami') }}"><i class="fa fa-headphones"></i> <span>Kontak Kami</span></a></li>
          

          
          
         
             
          
         
              <li class="treeview">
                <a href="#">
                  <i class="fa fa-gear"></i>
                  <span>Pengaturan</span>
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
                </a>
                <ul class="treeview-menu" style="background: {{ $color->sidebar }}">
                  <li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/metadata') }}"><i class="fa fa-pencil"></i> <span>Metadata</span></a></li>
                  <li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/change-password') }}"><i class="fa fa-lock"></i> <span>Ganti Password</span></a></li>
                  <li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/admin_color') }}"><i class="fa fa-paint-brush"></i> <span>Ganti Tema</span></a></li>
                  <li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/menu_rename') }}"><i class="fa fa-edit"></i> <span>Menu</span></a></li>
                </ul>
              </li>

          <!-- ======================================= -->
          <!-- <li class="header">{{ trans('backpack::base.user') }}</li>
          <li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/logout') }}"><i class="fa fa-sign-out"></i> <span>{{ trans('backpack::base.logout') }}</span></a></li> -->
        </ul>
      </section>
      <!-- /.sidebar -->
    </aside>
