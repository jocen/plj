@extends('vendor.backpack.base.layout')

@section('header')
    <section class="content-header">
      <h1>
        {{ $menu_admin[13]->menu }}
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ url(config('backpack.base.route_prefix', 'admin')) }}">{{ config('backpack.base.project_name') }}</a></li>
        <li class="active">{{ $menu_admin[13]->menu }}</li>
      </ol>
    </section>
@endsection


@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box box-default">
                <div class="box-header with-border">
                    Update {{ $menu_admin[13]->menu }}
                </div>

                <div class="box-body">
                    <form role="form" method="POST" action="{{ url(config('backpack.base.route_prefix').'/artikel/update') }}" enctype="multipart/form-data">
                      {!! csrf_field() !!}
                      <div class="form-group @if($errors->has('title')) has-error @endif">
                        <label for="exampleInputEmail1">Title</label>
                        <input type="text" name="title" value="{{ $data->title }}" class="form-control">
                        @if($errors->has('title')) <span class="help-block">{{ $errors->first('title') }}</span>  @endif
                      </div>
                      <div class="form-group @if($errors->has('image')) has-error @endif">
                        <label for="exampleInputEmail1">Image</label>
                        <input type="file" name="image" class="form-control">
                        <img src="{{ asset('/upload/artikel/'.$data->image) }}" width="40%" />
                        @if($errors->has('image')) <span class="help-block">{{ $errors->first('image') }}</span>  @endif
                        <input type="hidden" name="old_image" value="{{ $data->image }}">
                      </div>
                      @if($data->featured == 1)
                        <div class="form-group">
                          <label for="exampleInputEmail1">Banner</label><br> 
                          @if($data->banner)
                          <img src="{{ asset('/upload/artikel/'.$data->banner) }}" width="20%" >
                          @endif
                          <input type="file" name="banner" class="form-control">
                          <input type="hidden" name="old_banner" value="{{ $data->banner }}">
                        </div>
                        @endif
                      <div class="form-group">
                          <label for="exampleInputEmail1">Date</label>
                          <div class="input-group date">
                            <div class="input-group-addon">
                              <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" readonly="readonly" autocomplete="off" value="{{ $data->date }}" required="required" name="date" class="form-control pull-right datepicker">
                          </div>
                          
                        </div>

                      <div class="form-group">
                                <label for="exampleInputEmail1">Kategori</label>
                                <select class="form-control select2" multiple="multiple" name="kategori[]" id="kategori" data-placeholder="Pilih Kategori" style="width: 100%;">
                                  <?php
                                    for ($i = 0; $i < sizeof($kategori); $i++) {
                                      $inputted = false;
                                      for ($j = 0; $j < sizeof($data->kategori); $j++) {
                                        if ($data->kategori[$j]->id == $kategori[$i]->id) {
                                          echo '<option value="' .$kategori[$i]->id . '" selected="selected">'. $kategori[$i]->name.'</option>';
                                          $inputted = true;
                                          break;
                                        }
                                      }
                                      
                                      if (!$inputted) {
                                        echo '<option value="' .$kategori[$i]->id . '">'. $kategori[$i]->name.'</option>';
                                      }
                                    }
                                  ?>
                                </select>
                              </div>

                      <div class="form-group">
                          <label for="exampleInputEmail1">Description</label>
                          <textarea name="description" class="form-control my-editor">{{ $data->description }}</textarea>
                      </div>
                      <div class="form-group">
                        <label for="exampleInputEmail1">Sumber</label>
                        <input type="text" name="sumber" value="{{ $data->sumber }}" class="form-control">
                      </div>
                    <!--   <div class="form-group">
                        <label for="exampleInputEmail1">Link</label>
                        <input type="text" name="link" value="{{ $data->link }}" class="form-control">
                      </div> -->
                    

                      <input type="hidden" name="id" value="{{ $data->id }}">
                      <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
