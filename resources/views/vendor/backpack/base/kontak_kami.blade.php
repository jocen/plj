@extends('vendor.backpack.base.layout')

@section('header')
    <section class="content-header">
      <h1>
        {{ $menu_admin[7]->menu }}
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ url(config('backpack.base.route_prefix', 'admin')) }}">{{ config('backpack.base.project_name') }}</a></li>
        <li class="active">{{ $menu_admin[7]->menu }}</li>
      </ol>
    </section>
@endsection


@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box box-default">
                <div class="box-header with-border">
                    Update {{ $menu_admin[7]->menu }}
                </div>

                <div class="box-body">
                    <form role="form" method="POST" action="{{ url(config('backpack.base.route_prefix').'/kontak_kami/update') }}" enctype="multipart/form-data">
                      {!! csrf_field() !!}
                      <div class="form-group">
                        <label for="exampleInputEmail1">Title</label>
                        <input type="text" name="title" class="form-control" value="@if(isset($data->title)) {{ $data->title }} @endif">
                      </div>
                      <div class="form-group">
                        <label for="exampleInputEmail1">Description</label>
                        <textarea name="description" class="form-control my-editor">@if(isset($data->description)) {{ $data->description }} @endif</textarea>
                      </div>
                      <div class="form-group">
                        <label for="exampleInputEmail1">Catatan</label>
                        <input type="text" name="catatan" class="form-control" value="@if(isset($data->catatan)) {{ $data->catatan }} @endif">
                      </div>
                       <div class="form-group @if($errors->has('image')) has-error @endif">
                        <label for="exampleInputEmail1">Image</label>
                        <input type="file" name="image" class="form-control">
                        @if(isset($data->image)) <img src="{{ asset('/upload/'.$data->image) }}" width="10%" /> @endif
                        <input type="hidden" name="old_image" value="@if(isset($data->image)) {{ $data->image }} @endif">
                      </div>
                      <div class="form-group">
                        <label for="exampleInputEmail1">Email</label>
                        <input type="text" name="email" class="form-control" value="@if(isset($data->email)){{ $data->email }}@endif">
                      </div>
                      <div class="form-group">
                        <label for="exampleInputEmail1">Whatsapp</label>
                        <input type="text" name="whatsapp" class="form-control" value="@if(isset($data->whatsapp)){{ $data->whatsapp }}@endif">
                      </div>
                      <div class="form-group">
                        <label for="exampleInputEmail1">Fax</label>
                        <input type="text" name="fax" class="form-control" value="@if(isset($data->fax)){{ $data->fax }}@endif">
                      </div>
                      <div class="form-group">
                        <label for="exampleInputEmail1">Website</label>
                        <input type="text" name="website" class="form-control" value="@if(isset($data->website)){{ $data->website }}@endif">
                      </div>
                      <div class="form-group">
                        <label for="exampleInputEmail1">Alamat Surat</label>
                        <textarea name="address" class="form-control my-editor">@if(isset($data->address)) {{ $data->address }} @endif</textarea>
                      </div>  
                      <div class="form-group">
                        <label for="exampleInputEmail1">Donasi</label>
                        <input type="text" name="donasi" class="form-control" value="@if(isset($data->donasi)){{ $data->donasi }}@endif">
                      </div>

                      <!-- <div class="form-group">
                        <label for="exampleInputEmail1">Telephone</label>
                        <textarea name="telephone" class="form-control my-editor">@if(isset($data->telephone)) {{ $data->telephone }} @endif</textarea>
                      </div> -->
<!-- 
                      <div class="form-group">
                        <label for="exampleInputEmail1">Jam Kerja</label>
                        <textarea name="hours" class="form-control my-editor">@if(isset($data->hours)) {{ $data->hours }} @endif</textarea>
                      </div> -->
                      <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
