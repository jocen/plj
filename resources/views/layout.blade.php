<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=2.0"/>
    @if(isset($metadata->description))
    <meta name="description" content="{{ $metadata->description }}">
    @else
    <meta name="description" content="PLJ (Pusat Layanan Juru Bahasa Isyarat) adalah lembaga yang mengelola juru bahasa isyarat di Indonesia yang dikelola oleh tuli komunitas pengguna bahasa isyarat.">
    @endif
    
    @if(isset($metadata->keyword))
    <meta name="keywords" content="{{ $metadata->keyword }}">
    @else
    <meta name="keywords" content="PLJ (Pusat Layanan Juru Bahasa Isyarat) adalah lembaga yang mengelola juru bahasa isyarat di Indonesia yang dikelola oleh tuli komunitas pengguna bahasa isyarat.">
    @endif
    
    <title>@if(isset($metadata->title)) {{ $metadata->title }} @else PLJ @endif | Pusat Layanan Juru Bahasa Isyarat</title>
    <!--favicon-->
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('images/apple-icon-180x180.png?v.1') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('images/favicon-32x32.png?v.1') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('images/favicon-16x16.png?v.1') }}">
    
    <!-- CSS -->
    @yield('css')
    <link href="{{ asset('css/fonts.css') }}" rel="stylesheet"/>
    <link href="{{ asset('css/front.css') }}" rel="stylesheet"/>

</head>
<body>

    <header>
        <div class="header-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-9 col-md-2 my-auto">
                        <div class="link">
                            <a href="{{ URL::to('/') }}">
                                <div class="logo">
                                    <img src="{{ asset('upload/'.$logo->image) }}" alt="" title="">
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-3 col-md-10 my-auto text-end">
                        <div class="menu">
                            <i class="fas fa-bars"></i>
                        </div>
                        <div class="main-menu">
                            <ul class="nav-menu">
                                @foreach($menu as $men)
                                @if($men->id == 6)
                                <li class="dropdown">
                                    <div class="dropbtn">
                                        <a href="{{ $men->link }}">{{ $men->menu }}</a>
                                    </div>
                                    <div class="dropdown-content">
                                        <a href="{{ URL::to('/tentang-kami') }}#sejarah">Sejarah</a>
                                        <a href="{{ URL::to('/tentang-kami') }}#sejarah-visi">Visi Misi</a>
                                        <a href="{{ URL::to('/tentang-kami') }}#dasar-pemikiran">Dasar Pemikiran</a>
                                        <a href="{{ URL::to('/tentang-kami') }}#landasan-hukum">Landasan Hukum</a>
                                        <a href="{{ URL::to('/tentang-kami') }}#kerja-sama">Kerja sama</a>
                                        <a href="{{ URL::to('/tentang-kami') }}#kepngurusan-plj">Kepengurusan PLJ</a>
                                        <a href="{{ URL::to('/tentang-kami') }}#profil-jbi">Profil JBI</a>
                                    </div>
                                </li>
                                @elseif($men->id == 2)
                                <li class="dropdown">
                                    <div class="dropbtn">
                                        <a href="{{ $men->link }}">{{ $men->menu }}</a>
                                    </div>
                                    <div class="dropdown-content">
                                        <a href="{{ URL::to('/') }}#layanan">Order JBI</a>
                                        <a href="{{ URL::to('/') }}#layanan">Perekrutan JBI</a>
                                        <a href="{{ URL::to('/') }}#layanan">Saran / Keluhan</a>
                                    </div>
                                </li>
                                @elseif($men->id == 3)
                                <li class="dropdown">
                                    <div class="dropbtn">
                                        <a href="{{ $men->link }}">{{ $men->menu }}</a>
                                    </div>
                                    <div class="dropdown-content">
                                        <a href="{{ URL::to('/informasi') }}#informasi">SOP</a>
                                        <a href="{{ URL::to('/informasi') }}#informasi">Mekanisme</a>
                                        <a href="{{ URL::to('/informasi') }}#informasi">Panduan JBI</a>
                                    </div>
                                </li>
                                @elseif($men->id == 4)
                                <li class="dropdown">
                                    <div class="dropbtn">
                                        <a href="{{ $men->link }}">{{ $men->menu }}</a>
                                    </div>
                                    <div class="dropdown-content">
                                        <a href="{{ URL::to('/kegiatan') }}#mitra">Mitra</a>
                                        <a href="{{ URL::to('/kegiatan') }}#jadwal-jbi">Jadwal JBI</a>
                                        <a href="{{ URL::to('/kegiatan') }}#gallery">Galeri</a>
                                    </div>
                                </li>
                                @else
                                <li><a href="{{ $men->link }}" data-scroll="{{ $men->slug }}">{{ $men->menu }}</a></li>
                                @endif
                                @endforeach
                            </ul>
                        </div>
                        <div class="main-soc">
                            <ul>
                                @foreach($social_media as $social)
                                <?php if($social->name == 'facebook'){ $social_icon = "facebook-f"; }elseif($social->name == 'instagram'){ $social_icon = "instagram";}else{ $social_icon = "twitter"; } ?>
                                <li><a href="{{ $social->link }}" rel="noreferrer noopener" target="_blank"><i class="fab fa-{{ $social_icon }}"></i></a></li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <div id="main">
        @yield('content')
    </div>

    <footer>
        <div class="container">
            <div class="main-menu">
                <nav class="nav-menu">   
                    @foreach($menu as $men)
                    <a href="{{ $men->link }}" data-scroll="{{ $men->slug }}">{{ $men->menu }}</a>  
                    @endforeach
                </nav>
            </div>
            <ul class="l-footer">
                <li>Copyright@stefanstar 2021</li>
                <li>Pusat Layanan Juru Bahasa Isyarat</li>
                <li>All Rights Reserved</li>
            </ul>
            <ul class="l-soc">
                @foreach($social_media as $social)
                <?php if($social->name == 'facebook'){ $social_icon = "facebook-f"; }elseif($social->name == 'instagram'){ $social_icon = "instagram";}else{ $social_icon = "twitter"; } ?>
                    <li><a href="{{ $social->link }}" rel="noreferrer noopener" target="_blank"><i class="fab fa-{{ $social_icon }}"></i></a></li>
                @endforeach
            </ul>
        </div>
    </footer>

    <div class="fixed-message">
        <a href="{{ $hubungi_kami->chat_link }}" target="_blank" rel="noreferrer noopener">
            <div class="tbl">
                <div class="cell">
                    <div class="txt">{{ $hubungi_kami->chat_name }}</div>
                    <div class="img">
                        <img src="{{ asset('upload/'.$hubungi_kami->chat_image) }}"/>
                    </div>
                </div>
            </div>
        </a>
    </div>

    <div class="fixed-top">
        <a>
            <div class="tbl">
                <div class="cell">
                    <div class="img">
                        <img src="{{ asset('images/top.png') }}"/>
                    </div>
                    <div class="text">
                        <div>TOP</div>
                    </div>
                </div>
            </div>
        </a>
    </div>

    <div class="overlay"></div>
    <div class="slide-menu">
        <div class="box-close">
            <i class="fas fa-times"></i>
        </div>
        <div class="pad20">
           <nav class="nav-menu2">
                @foreach($menu as $men)
                    <a href="{{ $men->link }}" data-scroll="{{ $men->slug }}">{{ $men->menu }}</a>  
                    @endforeach
            </nav>
            <div class="main-soc">
                <ul>
                    @foreach($social_media as $social)
                    <?php if($social->name == 'facebook'){ $social_icon = "facebook-f"; }elseif($social->name == 'instagram'){ $social_icon = "instagram";}else{ $social_icon = "twitter"; } ?>
                        <li><a href="{{ $social->link }}" rel="noreferrer noopener" target="_blank"><i class="fab fa-{{ $social_icon }}"></i></a></li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>


<script type="text/javascript" src="{{ mix('/js/app.js') }}"></script>
<script type="text/javascript" src="{{ asset('scrollreveal.min.js') }}"></script>

<!-- JS -->
@yield('js')
<script type="text/javascript">

function scrollAll() {
    a = $(window).scrollTop();
    if (a > 150) {
        $('.fixed-message').fadeIn();
        $('.fixed-top').fadeIn();
    } else {
        $('.fixed-message').fadeOut();
        $('.fixed-top').fadeOut();
    }
}

$(function() {
    var config = {
        reset: true,
        mobile: false,
    }
    window.sr = ScrollReveal(config);

    for (var index = 1; index < 11; index++) {
        // fade in up
        sr.reveal('.sr-up-td' + index, {
            delay: 400 * index,
            scale: 1,
            duration: 1500,
            distance: '100px',
        });
        // fade in down
        sr.reveal('.sr-down-td' + index, {
            delay: 100 * index,
            scale: 1,
            distance: '-40px',
            duration: 1500,
        });
        // fade in left
        sr.reveal('.sr-left-td' + index, {
            delay: 400 * index,
            scale: 1,
            origin: 'left',
            distance: '100px',
            duration: 1500,
        });
        // fade in right
        sr.reveal('.sr-right-td' + index, {
            delay: 400 * index,
            scale: 1,
            origin: 'right',
            distance: '100px',
            duration: 1500,
        });
    }
});
</script>

</body>
</html>
        
