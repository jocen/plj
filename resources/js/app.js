require('./bootstrap');

import 'slick-carousel/slick/slick';

$(document).ready(function() {
	$('.fixed-top a').click(function() {
		$('html, body').animate({scrollTop: 0}, 500);
	});

	$('.menu').click(function(event) {
        $('body').addClass('no-scroll');
        $('.overlay').addClass('active');
        $('.slide-menu').addClass('active');
    });

    $('.box-close').click(function(event) {
        $('body').removeClass('no-scroll');
        $('.overlay').removeClass('active');
        $('.slide-menu').removeClass('active');
    });

    $("html").click(function(a) {
        if (!$(a.target).parents().is(".menu") && !$(a.target).is(".slide-menu") && !$(a.target).parents().is(".slide-menu")) {
            $('body').removeClass('no-scroll');
            $('.overlay').removeClass('active');
            $('.slide-menu').removeClass('active');
        }
    });

    $(window).scroll(function() {
        var a = $(window).scrollTop();
        if (a >= 10) {
            $('header').addClass('scroll');
            $('body').addClass('scroll');
        } else {
            $('header').removeClass('scroll');
            $('body').removeClass('scroll');
        }
    });

    $('ul.nav-menu li a').on('click', function() {
        var scrollAnchor = $(this).attr('data-scroll'),
        scrollPoint = $('section[data-anchor="' + scrollAnchor + '"]').offset().top - 60;
        $('body,html').animate({
            scrollTop: scrollPoint
        }, 500);
        $('body').removeClass('no-scroll');
        $('.overlay').removeClass('active');
        $('.slide-menu').removeClass('active');
        return false;
    });

    $('nav.nav-menu2 a').on('click', function() {
        var scrollAnchor = $(this).attr('data-scroll'),
        scrollPoint = $('section[data-anchor="' + scrollAnchor + '"]').offset().top - 40;
        $('body,html').animate({
            scrollTop: scrollPoint
        }, 500);
        $('body').removeClass('no-scroll');
        $('.overlay').removeClass('active');
        $('.slide-menu').removeClass('active');
        return false;
    });

    $('.main-menu nav a').click(function(event) {
        $('.main-menu nav a').removeClass('active');
        $(this).addClass('active');
    });

    $('.click-kontak-kami').click(function(event) {
        $('html,body').animate({
            scrollTop: $('#kontak-kami').offset().top + 20
        },'slow');
    });

    scrollAll();

    $(window).scroll(function() {
        scrollAll();
    });
});
